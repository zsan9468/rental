<?php

namespace App\Models;

use App\Lib\DB;

class GeneralModel extends BaseModel
{
    protected $table = 'generalsettings';

    public function getIndex()
    {
        return DB::link()->table($this->table)->limit(1)->get();
    }

    public function getOne($id)
    {
        $returnArray = $this->getSingle($this->table, $id);
        return $returnArray;
    }

    public function postItem($data, $id)
    {
        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
