<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">Agreement <span> LIST</span></h5>

                                <table id="agreement_tbale" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="customer_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Create New Agreement ……</h5>
                                <p>Please Create a New Agreement </p>
                                <?php include 'agreement_add.php' ?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="agreement_update_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg"  style="width: 1000px; ">
                <div class="modal-content" style="width: 1000px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.agreement.update">Update Agreement</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'agreement_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>

    function customer_select(){
        var ar =$("#agreement_update_form #customer_list_update option[value = "+$('#agreement_update_form #customer_name').val()+"]");
        $("#agreement_update_form #costumerId").val($('#agreement_update_form #customer_name').val());
        $('#agreement_update_form #customer_name').val(ar[0].outerText);
    }
    $(document).ready(function () {
        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'name', title: "unitName", name: "unitName" },
            { data: 'customer_name', title: "customer_name", name: "customer_name" },
            { data: 'cost', title: "cost", name: "cost" },
            { data: 'rentalBeginDateTime', title: "rentalBeginDateTime", name: "rentalBeginDateTime" ,render:(data,type,row)=>{
                    var dateInfo  =  dateFormaChanget(data,1)
                    return dateInfo;
                }},
            { data: 'rentalEndDateTime', title: "rentalEndDateTime", name: "rentalEndDateTime",render:(data,type,row)=>{
                    var dateInfo  =  dateFormaChanget(data,1)
                    return dateInfo;
                } },
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#agreement_tbale').undelegate('tbody #' + upodateBtn, 'click');
                    $('#agreement_tbale').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('.rentalAgreement_properties').html(' ');
                        $('.AddPropertList').html('');
                        $.agreementInitialize("agreement_update_form",'customer_list_update')
                        var agreementInfo = formAjaxHy('/api/agreement?id='+row.id ,'', 'get');
                        // Rental agreement information
                        var agreementInfoData = agreementInfo.payload.data.data[0];
                        $("#agreement_update_form input[name=id]").val(row.id);
                        $("#agreement_update_form input[name=customer_name]").val(agreementInfoData.customer_name);
                        $("#agreement_update_form input[name=rentalUnitsId]").val(agreementInfoData.rentalID);
                        $("#agreement_update_form input[name=costumerId]").val(agreementInfoData.customer_id);
                        $("#agreement_update_form input[name=rentalUnitsId]").val(agreementInfoData.rentalID);
                        $("#agreement_update_form #unitname").val(agreementInfoData.name);
                        $("#agreement_update_form input[name=rentalBeginDateTime]").val(agreementInfoData.rentalBeginDateTime);
                        $("#agreement_update_form input[name=rentalEndDateTime]").val(agreementInfoData.rentalEndDateTime);
                        $("#agreement_update_form input[name=cost]").val(agreementInfoData.cost);
                        $("#agreement_update_form #insurenceId").val(agreementInfoData.insurence_id);
                        var agreementInfoData = agreementInfo.payload.data.data[0];
                        $('#agreement_update_model').modal('show');
                        // 出租协议属性信息

                        var rentalagreementextrasData = agreementInfo.payload.data.rentalagreementextras;
                        $.extraList("#agreement_update_form .AddPropertList",rentalagreementextrasData)
                        var rentalagreementpropertiesData = agreementInfo.payload.data.rentalagreementproperties;
                        $.rentalAgreement_properties("#agreement_update_form .rentalAgreement_properties",rentalagreementpropertiesData)
                    })

                    $('#agreement_tbale').undelegate('tbody #' + deleteBtn, 'click');
                    $('#agreement_tbale').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/agreement');
                    })
                    return "<button  type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm   btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/agreement', 'get', 'agreement_tbale', columns,);

        $('#agreement_update_form').submit(function (e) {
            var rentalUnitsId = $("#agreement_update_form input[name=rentalUnitsId]").val();
            var id = $("#agreement_update_form input[name=id]").val();
            var data =  $.rentalAgreementBiaodan('agreement_update_form',e,'create')
            data.agreement.rentalUnitsId = rentalUnitsId
            data.agreement.id = id
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement','put',data,
                function(data){
                    if(data.payload.code == 200){
                        $('#agreement_update_model').modal('hide');
                         commonUtil.message('Update success！')
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>