<style>
    div{width: 800px;height: 500px;margin: auto;border: 1px solid black;}

    h1{margin-left: 650px;}

    input{ margin-right: 10px;}
    container{ width: 100%;height: auto}
    input:nth-of-type(1){margin-left: 360px;}
</style>

<h1>简易画图工具</h1>
<div class="container">
    <canvas width="100%" id="canvas" ></canvas>
</div>



<input type="button" value="清除画布" class="qingChu">

<input type="color" class="yanSe">

<input type="range" min="1" max="10" class="chuxi">

<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=6009e6adcf8c45466fee3e56" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>




    let canEle = document.getElementsByTagName("canvas")[0];
    console.log(canEle.offsetWidth, canEle.offsetHeight);
    let pen = canEle.getContext("2d");
    let image = new Image()
    image.src = '/public/Img/blueprints.jpg'
    image.onload = function(){
        var canvasTemp = document.getElementsByTagName("canvas")[0];
        var contextTemp = canvasTemp.getContext('2d');
        canvasTemp.width = 850; // 目标宽度
        canvasTemp.height = 800; // 目标高度
        contextTemp.drawImage(this,0,0,850,800)
        var ptn = pen.createPattern(canvasTemp,'no-repeat')
        pen.fillStyle = ptn;
        pen.fill()

    }
    let colEle = document.getElementsByClassName("yanSe")[0];
    let qcEle = document.getElementsByClassName("qingChu")[0];
    let chuxiEle = document.getElementsByClassName("chuxi")[0];

    console.log(colEle);
    canEle.addEventListener("mousedown", function (e) {
        let event1 = e || window.event1;
        pen.moveTo(event1.clientX - canEle.offsetLeft, event1.clientY - canEle.offsetTop);
        pen.beginPath();
        canEle.addEventListener("mousemove", hua);

        function hua(e) {
            let event = e || window.event;
            pen.strokeStyle = colEle.value;
            pen.lineWidth = chuxiEle.value;
            pen.lineTo(event.offsetX, event.offsetY);
            console.log(pen.lineWidth);
            pen.stroke();
        }

        canEle.addEventListener("mouseup", function () {
            canEle.removeEventListener("mousemove", hua);
        })

    })

    qcEle.addEventListener("click", qingchu);
    function qingchu() {
        pen.clearRect(0, 0, canEle.offsetWidth, canEle.offsetHeight);
    }

    function downLoadImage() {
        var canvas = document.getElementById("canvas");
        var dataURL = canvas.toDataURL();
        console.log(dataURL);

    }




    $(document).ready(function() {
        // The default language is English
        var lang = "en-gb";
        $(".lang").each(function(index, element) {
            $(this).text(arrLang[lang][$(this).attr("key")]);
        });
    });

    // get/set the selected language
    $(".translate").click(function() {
        var lang = $(this).attr("id");

        $(".lang").each(function(index, element) {
            $(this).text(arrLang[lang][$(this).attr("key")]);
        });
    });

    var arrLang = {
        "en-gb": {
            "HOME": "Home",
            "ABOUT": "About Us",
            "CONTACT": "Contact Us",
        },
        "zh-tw": {
            "HOME": "??",
            "ABOUT": "????",
            "CONTACT": "????",
        }
    };
</script>