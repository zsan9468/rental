<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/28
 * Time:  13:41
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\UserModel;
use App\Models\UserPermiasionSelectionModel;

class UserPermiasionSelectionResource
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create($requestData)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store($request,$userId)
    {
        if(!empty($request))
        {
            $permissionArray  = explode(',',$request);
            $userpermiasionselectionData = array();
            $i = 0;
            foreach ($permissionArray as $item)
            {
                $userpermiasionselectionData[$i]['permission_id'] = (int)$item;
                $userpermiasionselectionData[$i]['user_id'] = (int)$userId;
                $i++;
            }
            $permissonMode = new UserPermiasionSelectionModel();
            $perssionresult = $permissonMode->createAll($userpermiasionselectionData);
            return $perssionresult;
        }
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $permiasionModel= new UserPermiasionSelectionModel();
        $permiasionArr = $permiasionModel->getSignalUserPerssion($id);
        $permiasions = array();
        foreach ($permiasionArr as $item)
        {
            array_push($permiasions,$item['permission_id']);
        }
        return $permiasions;

    }

    /**
     * @param $permission
     * @param $userId
     * @return void
     */
    public function update($permission,$userId)
    {
        $this->destroy($userId);
        $this->store($permission,$userId);
    }


    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($userId)
    {
        //
        $permissonMode = new UserPermiasionSelectionModel();
        $permissonMode->delete($userId);
    }
}