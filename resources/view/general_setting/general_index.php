<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
  <!-- ======= Header ======= -->
  <?php include './../../layout/header.php'; ?>
  <!-- ======= Sidebar ======= -->
  <?php include './../../layout/sidebar.php'; ?>
  <main id="main" class="main">
    <section class="section dashboard">
      <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-12">
          <div class="row">
            <!-- Recent Sales -->
            <div class="col-12">
              <div class="card recent-sales overflow-auto">
                <div class="card-body">
                  <h5 class="card-title lang"  key="rentalunits.update.general">Update
                      General Content</h5>
                  <div class="card">
                  </div>
                    <form class="row g-3 was-validated" id="gereral_form_update" novalidate >
                        <div class="col-md-5">
                            <label for="Greeting" class="form-label">Greeting</label>
                            <input type="text" class="form-control" name="greeting" placeholder="Please Enter Greeting" required>
                            <input type="hidden" class="form-control" name="id" >
                        </div>

                        <div class="col-md-5">
                            <label for="VATrate" class="form-label">VATrate</label>
                            <input type="number" class="form-control" name="VATrate" placeholder="Please Enter VATrate" >
                        </div>

                        <div class="col-md-8">
                            <label for="invoiceText1" class="form-label">invoiceText1</label>
                            <input type="text" class="form-control" name="invoiceText1"
                                   placeholder="Please Enter invoiceText1" >
                        </div>


                        <div class="col-md-8">
                            <label for="invoiceText2" class="form-label">invoiceText2</label>
                            <input type="text" class="form-control" name="invoiceText2"
                                   placeholder="" >
                        </div>

                        <div class="col-md-8">
                            <label for="invoiceText3" class="form-label">invoiceText3</label>
                            <input type="text" class="form-control" name="invoiceText3">
                        </div>

                        <div class="col-md-4">
                            <label for="paymentWarning1" class="form-label">paymentWarning1</label>
                            <input type="text" class="form-control" name="paymentWarning1"
                                   placeholder="" >
                        </div>
                        <div class="col-md-4">
                            <label for="paymentWarning2" class="form-label">paymentWarning2</label>
                            <input type="text" class="form-control" name="VATrate" placeholder="" >
                        </div>

                        <div class="col-md-4">
                            <label for="paymentWarning3" class="form-label">paymentWarning3</label>
                            <input type="text" class="form-control" name="paymentWarning3"
                                   placeholder="" >
                        </div>
                        <div class="col-md-4">
                            <label for="offer1" class="form-label">offer1</label>
                            <input type="text" class="form-control" name="offer1" placeholder="">
                        </div>

                        <div class="col-md-4">
                            <label for="offer2" class="form-label">offer2</label>
                            <input type="text" class="form-control" name="offer2" placeholder="" >
                        </div>

                        <div class="col-md-4">
                            <label for="offer3" class="form-label">offer3</label>
                            <input type="text" class="form-control" name="offer3" placeholder="">
                        </div>

                        <div class="row col-md-8" style="margin-top: 1rem">
                            <label class="col-md-6 col-form-label lang" key="general.time.format">Time display format</label>
                            <div class="col-sm-6">
                                <select id="date_format" name="date_format" class="form-select">
                                    <option value="1">YYYY-MM-DD</option>
                                    <option value="2">MM/DD/YYYY</option>
                                    <option value="3">DD-MM-YYYY </option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-8" style="margin-top: 1rem">
                            <label class="col-md-6 col-form-label lang" key="general.language.selection">Enable  Language selection ？</label>
                            <div class="col-sm-6">
                                <select id="language_forbid" name="language_forbid" class="form-select">
                                    <option value="1">Enable</option>
                                    <option value="2">Disabled</option>
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form><!-- End No Labels Form -->

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!--  Add General Content -->


    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php include './../../layout/footer.html'; ?>
  <!-- End Footer -->
  <!-- Vendor JS Files -->
  <?php include './../../layout/js.html' ?>
  <script>
    $(document).ready(function () {
        $.ajaxUnit('/api/general','get','',function (data) {
            if (data.payload.code == 200)
            {
                if(data.payload.data.length >0){
                    var row = data.payload.data[0];
                    var dom = 'gereral_form_update';
                    var id = $(" input[name=id]").val(row.id);
                    var greeting = $(" input[name=greeting]").val(row.greeting);
                    var VATrate = $(" input[name=VATrate]").val(row.VATrate);
                    var invoiceText1 = $(" input[name=invoiceText1]").val(row.invoiceText1);
                    var invoiceText2 = $(" input[name=invoiceText2]").val(row.invoiceText2);
                    var invoiceText3 = $(" input[name=invoiceText3]").val(row.invoiceText3);
                    var paymentWarning1 = $(" input[name=paymentWarning1]").val(row.paymentWarning1);
                    var paymentWarning2 = $(" input[name=paymentWarning2]").val(row.paymentWarning2);
                    var paymentWarning3 = $(" input[name=paymentWarning3]").val(row.paymentWarning3);
                    var offer1 = $(" input[name=offer1]").val(row.offer1);
                    var offer2 = $(" input[name=offer2]").val(row.offer2);
                    var offer3 = $(" input[name=offer3]").val(row.offer3);
                    var date_format = $(" #date_format").val(row.date_format);
                    var language_forbid = $(" #language_forbid").val(row.language_forbid);
                }
            }
        },function (){})

        $('#gereral_form_update').submit(function (e) {
            var id = $("#gereral_form_update input[name=id]").val();

            var data =  $.gerena_InfoBiaodan('gereral_form_update',e,'update')
            if(id == ''){
                var sunmit_type = 'post'
            }else{
                data.id = id
                var sunmit_type = 'put'
            }
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/general',sunmit_type,data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Set success！Refresh after 1 seconds');
                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })

    });
  </script>
</body>

</html>