<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\RentalPropertiesModel;

class RentalPropertiesResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new RentalPropertiesModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'propertieName' => ['required'],
        ];
        $messages = [
            'propertieName.required' => 'The propertieName field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new RentalPropertiesModel;
        $resultCode = $this->baseUpdate($newModel, $data, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new RentalPropertiesModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
