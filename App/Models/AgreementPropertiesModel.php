<?php

namespace App\Models;

class AgreementPropertiesModel extends BaseModel
{
    protected $table = 'rentalagreementproperties';

    public function getIndex($start, $length, $search, $order)
    {
        $whereSql = '';
        if (!$search == '') {
            $whereSql = vsprintf("propertyName REGEXP '%s'", array($search));
        }
        return $this->baseGetIndex($this->table, $start, $length, $search, $order, $whereSql);
    }

    public function getOne($id)
    {
        $returnArray = $this->getSingle($this->table, $id);
        return $returnArray;
    }

    public function postItem($data, $id)
    {
        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
