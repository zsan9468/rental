<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\AgreementExtraPropertiesModel;

class AgreementExtraPropertiesResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new AgreementExtraPropertiesModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $newModel = new AgreementExtraPropertiesModel;
        $resultCode = $this->baseUpdate($newModel, $data, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new AgreementExtraPropertiesModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
