<?php

/**
 * --------------------------------------------------------------------------------
 * Dump and Die.
 * --------------------------------------------------------------------------------
 */
if (!function_exists('dd')) {
    function dd()
    {
        $args = func_get_args();
        call_user_func_array('var_dump', $args);
        die(1);
    }
}

/**
 * --------------------------------------------------------------------------------
 * Read the defined array data in the config folder
 * --------------------------------------------------------------------------------
 */
if (!function_exists('config')) {
    function config($key, $default_val = null)
    {
        // Load the config.php file from the config folder
        $config = include_once(PROJECT_ROOT_PATH . '/config/config.php');
        // Get the value of the specified key
        $value = $config[$key] ?? null;
        // If the key doesn't exist or is null, return the default value
        return $value ?? $default_val;
    }
}
