<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalPropertiesResource;

class RentalPropertiesController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalPropertiesResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalPropertiesResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalPropertiesResource)->destroy($data);
    }
}
