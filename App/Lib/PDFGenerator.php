<?php

namespace App\Lib;

class PDFGenerator
{
    private $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function generatePDF()
    {
        $pdf = "%PDF-1.4\n";
        $pdf .= "1 0 obj\n";
        $pdf .= "<< /Type /Catalog /Pages 2 0 R >>\n";
        $pdf .= "endobj\n";
        $pdf .= "2 0 obj\n";
        $pdf .= "<< /Type /Pages /Kids [3 0 R] /Count 1 >>\n";
        $pdf .= "endobj\n";
        $pdf .= "3 0 obj\n";
        $pdf .= "<< /Type /Page /Parent 2 0 R /MediaBox [0 0 612 792] /Contents 4 0 R >>\n";
        $pdf .= "endobj\n";
        $pdf .= "4 0 obj\n";
        $pdf .= "<< /Length " . strlen($this->content) . " >>\n";
        $pdf .= "stream\n";
        $pdf .= $this->content . "\n";
        $pdf .= "endstream\n";
        $pdf .= "endobj\n";
        $pdf .= "xref\n";
        $pdf .= "0 5\n";
        $pdf .= "0000000000 65535 f \n";
        $pdf .= "0000000010 00000 n \n";
        $pdf .= "0000000050 00000 n \n";
        $pdf .= "0000000100 00000 n \n";
        $pdf .= "0000000200 00000 n \n";
        $pdf .= "trailer\n";
        $pdf .= "<< /Size 5 /Root 1 0 R >>\n";
        $pdf .= "startxref\n";
        $pdf .= "292\n";
        $pdf .= "%%EOF\n";

        return $pdf;
    }
}
