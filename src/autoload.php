<?php

spl_autoload_register(function ($class) {
    $file = str_replace('\\', '/', $class) . '.php';
    require $file;
});

include_once 'Helper.php';
