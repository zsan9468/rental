<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class AgreementModel extends BaseModel
{
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }

    protected $table = 'rentalagreement';
    protected $tablePropertiesSelection = 'rentalagreementpropertyselection';
    protected $tableProperties = 'rentalagreementproperties';
    protected $tableExtrasSelection = 'unitextraspropertyselection';
    protected $tableExtras = 'unitextrasproperty';
    protected $tableInsurence = 'insurence';

    protected $tableRentalUnit = 'rentalunits';

    public function getIndex($start, $length, $search, $order)
    {
        $limitSql = 'LIMIT ' . $start . ',' . $length;
        if ($search == '') {
            $whereSql = '';
        } else {
            $whereSql = vsprintf("WHERE cu.`firstName` REGEXP '%s' or un.`name` REGEXP '%s'", array($search, $search));
        }

        // Query All Agreement Data
        $sql = "SELECT a.`id`, a.`createdDateTime`, a.`rentalBeginDateTime`, a.`rentalEndDateTime`,a.cost,
        cu.`firstName`, cu.`city`,
        ins.`insurenceType`,
        re.`id` AS rentalID,
        concat(cu.firstName,' ',cu.lastName) as customer_name,
        un.`name`
        FROM rentalagreement AS a
        LEFT JOIN customer AS cu  ON  cu.id = a.costumerId 
        LEFT JOIN insurence AS ins  ON  ins.id = a.insurenceId 
        LEFT JOIN rentalunits AS re  ON  re.id = a.rentalUnitsId 
        LEFT JOIN unitnames AS un  ON  un.id = re.unitNamesId         
        {$whereSql}
        ORDER BY {$order}            
        {$limitSql}";
        $returnArray['data'] = DB::link()->query($sql);

        // Query Agreement Count
        $toTal = DB::link()->table($this->table)->count();
        $returnArray['recordsFiltered'] = count($returnArray['data']);
        $returnArray['recordsTotal'] = (int)$toTal;

        return $returnArray;
    }

    public function getOne($id)
    {
        // Query One Agreement Data
        $whereSql = "WHERE a.id = $id";
        $sql = "SELECT a.`id`, a.cost, a.`createdDateTime`, a.`rentalBeginDateTime`, a.`rentalEndDateTime`,
        cu.`id` as customer_id, cu.`firstName`, cu.`city`,
        ins.`id` as insurence_id, ins.`insurenceType`,
        concat(cu.firstName,' ',cu.lastName) as customer_name,
        re.`id` AS rentalID,
        un.`name`
        FROM rentalagreement AS a
        LEFT JOIN customer AS cu  ON  cu.id = a.costumerId 
        LEFT JOIN insurence AS ins  ON  ins.id = a.insurenceId 
        LEFT JOIN rentalunits AS re  ON  re.id = a.rentalUnitsId 
        LEFT JOIN unitnames AS un  ON  un.id = re.unitNamesId         
        {$whereSql}";
        $returnArray['data'] = DB::link()->query($sql);

        $sqlProperties = "SELECT p.`propertyName` AS rentalAgreementPropertyName, p.`id` AS rentalAgreementPropertiesId, p.`propertyDescription`,
        ps.`rentalAgreementPropertyValue`, ps.`id` AS selectionId
        FROM rentalagreementpropertyselection AS ps
        LEFT JOIN rentalagreement AS a ON a.id = ps.rentalAgreementId
        LEFT JOIN rentalagreementproperties AS p ON p.id = ps.rentalAgreementPropertiesId
        {$whereSql}";
        $returnArray['rentalagreementproperties'] = DB::link()->query($sqlProperties);

        $sqlExtras = "SELECT un.id AS extraspropertyID, un.extraName, un.extraPrice, un.extraDescription
        FROM unitextraspropertyselection AS us
        LEFT JOIN rentalagreement AS a ON a.id = us.rentalAgreementId
        LEFT JOIN unitextrasproperty AS un ON un.id = us.unitExtrasId
        {$whereSql}";
        $returnArray['rentalagreementextras'] = DB::link()->query($sqlExtras);
        return $returnArray;
    }

    public function postItem($data)
    {
        try {
            // RentalStatusTyp: 1 free, 2 rented, 3 on repair
            // DamageStatus: 1 on repair, 2 repaired

            // Begin the transaction
            DB::link()->db->beginTransaction();

            if (isset($data['agreement']['id'])) {
                // update
                $id = $data['agreement']['id'];
                unset($data['agreement']['id']);

                // update the main data
                DB::link()->table($this->table)->where("id = $id")->update($data['agreement']);

                // Insert the additional information data
                if (!empty($data['agreementextras'])) {
                    foreach ($data['agreementextras'] as $v) {
                        if (!empty($v['extraName'])) {
                            // If there is a value
                            // it is determined whether to add or update according to the extraID
                            if (isset($v['extraID']) && !empty($v['extraID'])) {
                                // update
                                $extraId = $v['extraID'];
                                unset($v['extraID']);
                                DB::link()->table($this->tableExtras)->where("id = $extraId")->update($v);
                            } else {
                                // add
                                unset($v['extraID']);
                                DB::link()->table($this->tableExtras)->insert($v);
                                $unitExtrasId = DB::link()->db->lastInsertId();
                                $selectionData['unitExtrasId'] = $unitExtrasId;
                                $selectionData['rentalAgreementId'] = $id;
                                DB::link()->table($this->tableExtrasSelection)->insert($selectionData);
                            }
                        } else {
                            // Value is empty, delete or do nothing
                            if (isset($v['extraID']) && !empty($v['extraID'])) {
                                // delete
                                $extraId = $v['extraID'];
                                DB::link()->table($this->tableExtrasSelection)->where("unitExtrasId = $extraId")->delete();
                                DB::link()->table($this->tableExtras)->where("id = $extraId")->delete();
                            }
                        }
                    }
                }

                if (!empty($data['agreementproperties'])) {
                    foreach ($data['agreementproperties'] as $v) {
                        if (!empty($v['rentalAgreementPropertyValue'])) {
                            // If there is a value
                            // it is determined whether to add or update according to the selectionId
                            if (isset($v['selectionId']) && !empty($v['selectionId'])) {
                                // update
                                $propertiesId = $v['selectionId'];
                                unset($v['selectionId']);
                                DB::link()->table($this->tablePropertiesSelection)->where("id = $propertiesId")->update($v);
                            } else {
                                // add
                                $v['rentalAgreementId'] = $id;
                                unset($v['selectionId']);
                                DB::link()->table($this->tablePropertiesSelection)->insert($v);
                            }
                        } else {
                            // Value is empty, delete or do nothing
                            if (isset($v['selectionId']) && !empty($v['selectionId'])) {
                                // delete
                                $propertiesId = $v['selectionId'];
                                DB::link()->table($this->tablePropertiesSelection)->where("id = $propertiesId")->delete();
                            }
                        }
                    }
                }
            } else {
                // Insert
                // Insert the main data
                DB::link()->table($this->table)->insert($data['agreement']);
                $agreementId = DB::link()->db->lastInsertId();

                // Insert the additional information data
                if (!empty($data['agreementextras'])) {
                    foreach ($data['agreementextras'] as $v) {
                        DB::link()->table($this->tableExtras)->insert($v);
                        $unitExtrasId = DB::link()->db->lastInsertId();
                        $selectionData['unitExtrasId'] = $unitExtrasId;
                        $selectionData['rentalAgreementId'] = $agreementId;
                        DB::link()->table($this->tableExtrasSelection)->insert($selectionData);
                    }
                }

                if (!empty($data['agreementproperties'])) {
                    foreach ($data['agreementproperties'] as $v) {
                        if (!empty($v['rentalAgreementPropertyValue'])) {
                            $v['rentalAgreementId'] = $agreementId;
                            DB::link()->table($this->tablePropertiesSelection)->insert($v);
                        }
                    }
                }

                // Update the status of the rentalunit
                $statusTypOnrepairValue = config('rentalStatusTyp')[2]; // rented
                $statustypData = DB::link()->table('statustyp')->where("type = '$statusTypOnrepairValue'")->field('id')->get();
                if (isset($statustypData[0]['id'])) {
                    $rentalUnitsId = $data['agreement']['rentalUnitsId'];
                    $updateWhereSql = "id = $rentalUnitsId";
                    $updateData['statusTypId'] = $statustypData[0]['id'];
                    $ReturnBool = DB::link()->table($this->tableRentalUnit)->where($updateWhereSql)->update($updateData);
                } else {
                    throw new PDOException("Invalid statusType Table data, '$statusTypOnrepairValue' value not found");
                }
            }

            // Commit the transaction
            DB::link()->db->commit();

            $ReturnBool = true;
        } catch (PDOException $e) {
            // Roll back the transaction on error
            DB::link()->db->rollBack();

            // Log the error
            FileLogger::error('Error inserting data: ' . $e->getMessage());

            // Return failure
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function deleteItem($id)
    {
        try {
            DB::link()->db->beginTransaction();
            DB::link()->table($this->tablePropertiesSelection)->where("rentalAgreementId = $id")->delete();
            $extraIdData = DB::link()->table($this->tableExtrasSelection)->where("rentalAgreementId = $id")->field('unitExtrasId')->get();

            $extraId = implode(',', array_column($extraIdData, 'unitExtrasId'));

            DB::link()->table($this->tableExtrasSelection)->where("rentalAgreementId = $id")->delete();
            if (!empty($extraId)) {
                DB::link()->table($this->tableExtras)->where("id IN ($extraId)")->delete();
            }
            $queryRentalUnitsId = DB::link()->table($this->table)->where("id = $id")->field('rentalUnitsId')->get();
            DB::link()->table($this->table)->where("id = $id")->delete();

            // Update the status of the rentalunit
            if (false) {
                // !! When deleting the agreement, the status of the rentalUnit should not be updated
                $statusTypFreeValue = config('rentalStatusTyp')[1]; // free
                $statustypData = DB::link()->table('statustyp')->where("type = '$statusTypFreeValue'")->field('id')->get();
                if (isset($statustypData[0]['id'])) {
                    $rentalUnitsId = $queryRentalUnitsId[0]['rentalUnitsId'];
                    $updateWhereSql = "id = $rentalUnitsId";
                    $updateData['statusTypId'] = $statustypData[0]['id'];
                    $ReturnBool = DB::link()->table($this->tableRentalUnit)->where($updateWhereSql)->update($updateData);
                } else {
                    throw new PDOException("Invalid statusType Table data, '$statusTypFreeValue' value not found");
                }
            }

            DB::link()->db->commit();
            $ReturnBool = true;
        } catch (PDOException $e) {
            DB::link()->db->rollBack();
            FileLogger::error('Error deleting data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function getInsurencesList()
    {
        try {
            $returnArray['data'] = DB::link()->table($this->tableInsurence)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error getting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }

    public function getPropertiesList()
    {
        try {
            $returnArray['data'] = DB::link()->table($this->tableProperties)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error getting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }

    public function getExtrasList()
    {
        try {
            $returnArray['data'] = DB::link()->table($this->tableExtras)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error getting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }

    public function updateRentalunitStatusTyp($AgreementId)
    {
        try {
            // Update the status of the rentalunit
            $statusTypFreeValue = config('rentalStatusTyp')[1]; // free
            $statustypData = DB::link()->table('statustyp')->where("type = '$statusTypFreeValue'")->field('id')->get();
            if (isset($statustypData[0]['id'])) {
                $queryRentalUnitsId = DB::link()->table($this->table)->where("id = $AgreementId")->field('rentalUnitsId')->get();
                $rentalUnitsId = $queryRentalUnitsId[0]['rentalUnitsId'];
                $updateData['statusTypId'] = $statustypData[0]['id'];
                DB::link()->table($this->tableRentalUnit)->where("id = $rentalUnitsId")->update($updateData);
                $returnArray = 200;
            } else {
                throw new PDOException("Invalid statusType Table data, '$statusTypFreeValue' value not found");
            }
        } catch (PDOException $e) {
            FileLogger::error('Error updating data: ' . $e->getMessage());
            $returnArray = 502;
        }
        return $returnArray;
    }

    /**
     * statistics()
     * 获取关于agreement的相关统计信息 。返回array数组
     * Obtain statistical information about agreement. Return array array
     * @return array
     *
     */
    public function Statistics()
    {
        $returnArray = array();
        // 返回目前有效租约条数
        // Returns the number of leases currently in force$
        $validNumSql = "select count(*) as agreementValidNum from rentalagreement  where rentalEndDateTime > NOW();";
        $validNumResult  = $this->DB->query($validNumSql);
        $validNum = $validNumResult->fetch();
        $returnArray['agreementValidNum'] = $validNum['agreementValidNum'];
        // 返回全部租约条数
        // Returns the total number of lease terms
        $totalNumSql = "select count(*) as TotalNum from rentalagreement";
        $TotalNumResult  = $this->DB->query($totalNumSql);
        $totalNum = $TotalNumResult->fetch();
        $returnArray['agreementTotalNum'] = $totalNum['TotalNum'];

        return $returnArray;
    }
}
