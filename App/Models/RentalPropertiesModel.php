<?php

namespace App\Models;

class RentalPropertiesModel extends BaseModel
{
    protected $table = 'rentalproperties';

    public function getIndex($start, $length, $search, $order)
    {
        $whereSql = '';
        if (!$search == '') {
            $whereSql = vsprintf("propertieName REGEXP '%s'", array($search));
        }
        return $this->baseGetIndex($this->table, $start, $length, $search, $order, $whereSql);
    }

    public function getOne($id)
    {
        $returnArray = $this->getSingle($this->table, $id);
        return $returnArray;
    }

    public function postItem($data, $id)
    {
        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
