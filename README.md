# Rental

## First visit

The connection to the database needs to be configured before use, and the configuration file is located at: `config/database.php`

If the file: config/database.php does not exist, please refer to the following code to create the file manually:

 ```php
 <?php
 
 define("DB_HOST", "localhost");
 define("DB_PORT", "3306");
 define("DB_USER", "root");
 define("DB_PASS", "123456");
 define("DB_DBNAME", "database_name");
 define("DB_CHARSET", "utf8");
 ```

You can use the migration file to test the application. The database migration file is located in: `database/migrations`

Once everything is ready, visit the site directory to get started.

## Directory Structure

### The Root Directory

#### The app Directory

The `app` directory contains the core code of application.

Almost all of the classes will be in this directory.

#### The bootstrap Directory

Not typically need to modify any files within this directory.

#### The config Directory

The `config` directory contains all of application's configuration files.

#### The database Directory

The `database` directory contains database migrations.

#### The routes Directory

The `routes` directory contains all of the route definitions for application.

### The app Directory

#### The Lib Directory

The `Lib` directory contains all general functions and classes.

The `Helper` class is a custom function, if there is a new tool function, please add it here.

// more content
