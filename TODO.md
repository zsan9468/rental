# Todo List

[2023-02-20] Do not update personal to-dos anymore

- ~~HTTP status codes;~~

- ~~Merge the code of the Database class into the Basemodel class;~~

- ~~Compilation of Api interface documents;~~

- ~~logging system;~~

- ~~Use namespaces to standardize code;~~

- ~~Implement automatic loading of classes (spl_autoload_register() or composer);~~

- ~~Remove other router lib;~~

- ~~complete query builder;~~

- ~~Write a routing system that conforms to the REST API style;~~

- ~~Optimize code to achieve object-oriented programming;~~

- ~~Build a simple curd controller.~~
