<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\RentalLocationModel;

class RentalLocationResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new RentalLocationModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'location' => ['required'],
        ];
        $messages = [
            'location.required' => 'The location field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new RentalLocationModel;
        $resultCode = $this->baseUpdate($newModel, $data, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new RentalLocationModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
