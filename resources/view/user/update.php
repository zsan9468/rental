<div class="modal fade" id="update_emplyee_user_model" tabindex="-1">

    <div class="modal-dialog modal-dialog-scrollable  modal-lg">
        <div class="modal-content" style="width: 1000px; height: auto">
            <div class="modal-header">
                <h4 class="modal-title lang" key="user.update.employee">Update Employee</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class=" modal-body">
                <div class="card">
                    <div class="card-body">
                        <form class="row g-3 was-validated" id="update_emplyee_user_form" novalidate>
                            <div class="col-md-8">
                                <label for="name" class="form-label lang" key="user.username">UserName</label>
                                <input type="text" class="form-control" id="update_username" name="username" value="" required>
                                <input type="hidden" name="userId" value="">
                                <div class="valid-feedback">
                                    UserName Data Entry Ok !
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a UserName
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="name" class="form-label">Password(If the password is empty, the password remains the original password)</label>
                                <input type="password" class="form-control" id="update_password" name="password" value="" >
                            </div>
                            <div class="col-md-6">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control" id="update_name" name="name" value="">
                            </div>
                            <div class="col-md-6">
                                <label for="name" class="form-label">Surname</label>
                                <input type="text" class="form-control" id="update_surname" name="surname" value="" >
                            </div>
                            <div class="col-md-8">
                                <label for="name" class="form-label">Email</label>
                                <i>@</i><input type="email" class="form-control" id="update_email" name="email" value="" autocomplete
                                               >
                            </div>

                            <div class="row col-md-8" style="margin-top: 1rem">
                                <label class="col-md-3 col-form-label">User Type</label>
                                <div class="col-sm-9">
                                    <select id="update_user_type" name="user_type" class="form-select">
                                        <option value='3'>Employee</option>
                                        <option value='2'>Admin</option>
                                        <option value='1' disabled>IT</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 hy_create_permission">
                                <label for="name" class="form-label"><b>Permission Choose</b></label>
                                <!--                                        menu-->
                                <div class="row md-12 update_menu ">

                                </div>
                            </div>

                            <div class="row col-md-8" style="margin-top: 1rem">
                                <label class="col-md-3 col-form-label">CostCenter</label>
                                <div class="col-sm-9">
                                    <select id="update_update_cost_center" name="cost_center" class="form-select">

                                    </select>
                                </div>
                            </div>

                            <div class="col-12">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </form><!-- End Custom Styled Validation -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Modal Dialog Scrollable-->
