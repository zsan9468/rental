
<div class="tab-pane fade show active profile-overview" id="profile-overview">

    <h5 class="card-title">Profile Details</h5>

    <div class="row">
        <div class="col-lg-3 col-md-4 label ">UserName</div>
        <div class="col-lg-9 col-md-8"><text class="overview_username"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 label">Name</div>
        <div class="col-lg-9 col-md-8"><text class="overview_name"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 label">Surname</div>
        <div class="col-lg-9 col-md-8"><text class="overview_surname"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 label">Email</div>
        <div class="col-lg-9 col-md-8"><text class="overview_email"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 label">User Type</div>
        <div class="col-lg-9 col-md-8"><text class="overview_user_type text-danger"></text></div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-4 label">CostCenter Name</div>
        <div class="col-lg-9 col-md-8"><text class="overview_costName"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 label">CostCenter City</div>
        <div class="col-lg-9 col-md-8"><text class="overview_city"></text></div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md- label">PostalCode</div>
        <div class="col-lg-9 col-md-8"><text class="overview_postalCode"></text></div>
    </div>

</div>
