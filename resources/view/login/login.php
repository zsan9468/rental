<?php session_start();
session_destroy(); ?>
<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/head.php'; ?>
<!-- Template Main CSS File -->
</head>

<body>
  <main>
    <div class="container">
      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                    <p class="text-center small">Enter your username & password to login</p>
                  </div>
                  <form class="row g-3 needs-validation" id="login_form" novalidate>
                    <div class="col-12">
                      <label for="yourUsername" class="form-label">Username</label>
                      <div class="input-group has-validation">
<!--                        <span class="input-group-text" id="inputGroupPrepend">@</span>-->
                        <input type="text" name="username" class="form-control" id="yourUsername" required>
                        <div class="invalid-feedback">Please enter your username.</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <label for="yourPassword" class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" id="yourPassword" required>
                      <div class="invalid-feedback">Please enter your password!</div>
                    </div>
<!---->
<!--                    <div class="col-12">-->
<!--                      <div class="form-check">-->
<!--                        <input class="form-check-input" type="checkbox" name="remember" value="true" id="rememberMe">-->
<!--                        <label class="form-check-label" for="rememberMe">Remember me</label>-->
<!--                      </div>-->
<!--                    </div>-->
                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit">Login</button>
                    </div>
                  </form>
                </div>
              </div>

            </div>

          </div>
        </div>
    </div>
    </section>

    </div>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php include './../../layout/footer.html'; ?>
  <!-- End Footer -->
  <!-- Vendor JS Files -->
  <?php include './../../layout/js.html' ?>

  <!-- Template Main JS File -->
  <script>
    $('#login_form').submit(function(e) {
      let value = $('#login_form').serializeArray();
      if (e.isDefaultPrevented()) return;
      e.preventDefault()
      var responses = formAjaxHy('/api/user/login', value, 'post');

      if (responses.payload.code == 200) {
          localStorage.userinfo =  responses.payload.data;
        window.location.href = '../index/index'
      } else {
         commonUtil.message('Access or Password is Error!')
      }
      return false; //Block page refresh
    })
  </script>
</body>

</html>