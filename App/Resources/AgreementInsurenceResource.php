<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\AgreementInsurenceModel;

class AgreementInsurenceResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new AgreementInsurenceModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'insurenceType' => ['required'],
            'insurenceCost' => ['required', 'numeric'],
        ];
        $messages = [
            'insurenceType.required' => 'The insurenceType field is required.',
            'insurenceCost.required' => 'The insurenceCost field is required.',
            'insurenceCost.numeric' => 'The insurenceCost field must be a number.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new AgreementInsurenceModel;
        $resultCode = $this->baseUpdate($newModel, $data, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new AgreementInsurenceModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
