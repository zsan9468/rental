<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/28
 * Time:  16:58
 */

namespace App\Models;

use App\Lib\DB;

class CostcenterSelectionModel
{
    protected $table = 'costcenterselection';


    public function cerate($data)
    {
        return DB::link()->table($this->table)->insert($data);
    }
    public function checkCostcenterSelection($data)
    {
        $whereSql = vsprintf("costCenterId = %s and  userId = %d",array($data['costCenterId'],$data['userId']));
        $result =  DB::link()->table($this->table)->where($whereSql)->get();
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function getInfo($where)
    {
        return DB::link()->table($this->table)->where($where)->get();
    }
    public function update($cost_id,$userId)
    {
        DB::link()->table($this->table)->where("userId = $userId")->update(array('costCenterId'=>$cost_id));
    }
}