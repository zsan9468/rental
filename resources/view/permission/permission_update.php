<div class="modal fade" id="update_permission_model" tabindex="-1">
    <div class="modal-dialog modal-dialog-scrollable  modal-lg">
        <div class="modal-content" style="width: 1000px; height: auto">
            <div class=" modal-body">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Update  permission……</h5>
                        <p>Please Update permission  Info</text></p>
                        <form class="row g-3 was-validated" id="update_permission" novalidate>

                            <div class="col-md-8">
                                <label for="menu_name" class="form-label">menu_name</label>
                                <input type="text" class="form-control" id="update_menu_name" name="menu_name" value="">
                                <input type="hidden"  id="id" name="id" value="" >
                            </div>
                            <div class="row col-md-8" style="margin-top: 1rem">
                                <label class="col-md-4 col-form-label">Level</label>
                                <div class="col-sm-9">
                                    <select id="father_id" name="father_id" class="form-select father_id">
                                    </select>
                                </div>
                            </div>
                            <div class="row col-md-8" style="margin-top: 1rem">
                                <label class="col-md-4 col-form-label">Type</label>
                                <div class="col-sm-9">
                                    <select id="type" name="type" class="form-select type">
                                        <option value='1'>menu</option>
                                        <option value='2'>Operation</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <label for="menu_icon" class="form-label">menu_Id</label>
                                <input type="text" class="form-control menu_icon" id="menu_icon" name="menu_icon" value=""
                                >
                            </div>
                            <div class="col-md-8">
                                <label for="menu_url" class="form-label">menu_url</label>
                                <input type="text" class="form-control menu_url" id="update_menu_url" name="menu_url" value=""
                                >
                            </div>
                            <div class="row col-md-8" style="margin-top: 1rem">
                                <label class="col-md-4 col-form-label">the way of submission</label>
                                <div class="col-sm-9">
                                    <select id="method" name="method" class="form-select method">
                                        <option value='0'>--No method--</option>
                                        <option value='POST'>POST</option>
                                        <option value='GET'>GET</option>
                                        <option value='DELETE'>DELETE</option>
                                        <option value='PUT'>PUT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="menu_order" class="form-label">order</label>
                                <input type="number" class="form-control" id="menu_order" name="menu_order" value=""
                                >
                            </div>




                            <div class="col-12">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </form><!-- End Custom Styled Validation -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Modal Dialog Scrollable-->
