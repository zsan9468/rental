<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class RentalDamageModel extends BaseModel
{
    protected $table = 'damages';
    protected $tableProperties = 'damageproperties';
    protected $tableRentalUnit = 'rentalunits';
    protected $tableUnitNames = 'unitnames';
    protected $tableStatusTyp = 'statustyp';

    public function getIndex($start, $length, $search, $order)
    {
        if ($search == '') {
            $whereSql = '';
        } else {
            $whereSql = vsprintf(" where damageStatus REGEXP '%s' or un.name  REGEXP '%s' ", array($search, $search));
        }
        $limitSql = 'LIMIT ' . $start . ',' . $length;
        $getLimitSql = "select d.id,d.rentalUnitsId,dp.id as damagepropertiesId,up.priceGroup ,ut.typName,un.name,d.damageStatus,d.damageImgPath,d.repairDateTime,d.damageTime,dp.damageName,ul.number,un.name as unit_name,ul.location
                 from 
                damages as d 
                LEFT JOIN damageproperties as dp on dp.id = d.damagePropertiesId
                LEFT JOIN rentalunits as ru  on d.rentalUnitsId = ru.id
                LEFT JOIN unitnames as un on un.id = ru.unitNamesId
                LEFT JOIN unittyp as ut on ut.id = ru.unitTypId
                LEFT JOIN unitprice as up on up.unitTypId = ut.id
                LEFT JOIN unitlocation as ul on ul.id = ru.unitLocationPropertiesId 
            {$whereSql}
            ORDER BY {$order}
            {$limitSql}";
        $result['data'] = DB::link()->query($getLimitSql);

        $countSql = "SELECT  count(*) as count
             FROM damages";
        $result['allData'] = DB::link()->query($countSql);
        $toTal =   $result['allData'][0]['count'];

        $returnArray['recordsFiltered'] = (int)$toTal;;
        $returnArray['data'] = $result['data'];
        $returnArray['recordsTotal'] = count($result['data']);

        return $returnArray;
    }

    public function getOne($id)
    {
        return DB::link()->table($this->table)->where("id =$id")->get();
    }

    public function postItem($data, $id)
    {
        try {
            // RentalStatusTyp: 1 free, 2 rented, 3 on repair
            // DamageStatus: 1 on repair, 2 repaired
            if (isset($data['id'])) {
                // update
                // Before updating or deleting, you should check whether there is data
                $ReturnBool = false;
                $id = $data['id'];
                $oldData = DB::link()->table($this->table)->where("id = $id")->field('id')->get();
                if (!empty($oldData)) {
                    $ReturnBool = DB::link()->table($this->table)->where("id = $id")->update($data);
                    if ($ReturnBool && $data['damageStatus'] == 2) {
                        // Update the status of the rentalunit
                        $rentalUnitsId = $data['rentalUnitsId'];
                        $isHaveOnRepair = DB::link()->table($this->table)->where("rentalUnitsId = $rentalUnitsId AND damageStatus = 1")->field('id')->get();
                        if (empty($isHaveOnRepair)) {
                            $statusTypFreeValue = config('rentalStatusTyp')[1];
                            $statustypData = DB::link()->table($this->tableStatusTyp)->where("type = '$statusTypFreeValue'")->field('id')->get();
                            if (isset($statustypData[0]['id'])) {
                                $updateWhereSql = "id = $rentalUnitsId";
                                $updateData['statusTypId'] = $statustypData[0]['id'];
                                $ReturnBool = DB::link()->table($this->tableRentalUnit)->where($updateWhereSql)->update($updateData);
                            } else {
                                throw new PDOException("Invalid statusType Table data, '$statusTypFreeValue' value not found");
                            }
                        }
                    }
                }
            } else {
                // insert
                $ReturnBool = DB::link()->table($this->table)->insert($data);
                // $ReturnBool = DB::link()->table($this->table)->dd('insert', $data);
                if ($ReturnBool) {
                    // Update the status of the rentalunit
                    $statusTypOnrepairValue = config('rentalStatusTyp')[3];
                    $statustypData = DB::link()->table($this->tableStatusTyp)->where("type = '$statusTypOnrepairValue'")->field('id')->get();
                    if (isset($statustypData[0]['id'])) {
                        $rentalUnitsId = $data['rentalUnitsId'];
                        $updateWhereSql = "id = $rentalUnitsId";
                        $updateData['statusTypId'] = $statustypData[0]['id'];
                        $ReturnBool = DB::link()->table($this->tableRentalUnit)->where($updateWhereSql)->update($updateData);
                    } else {
                        throw new PDOException("Invalid statusType Table data, '$statusTypOnrepairValue' value not found");
                    }
                }
            }
        } catch (PDOException $e) {
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function deleteItem($id)
    {
        try {
            $rentalUnitsIdData = DB::link()->table($this->table)->where("id = $id")->field('rentalUnitsId')->get();
            $ReturnBool = DB::link()->table($this->table)->where("id = $id")->delete();
            // Update the status of the rentalunit
            if (isset($rentalUnitsIdData[0]['rentalUnitsId'])) {
                $rentalUnitsId = $rentalUnitsIdData[0]['rentalUnitsId'];
                $isHaveOnRepair = DB::link()->table($this->table)->where("rentalUnitsId = $rentalUnitsId AND damageStatus = 1")->field('id')->get();
                if (empty($isHaveOnRepair)) {
                    $statusTypFreeValue = config('rentalStatusTyp')[1];
                    $statustypData = DB::link()->table($this->tableStatusTyp)->where("type = '$statusTypFreeValue'")->field('id')->get();
                    if (isset($statustypData[0]['id'])) {
                        $updateWhereSql = "id = $rentalUnitsId";
                        $updateData['statusTypId'] = $statustypData[0]['id'];
                        $ReturnBool = DB::link()->table($this->tableRentalUnit)->where($updateWhereSql)->update($updateData);
                    } else {
                        throw new PDOException("Invalid statusType Table data, '$statusTypFreeValue' value not found");
                    }
                }
            }
        } catch (PDOException $e) {
            FileLogger::error('Error deleting data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }
}
