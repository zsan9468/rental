<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalNameResource;

class RentalNameController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalNameResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalNameResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalNameResource)->destroy($data);
    }
}
