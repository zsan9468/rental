<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title "><text class="lang" key="rentalUnits.Unitnames_title">Unitnames_title</text>  <span class="lang" key="list.title"> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary lang" id="statustyp_add"  key="rentalUnits.Create Unitnames">
                                            Create Unitnames
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="unitname_table" class="hover table-striped" style="width:100%">-->
                                <table id="unitname_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="statustyp__add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.unitnames">Create New
                            Unitnames</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'unitnames_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="unitname_update" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.unitnames">Create New
                            Unitnames</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'unitnames_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'name', title: "name", name: "name" },
            { data: 'manufacture', title: "manufacture", name: "manufacture" },
            { data: 'Action', title: "Action", name: "description", "orderable" : false,render: (data, type, row) => {

                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    console.log(row)
                    $('#unitname_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#unitname_table').on('click', 'tbody #' + upodateBtn, function (e) {

                        $('#unitname_update').modal('show');
                        $("#unitname_form_update input[name=id]").val(row.id);
                        $("#unitname_form_update input[name=manufacture]").val(row.manufacture);
                        $("#unitname_form_update input[name=name]").val(row.name);
                    })

                    $('#unitname_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#unitname_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_name');

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_name', 'get', 'unitname_table', columns,);

        $('#statustyp_add').click(function (e) {
            $('#statustyp__add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#unitname_form_create').submit(function (e) {
            var data =  $.unitnameBiaodan('unitname_form_create',e,'create')

            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_name','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#statustyp__add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#unitname_form_update').submit(function (e) {
            var id = $("#unitname_form_update input[name=id]").val();
            var data =  $.unitnameBiaodan('unitname_form_update',e,'update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_name','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#unitname_update').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>