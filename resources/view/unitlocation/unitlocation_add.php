
<form class="row g-3 was-validated" id="unitlocation_form_create" novalidate>
<!--    <form class="row g-3 was-validated" id="ccustomer_properties_form_create" novalidate>-->

    <div class="col-md-12">
        <label for="description" class="form-label">Description</label>
        <input type="text" class="form-control" id="description" name="description" value="" required>
        <div class="valid-feedback">
            Description Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            propertyName provide a Description
        </div>
    </div>

    <div class="col-md-12">
        <label for="location" class="form-label">Location</label>
        <input type="text" class="form-control" id="propelocationrtyDescription" name="location" value="" >
    </div>


        <div class="col-md-6">
            <label for="number" class="form-label">Number</label>
            <input type="text" class="form-control" id="number" name="number" value="" >
        </div>

        <div class="col-md-12">
            <label for="remarks" class="form-label">remarks</label>
            <textarea type="text" class="form-control remarks" id="remarks"  value="" ></textarea>
        </div>


        <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
                           