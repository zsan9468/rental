<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\GeneralResource;

class GeneralController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new GeneralResource)->index($data);
    }

    public function post($id = null)
    {
        $data = $this->getQueryJson();
        (new GeneralResource)->update($data, $id);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new GeneralResource)->destroy($data);
    }
}
