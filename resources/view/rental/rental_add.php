<form class="row g-3 was-validated" id="rental_form_create" novalidate>

    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label lang" key="rental.choose.unitname" >UnitName</label>
        <div class="col-sm-8">
            <select id="unitNamesId" name="unitNamesId" class="form-select">
            </select>
        </div>
    </div>
    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label lang" key="rental.choose.statustyp">statusTyp</label>
        <div class="col-sm-8">
            <select id="statusTypId" name="statusTypId" class="form-select">
            </select>
        </div>
    </div>
    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label lang" key="rental.choose.unitlocation">unitLocation</label>
        <div class="col-sm-8">
            <select id="unitLocationPropertiesId" name="unitLocationPropertiesId" class="form-select">
            </select>
        </div>
    </div>


    <div class="col-md-6">
        <label for="typName" class="form-label lang" key="rental.create.typename">Create a rental Type Name</label>
        <input type="text" class="form-control" id="typName" name="typName" value="" required >
        <div class="valid-feedback lang" key="correct.prompt">
            Type Name Data Entry Ok !
        </div>
        <div class="invalid-feedback lang" key="error.prompt">
            Type Nam provide a lastName
        </div>
    </div>

    <div class="col-md-12">
        <label for="typDescription" class="form-label lang" key="rental.create.typdescription">Please give a description of the Type Name</label>
        <textarea type="text" class="form-control" name ="typDescription" id="typDescription"  value="" ></textarea>
    </div>

    <div class="col-md-12">
        <label for="" class="form-label"> <text class="text-primary lang" key="rental.create.rentalproperties"> Enter the properties of the rental item</text></label>
        <div class="Rental_properties">

        </div>

    </div>


    <div class="col-md-5">
        <label for="priceGroup" class="form-label lang" key="rental.create.pricegroup">Fill in the priceGroupof the rental item</label>
        <input type="text" class="form-control" id="priceGroup" name="priceGroup" value=""   required>
        <div class="valid-feedback lang" key="correct.prompt">
            priceGroup Data Entry Ok !
        </div>
        <div class="invalid-feedback lang" key="error.prompt">
            Please provide a priceGroup
        </div>
    </div>

    <div class="col-md-4">
        <label for="typName" class="form-label lang" key="rental.create.price">Price</label>
        <input type="text" class="form-control" id="price" name="price" value=""   required>
        <div class="valid-feedback lang" key="correct.prompt">
            Price Data Entry Ok !
        </div>
        <div class="invalid-feedback lang" key="error.prompt">
            Please provide a Price
        </div>
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
                           