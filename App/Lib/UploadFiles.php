<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/3/7
 * Time:  11:12
 */

namespace App\Lib;

class UploadFiles
{

    /**
     * @param $fileData
     * @return void
     * public/upload/img/damage/
     */
    public function  uploadFilesBase64($fileData, $path)
    {
        if (preg_match('/^(data:\s*image\/(jpeg|jpg|png|gif|x-icon);base64,)/', $fileData, $result)) {
            $type = $result[2];
            if ($type == 'x-icon') {
                $type = 'ico';
            }
            $pathDir = $path . date('Ymd', time()) . "/";
            if (!is_dir($pathDir)) {
                mkdir($pathDir, 0777, true);
            }
            $new_file =  $pathDir . time() . '.' . $type;
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $fileData)))) {
                return $new_file;
            } else {
                return false;
            }
        }
    }


    /**
     * GD库自动生成用户图片
     */
    public function createRandProfilePhoto($path,$a = '',$c = '')
    {
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $text = substr($a,0,2);
        $im = imagecreate(200,200);
        $bg = imagecolorallocate($im,rand(1,254),rand(1,254),rand(1,254));
        imagefill($im,0,0,$bg);
        $text_color = imagecolorallocate($im, rand(1,254), rand(1,254), rand(1,254));
        imagefttext($im,90,0,40,140,$text_color,$_SERVER['DOCUMENT_ROOT'].'/public/font/arial.ttf',$text);
        $pathNew = $path.'/'.$this->randomkeys(9).time().'.png';
        imagepng($im,'./'.$pathNew);
        imagedestroy($im);
        return $pathNew;
    }

    /**
     * @param $length
     * @return $returnStr
     * 生成随随机数
     */
    function randomkeys($length) {
        $returnStr ='';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for ( $i = 0; $i < $length; $i++ ) {
            $returnStr .= $chars[ mt_rand(0, strlen($chars) - 1) ];
        }
        return $returnStr;
    }
}


