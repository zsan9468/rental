<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">Rental Properties  <span class="lang" key="list.title"> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary lang" id="customer_properties_add">
                                            Rental Properties
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="rentalproperties_table" class="hover table-striped" style="width:100%">-->
                                <table id="rentalproperties_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="rentalproperties_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Create New Rental Property ……</h5>
                                <p>Please Create a New  Rental Property </p>
                                <?php include 'rentalproperties_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="rentalproperties_update_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Update New  Rental Property ……</h5>
                                <p>Please Update a  Rental Property </p>
                                 <?php include 'rentalproperties_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {
        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'propertieName', title: "propertieName", name: "propertieName" },
            { data: 'propertyDescription', title: "propertyDescription", name: "propertyDescription" },
            { data: 'Action', title: "Action", name: "propertieName", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#rentalproperties_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#rentalproperties_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('#rentalproperties_update_model').modal('show');
                        // var responses = formAjaxHy('/api/rental_properties?id='+row.id, data, 'get');
                        $("#rentalproperties_form_update input[name=id]").val(row.id);
                        $("#rentalproperties_form_update input[name=propertieName]").val(row.propertieName);
                        $("#rentalproperties_form_update input[name=propertyDescription]").val(row.propertyDescription);
                    })

                    $('#rentalproperties_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#rentalproperties_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_properties');

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_properties', 'get', 'rentalproperties_table', columns,);

        $('#customer_properties_add').click(function (e) {
            $('#rentalproperties_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#rentalproperties_form_create').submit(function (e) {
            var data =  $.rentalpropertiesBiaodan('rentalproperties_form_create',e,'create')
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_properties','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#rentalproperties_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#rentalproperties_form_update').submit(function (e) {
            var id = $("#rentalproperties_form_update input[name=id]").val();
            var data =  $.rentalpropertiesBiaodan('rentalproperties_form_update',e,'update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_properties','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#rentalproperties_update_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>