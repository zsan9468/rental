<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/24
 * Time:  12:49
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\CostCenterModel;
use App\Models\CostcenterSelectionModel;

class CostCenterResource
{
    /**
     * Display a listing of the resource.
     * @param $dataPar
     * @return void
     */
    public function index($params)
    {
        $search = '';
        $start = 0;
        $length = 1000;
        if (empty($params)) {
            ApiOutputController::ApiOutput([], 501, '');
        }

        $start = $params['start'];
        $length = $params['length'];
        $search = $params['search']['value'];

        $field = $params['columns'][$params['order'][0]['column']]['data'];
        $collation  = $params['order'][0]['dir'];
        $order = "$field $collation";

        $costCenterModel = new CostCenterModel();
        $result = $costCenterModel->getCostCenterList($start, $length, $search, $order);
        $result['draw'] = (int)$params['draw'];
        if ($result) {
            return $result;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store($requestData)
    {
        $rules = [
            'name' => ['required'],
            'street1' => ['required'],
            'postalCode' => ['required', 'numeric'],
            'city' => ['required'],
        ];
        $messages = [
            'name.required' => 'The name field is required.',
            'street1.required' => 'The street1 field is required.',
            'postalCode.required' => 'The postalCode field is required.',
            'postalCode.numeric' => 'The postalCode field must be a number',
            'city.required' => 'The city field is required.',
        ];
        $validator = new \App\Lib\Validator($requestData, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $costCenterModel = new CostCenterModel();
        $result = $costCenterModel->create($requestData);
        if ($result) {
            ApiOutputController::ApiOutput([], 200, '');
        }
        ApiOutputController::ApiOutput([], 502, '');
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     */
    public function getAllCostCenter()
    {
        $costCenterModel = new CostCenterModel();
        $costCerterArray = $costCenterModel->getCostCenterList();
        return $costCerterArray;
    }

    /**
     * Update the specified resource in storage.
     * @param  int  $id
     */
    public function update($data)
    {
        if (empty($data['id'])) {
            ApiOutputController::ApiOutput([], 503, '');
        }

        $rules = [
            'name' => ['required'],
            'street1' => ['required'],
            'postalCode' => ['required', 'numeric'],
            'city' => ['required'],
        ];
        $messages = [
            'name.required' => 'The name field is required.',
            'street1.required' => 'The street1 field is required.',
            'postalCode.required' => 'The postalCode field is required.',
            'postalCode.numeric' => 'The postalCode field must be a number',
            'city.required' => 'The city field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $costCenterModel = new CostCenterModel();
        $result = $costCenterModel->update($data);
        if ($result) {
            ApiOutputController::ApiOutput([], 200, '');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id

     */
    public function destroy($data)
    {
        if (empty($data['id'])) {
            ApiOutputController::ApiOutput([], 503, '');
        }

        $UserModel = new CostcenterSelectionModel();
        $result = $UserModel->getInfo("costCenterId = {$data['id']} ");
        if ($result) {
            ApiOutputController::ApiOutput('', 506, '');
        }
        $costCenterModel = new CostCenterModel();
        $result = $costCenterModel->delete($data['id']);
        if ($result) {
            ApiOutputController::ApiOutput([], 200, '');
        }
    }
}
