<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">Unit Location<span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary" id="unitlocation_add">
                                            Create Unit Location
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="unitlocation_table" class="hover table-striped" style="width:100%">-->
                                <table id="unitlocation_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="unitlocation_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.nitlocation">Create New
                            Location</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'unitlocation_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="unitlocation_update_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.nitlocation">Update New
                            Location</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'unitlocation_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'description', title: "description", name: "description" },
            { data: 'location', title: "location", name: "location" },
            { data: 'number', title: "number", name: "number" },
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#unitlocation_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#unitlocation_table').on('click', 'tbody #' + upodateBtn, function (e) {

                        $('#unitlocation_update_model').modal('show');
                        $("#unitlocationBiaodan_form_update input[name=id]").val(row.id);
                        $("#unitlocationBiaodan_form_update input[name=location]").val(row.location);
                        $("#unitlocationBiaodan_form_update input[name=number]").val(row.number);
                        $("#unitlocationBiaodan_form_update input[name=description]").val(row.description);
                        $("#unitlocationBiaodan_form_update .remarks").val(row.remarks);
                    })

                    $('#unitlocation_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#unitlocation_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_location');
                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_location', 'get', 'unitlocation_table', columns,);

        $('#unitlocation_add').click(function (e) {
            $('#unitlocation_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#unitlocation_form_create').submit(function (e) {
            var data =  $.unitlocationBiaodan('unitlocation_form_create',e,'create')
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_location','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#unitlocation_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })
        $('#unitlocationBiaodan_form_update').submit(function (e) {
            var id = $("#unitlocationBiaodan_form_update input[name=id]").val();
            var data =  $.unitlocationBiaodan('unitlocationBiaodan_form_update',e,'update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_location','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#unitlocation_update_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>