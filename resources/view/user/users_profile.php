<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>
<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php';?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php';?>

<main id="main" class="main">
        <div class="pagetitle">
            <h1>Profile</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/web/index/index">Home</a></li>
                    <li class="breadcrumb-item">Users</li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </nav>
        </div><!-- End Page Title -->

        <section class="section profile">
            <div class="row">
                <div class="col-xl-4">

                    <div class="card">
                        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                            <img src="/resources/assets/img/profile-img.jpg" alt="Profile" class=" profile_photo rounded-circle">
                            <h2 class="overview_name"></h2>
                            <h3>Login ID:  <text class="overview_username"> </text></h3>
                        </div>
                    </div>

                </div>

                <div class="col-xl-8">

                    <div class="card">
                        <div class="card-body pt-3">
                            <!-- Bordered Tabs -->
                            <ul class="nav nav-tabs nav-tabs-bordered">

                                <li class="nav-item">
                                    <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                                </li>

                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                                </li>


                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                                </li>

                            </ul>
                            <div class="tab-content pt-2">
                                <?php include 'overview.php' ?>
                                <?php include 'edit_profile.php' ?>
                                <?php include 'changePassword.php' ?>
                            </div><!-- End Bordered Tabs -->

                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main><!-- End #main -->


<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {
        /**
         * 上传头像
         */
        $("#profile_photo_btn").change(function () {
            $('.profile_photo_show').attr('src','');
            $.getImageBase(this, 'profile_photo_show', 'profile_photo')
        });


        var pwd = $("#newPassword");
        var eye = $("#chageTypePass");
        $("#chageTypePass").click(function (){
            if (pwd.attr("type") == "text") {
                pwd.attr("type", "password");
                eye.attr("class", "bi-eye-slash-fill");
            } else {
                pwd.attr("type", "text");
                eye.attr("class", "bi-eye-slash");
            }
        })

            $.ajaxUnit('/api/user_action','get',{'details':true},
                function (data){
                    if(data.payload.code == 200){
                        console.log(data.payload.data.userInfo)
                        $(".overview_name").text(data.payload.data.userInfo.name)
                        $(".overview_username").text(data.payload.data.userInfo.username)
                        $(".overview_surname").text(data.payload.data.userInfo.surname)
                        $(".overview_email").text(data.payload.data.userInfo.email)
                        $(".overview_city").text(data.payload.data.userInfo.city)
                        $(".overview_postalCode").text(data.payload.data.userInfo.postalCode)
                        $(".overview_costName").text(data.payload.data.userInfo.costName)
                        $("#edit_user_info input[name=name]").val(data.payload.data.userInfo.name)
                        $("#edit_user_info input[name=surname]").val(data.payload.data.userInfo.surname)
                        $("#edit_user_info input[name=userId]").val(data.payload.data.userInfo.userId)
                        $("#edit_user_info input[name=email]").val(data.payload.data.userInfo.email)
                        $("#edit_user_info input[name=email]").val(data.payload.data.userInfo.email)


                        if(data.payload.data.userInfo.profile_photo){
                            $('#edit_user_info .profile_photo_show').attr('src',host+'/'+data.payload.data.userInfo.profile_photo);
                        }
                        if(data.payload.data.userInfo.user_type == 1)
                        {
                            $(".overview_user_type").text("Super Admin")
                        }else if (data.payload.data.userInfo.user_type == 2){
                            $(".overview_user_type").text("Admin")
                        }else{
                            $(".overview_user_type").text("EMPLOYEE USER")
                        }
                    }else{
                         commonUtil.message(data.payload.data,'danger');
                    }
                },
                function (data)
                {
                    console.log(data)
                }
            )


        $('#edit_user_info').submit(function (e){
            var name = $("#edit_user_info input[name=name]").val();
            var userId = $("#edit_user_info input[name=userId]").val();
            var surname = $("#edit_user_info input[name=surname]").val();
            var email = $("#edit_user_info input[name=email]").val();
            var modify_type = $("#edit_user_info input[name=modify_type]").val();
            var profile_photo = $("#edit_user_info input[name=profile_photo]").val();

            // console.log(e.preventDefault())

            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            var data = {
                'userId':userId,
                'name': name,
                'surname': surname,
                'email': email,
                'modify_type':modify_type,
                'profile_photo':profile_photo
            };
            $.ajaxUnit('/api/user_action','put',data,
                function(data){
                    if(data.payload.code == 200){
                        localStorage.userinfo =  data.payload.data;
                        commonUtil.message('Set success！Refresh after 1 seconds');
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);

                    }else{

                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })
            $('.chanagePassword').submit(function (e){
                e.preventDefault();
                var password = $(".chanagePassword input[name=password]").val();
                var newpassword = $(".chanagePassword input[name=newpassword]").val();
                var renewPasswords = $(".chanagePassword input[name=renewpassword]").val();
                if(renewPasswords !== newpassword){
                     commonUtil.message('Password inconsistency!','danger');
                    return false;
                }
                var data = {
                    'password': password,
                    'newpassword': newpassword,
                    'renewpassword': renewPasswords,
                };
                $.ajaxUnit('/api/user_changepassword','post',data,
                        function(data){
                            if(data.payload.code == 200){
                                 commonUtil.message("updated successfully，please login again！")
                                window.location.href = '/web/login/login';
                            }else{
                                 commonUtil.message(data.payload.data,'danger');
                            }
                        },function(data){
                           console.log(data)
                        })
                })
            })

</script>
</body>

</html>