 <form class="row g-3 was-validated" id="damages_properties_form_create" novalidate>

    <div class="col-md-4">
        <label for="damageName" class="form-label">damageName</label>
        <input type="text" class="form-control" id="damageName" name="damageName" value="" required>
        <div class="valid-feedback lang" key="Correct prompt">
            Correct input format
        </div>
        <div class="invalid-feedback lang" key="Error prompt">
            The input format is incorrect
        </div>
    </div>

    <div class="col-md-4">
        <label for="damageDescription" class="form-label" >damageDescription</label>
        <input type="text" class="form-control" id="damageDescription" name="damageDescription" value="" >
        <div class="valid-feedback lang" key="Correct prompt">
            Correct input format
        </div>
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
                           