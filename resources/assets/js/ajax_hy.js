// author：haiyang Wang
// date：Thu Feb 23 2023 18:54:24 GMT+0800 (中国标准时间)
// Encapsulate Ajax request and unify url control
// 封装Ajax请求，统一url控制
// var host = 'http://admin.rental.com';
var formAjaxHy = function (url,parameters,submit_type) {
    let dataJson = {};
    $.each(parameters, function (index, item) {
        dataJson[item.name] = item.value;
       if(item.name == undefined)
       {
           dataJson[index] = item;
       }
    });
    if($.isEmptyObject(dataJson))
    {
        var  new_data = ''
    }else{
        var  new_data= JSON.stringify(dataJson);
    }
    var response ='';
      $.ajax({
        type:submit_type,
        url:host + url,
        data:new_data,
        dataType:"json",
        async: false,
        success: function(callback){
            response =  callback ;
        }
       });
       return response
};

var table;
var tableAjax = function (url,submit_type,dmo,columns) {


     table = $("#"+dmo).DataTable({
        stateSave: false,
        serverSide: true,
        processing: true,
        autoWidth: false,
        "pageLength":8,
         "lengthMenu": [[4, 8, 10], [4, 8, 10]],
         // "lengthMenu": [[4, 8, 10, -1], [4, 8, 10, "All Data"]],
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers",
        "searching": true,
        "jQueryUI": true,
        ajax: {
            url:host + url,
            "dataType": 'json',
            type: submit_type,
            dataSrc: function (data) {
                data.draw = data.payload.data.draw;
                data.recordsTotal =  data.payload.data.recordsTotal;
                data.recordsFiltered =  data.payload.data.recordsFiltered
                return data.payload.data.data
            }
        },
        columns: columns
    });
};

var get_user_correlation = function (responses,menu='',update_cost_center=''){
    if (responses.payload.data) {
        var data = responses.payload.data.costCerter;
        var items = [];
        for (var i = 0, len = data.length; i < len; i++) {
            var itemdata = data[i];
            items.push('<option value="' + itemdata.id + '">' + itemdata.name + '</option>')
        }

        $('#'+update_cost_center).html(items.join(' '))
        var permissionDatas = [];
        var perdata = responses.payload.data.permissions;
        for (var j = 0, perlen = perdata.length; j < perlen; j++) {
            var peritemdata = perdata[j];
            permissionDatas.push(" <div class='form-check col-md-12'>" +
                "<input class='form-check-input  father' type='checkbox' name='permission[]' value='"+peritemdata.info.id+"'>" +
                "<label class='form-check-label text-primary' for='gridCheck1'>" +peritemdata.info.menu_name+
                "</label>")
            if (peritemdata.son != '') {
                var sonArr = [];
                var sonDatas = peritemdata.son;
                permissionDatas.push(" <div class='row md-12'>")
                for (var z = 0, sonlenth = sonDatas.length; z < sonlenth; z++) {
                    var sonPerData = sonDatas[z];
                    permissionDatas.push("<div class='form-check col-md-3' >" +
                        "<input class='form-check-input son' type='checkbox' name='permission[]'  value='"+sonPerData.info.id+"'>" +
                        "<label class='form-check-label' for='gridCheck1'>" + sonPerData.info.menu_name +
                        "</label>" +
                        "</div>")
                }
                permissionDatas.push("</div>")
            }
            permissionDatas.push("</div>")
        }
        $('.'+menu).html(permissionDatas.join(' '));
    }

}
// Ajax request wrapper
$.extend({
    ajaxUnit:function(url,type,data,success,error){
        if(type.toLowerCase()=='post' || type.toLowerCase()=='put' ){
            data = pack(data)
        }
        $.ajax({
            type: type,
            url: host + url,
            contentType:'application/json',
            data:data,
            // beforeSend: function (XMLHttpRequest) {
            //     XMLHttpRequest.setRequestHeader("X-Token", sessionStorage.token)
            // },
            success: function(data){
                success(data)
            },
            error: function(data){
                error(data)
            },
            dataType: "json"
        })
    }
})

//对象序列化
function pack(data) {
    return JSON.stringify(data)
}

function unpack(data) {
    return JSON.parse(data)
}

function Prevented(e){
    if (e.isDefaultPrevented()) {
        return;
    }
    e.preventDefault()
    return false;
}
// Add a property box// Ajax request wrapper
$.extend({
    checkUSerFild:function ($data,e,type)
    {
        $.each( $data, function( i, val ) {
            if(val.name=='username')
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    console.log(111)
                    commonUtil.message('usename must exist','danger')
                    Prevented(e)
                }
            }
            if(type == 'create'){
                if(val.name == 'password')
                {
                    if(val.value != ''){
                        $data[i].value = (val.value).trim();
                        if(($data[i].value.length < 6)){
                            commonUtil.message('password  At least 6 characters','danger')
                            Prevented(e)
                            return false;
                        }
                    }else{
                        commonUtil.message('password must exist','danger')
                        Prevented(e)
                    }
                }
            }

            if(val.name=='name')
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    commonUtil.message('name must exist','danger')
                    Prevented(e)
                }
            }


        });
    },
    checkCostCenter:function ($data,e,type)
    {
        console.log($data)
        $.each( $data, function( i, val ) {
            if(val.name=='name')
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    console.log(111)
                    commonUtil.message('name must exist','danger')
                    return false;
                    Prevented(e)
                }
            }
            if(val.name=='name')
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    commonUtil.message('name must exist','danger')
                    return false;
                    Prevented(e)
                }
            }
            if(val.name=='postalCode')
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    commonUtil.message('postalCode must exist','danger')
                    return false;
                    Prevented(e)
                }
            }
            if(val.name=="city")
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    commonUtil.message('city must exist','danger')
                    return false;
                    Prevented(e)
                }
            }
            if(val.name=="street1")
            {
                if(val.value != ''){
                    $data[i].value = (val.value).trim();
                }else{
                    commonUtil.message('street1 must exist','danger')
                    return false;
                    Prevented(e)
                }
            }
        })
    }
})
$(document).on('click', '.deletedProperty', function(){
    $(this).parent().remove()
})

$.extend({
    customerInfoBiaodan :function(dom,e,type) {
        var firstName = $("#"+dom+" input[name=firstName]").val();
        var lastName = $("#"+dom+" input[name=lastName]").val();
        var birthDate = $("#"+dom+" input[name=birthDate]").val();
        var companyName = $("#"+dom+" input[name=companyName]").val();
        var street1 = $("#"+dom+" input[name=street1]").val();
        var street2 = $("#"+dom+" input[name=street2]").val();
        var city = $("#"+dom+" input[name=city]").val();
        var postalCode = $("#"+dom+" input[name=postalCode]").val();
        var telephone = $("#"+dom+" input[name=telephone]").val();
        var country = $("#"+dom+" input[name=country]").val();
        var mobile = $("#"+dom+" input[name=mobile]").val();
        var email = $("#"+dom+" input[name=email]").val();
        var rateCode = $("#"+dom+" input[name=rateCode]").val();
        var salesVolume = $("#"+dom+" input[name=salesVolume]").val();
        var remarks = $("#"+dom+" .remarks").val();
        let costumerPropertyNameArr = [];
        let customerPropertiesIdArr = [];
        let selectionIdArr  = []
        //验证 verification
        if(firstName != ''){
            firstName = firstName.trim();
        }else{
            commonUtil.message('firstName must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(lastName != ''){
            lastName = lastName.trim();
        }else{
            commonUtil.message('lastName must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(birthDate != ''){
            birthDate = birthDate.trim();
        }else{
            commonUtil.message('birthDate must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(street1 != ''){
            street1 = street1.trim();
        }else{
            commonUtil.message('street1 must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(city != ''){
            city = city.trim();
        }else{
            commonUtil.message('city must exist','danger')
            Prevented(e)
        }
//验证 verification
        if(country != ''){
            country = country.trim();
        }else{
            commonUtil.message('country must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(postalCode != ''){
            postalCode = postalCode.trim();
        }else{
            commonUtil.message('postalCode must exist','danger')
            Prevented(e)
        }
        $("input[name='customerPropertiesId']").each(function (i) {
            customerPropertiesIdArr.push($(this).val());
        });
        $("input[name='costumerPropertyName']").each(function (i) {
            costumerPropertyNameArr.push($(this).val());
        });
        if($("input[name='selectionId']")){
            $("#"+dom+" input[name='selectionId']").each(function (i) {
                selectionIdArr.push($(this).val());
            });
        }

        console.log(selectionIdArr);

        var customerproperties = [];
        console.log(customerPropertiesIdArr);
        console.log(costumerPropertyNameArr);

        $("input[name='customerPropertyValue']").each(function (i) {

            if(selectionIdArr.length>0){
                if(selectionIdArr[i] != ''){
                    var data = {
                        "selectionId": selectionIdArr[i],
                        "customerPropertyValue": $(this).val(),
                        "costumerPropertyName": costumerPropertyNameArr[i],
                        "customerPropertiesId": customerPropertiesIdArr[i]
                    }
                    customerproperties.push(data)
                }else{
                    var data = {
                        "customerPropertiesId": customerPropertiesIdArr[i],
                        "customerPropertyValue": $(this).val(),
                        "costumerPropertyName": costumerPropertyNameArr[i],
                    }
                    customerproperties.push(data)
                }
            }else{
                if($(this).val() != ''){
                    // console.log(customerPropertiesIdArr[i])
                    var data = {
                        "costumerPropertyName":costumerPropertyNameArr[i],
                        "customerPropertyValue": $(this).val(),
                        "customerPropertiesId": customerPropertiesIdArr[i]
                    }
                    customerproperties.push(data);
                }
            }




            console.log($(this).val());

        });
        console.log(customerproperties);
        var customer_data = {};
        customer_data.customer = {
            firstName:firstName,
            lastName:lastName ,
            birthDate:birthDate ,
            companyName:companyName ,
            street1:street1 ,
            street2:street2 ,
            country:country,
            city:city ,
            postalCode:parseInt(postalCode) ,
            telephone:parseInt(telephone) ,
            mobile:parseInt(mobile) ,
            email:email ,
            rateCode:parseInt(rateCode) ,
            salesVolume:parseInt(salesVolume) ,
            remarks:remarks ,
        }
        customer_data.customerproperties = customerproperties;

        return customer_data

    }
})

$.extend({
    gerena_InfoBiaodan :function(dom,e,type) {
        var greeting = $("#"+dom+" input[name=greeting]").val();
        var VATrate = $("#"+dom+" input[name=VATrate]").val();
        var invoiceText1 = $("#"+dom+" input[name=invoiceText1]").val();
        var invoiceText2 = $("#"+dom+" input[name=invoiceText2]").val();
        var invoiceText3 = $("#"+dom+" input[name=invoiceText3]").val();
        var paymentWarning1 = $("#"+dom+" input[name=paymentWarning1]").val();
        var paymentWarning2 = $("#"+dom+" input[name=paymentWarning2]").val();
        var paymentWarning3 = $("#"+dom+" input[name=paymentWarning3]").val();
        var offer1 = $("#"+dom+" input[name=offer1]").val();
        var offer2 = $("#"+dom+" input[name=offer2]").val();
        var offer3 = $("#"+dom+" input[name=offer3]").val();
        var date_format = $("#"+dom+" #date_format").val();
        var language_forbid = $("#"+dom+" #language_forbid").val();

        if(date_format != ''){
            localStorage.dateFormat = date_format;
        }else{
            commonUtil.message('date_format must exist','danger')
            Prevented(e)
        }
        if(language_forbid != ''){
            localStorage.language_forbid = language_forbid;
        }else{
            commonUtil.message('language_forbid must exist','danger')
            Prevented(e)
        }

        if(greeting != ''){
            greeting = greeting.trim();
        }else{
            commonUtil.message('greeting must exist','danger')
            Prevented(e)
        }
        //验证 verification
        if(VATrate != ''){
            VATrate = VATrate.trim();
        }else{
            commonUtil.message('VATrate must exist','danger')
            Prevented(e)
        }
        var customer_data = {};
        customer_data = {
            greeting:greeting,
            VATrate:VATrate ,
            invoiceText1:invoiceText1 ,
            invoiceText2:invoiceText2 ,
            invoiceText3:invoiceText3 ,
            paymentWarning1:paymentWarning1 ,
            paymentWarning2:paymentWarning2,
            paymentWarning3:paymentWarning3 ,
            offer1:offer1 ,
            offer2:offer2 ,
            offer3:offer3 ,
            date_format:date_format,
            language_forbid:language_forbid
        }
        return customer_data
    }
})

$.extend({
    customerpropertiesBiaodan :function(dom,e,type) {
        var propertyName = $("#"+dom+" input[name=propertyName]").val();
        var propertyDescription = $("#"+dom+" input[name=propertyDescription]").val();
        if(propertyName != ''){
            propertyName = propertyName.trim();
        }else{
            commonUtil.message('propertyName must exist','danger')
            Prevented(e)
        }
        if(propertyDescription != ''){
            propertyDescription = propertyDescription.trim();
        }else{
            commonUtil.message('propertyDescription must exist','danger')
            Prevented(e)
        }
        var customerproperties = {};
        customerproperties = {
            propertyName:propertyName,
            propertyDescription:propertyDescription ,
        }
        return customerproperties

    }
})
/**
 * 协议相关内容
 *
 */

$.extend({
    agreement_insurenceBiaodan:function (dom,e){
        var insurenceType = $("#"+dom+" input[name=insurenceType]").val();
        var insurenceCost = $("#"+dom+" input[name=insurenceCost]").val();
        if(insurenceType == ''){
            commonUtil.message('insurenceType Can not be empty');
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
        }
        if(insurenceCost == ''){
            commonUtil.message('insurenceCost Can not be empty');
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault();
        }
        var insurenceData = {};
         insurenceData = {
            insurenceType: insurenceType,
            insurenceCost: insurenceCost
        }
        return insurenceData;

    },
    agreementpropertiesBiaodan :function(dom,e) {
        var propertyName = $("#"+dom+" input[name=propertyName]").val();
        var propertyDescription = $("#"+dom+" input[name=propertyDescription]").val();
        var customerproperties = {};
        if(propertyName == ''){
            commonUtil.message('propertyName Can not be empty');
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
        }
        customerproperties = {
            propertyName:propertyName,
            propertyDescription:propertyDescription ,
        }
        return customerproperties

    },
    /**
     * 添加出租协议表单初始化
     * 初始化顾客列表
     * 初始化保险类型
     */
    agreementInitialize :function(dom,cloum) {
        var customerList = formAjaxHy('/api/customer', '', 'get');
        var agreement_insurence_list = formAjaxHy('/api/agreement_insurence_list', {}, 'get');
        var agreement_properties_list = formAjaxHy('/api/agreement_properties_list', {}, 'get');
        var customer_list =[];
        var insurence_list =[];
        var properties_list =[];
        var customerListData = customerList.payload.data;
        var insurenceData = agreement_insurence_list.payload.data;
        var propertiesData = agreement_properties_list.payload.data;
        if (customerListData.length>0){
            for(var i = 0 , length = customerListData.length ;i<length ;i++)
            {
                var itemdata = customerListData[i];
                customer_list.push('<option value="' + itemdata.id + '">' + itemdata.firstName + "  "+ itemdata.lastName  + '</option>')
                // customer_list.push('<option value="' + itemdata.id + '">11111</option>')
            }
            $('#'+dom+' #'+cloum).html(customer_list.join(' '));
        }
        if (insurenceData.length>0){
            for(var i = 0 , length = insurenceData.length ;i<length ;i++)
            {
                var itemdata = insurenceData[i];
                insurence_list.push('<option value="' + itemdata.id + '">' + itemdata.insurenceType +  '</option>')
            }
            $('#'+dom+' #insurenceId').html(insurence_list.join(' '));
        }else {

        }
        if (propertiesData.length>0){
            for(var i = 0 , length = propertiesData.length ;i<length ;i++)
            {
                var agreementPropertyarr = propertiesData[i];
                if(dom != 'agreement_update_form'){
                    $('#'+dom+'  .rentalAgreement_properties').append("<div class='row propertyNameClss'>"+
                        "<div class='col-md-3'>"+agreementPropertyarr.propertyName+"</div>"+
                        " <div class='col-md-4'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalAgreementPropertiesId' value='"+agreementPropertyarr.id+"'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalAgreementPropertyName' value='"+agreementPropertyarr.propertyName+"'>"+
                        "<input type='text' maxlength='11' class='form-control' id='rentalAgreementPropertyValue' name='rentalAgreementPropertyValue' value=''>"+
                        "</div>"+
                        " </div>")
                }else{
                    $('#'+dom+'  .rentalAgreement_properties').append("<div class='row propertyNameClss'>"+
                        "<div class='col-md-3'>"+agreementPropertyarr.propertyName+"</div>"+
                        " <div class='col-md-4'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalAgreementPropertiesId' value='"+agreementPropertyarr.id+"'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='selectionId' name='selectionId' value=''>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalAgreementPropertyName' value='"+agreementPropertyarr.propertyName+"'>"+
                        "<input type='text' maxlength='11' class='form-control' id='rentalAgreementPropertyValue' name='rentalAgreementPropertyValue' value=''>"+
                        "</div>"+
                        " </div>")
                }

            }
        }else {

        }
        console.log(customerList)
        console.log("-----")
        console.log(agreement_insurence_list)
        console.log("-----")
        console.log(agreement_properties_list)
    },

    rentalAgreementBiaodan:function (dom,e,type){
        var costumerId = $("#"+dom+" input[name=costumerId]").val();
        var rentalUnitsId = $("#"+dom+" input[name=rentalUnitsId]").val();
        var rentalBeginDateTime = $("#"+dom+" input[name=rentalBeginDateTime]").val();
        var rentalEndDateTime = $("#"+dom+" input[name=rentalEndDateTime]").val();
        var cost = $("#"+dom+" input[name=cost]").val();
        var insurenceId = $("#"+dom+" #insurenceId").val();
        // console.log(rentalBeginDateTime)
        // rentalBeginDateTime = rentalBeginDateTime.replace("/T/"," ");
        // console.log(rentalBeginDateTime)
        // Prevented(e)
        var agreementData = {};
        if(cost != ''){
            cost = cost.trim();
        }else{
            commonUtil.message('cost must exist','danger')
            Prevented(e)
        }
        var data = {
            costumerId :costumerId,
            rentalUnitsId :rentalUnitsId,
            rentalBeginDateTime :rentalBeginDateTime,
            rentalEndDateTime :rentalEndDateTime,
            cost :cost,
            insurenceId :insurenceId,
        }
        //额外属性
        var extraNameArr = [];
        $("#"+dom+" input[name='extraName']").each(function (i) {
            extraNameArr.push($(this).val());
        });
        var extraIdArr = [];
        $("#"+dom+" input[name='extraID']").each(function (i) {
            extraIdArr.push($(this).val());
        });

        var extraDescriptionArr = [];
        $("#"+dom+" input[name='extraDescription']").each(function (i) {
            extraDescriptionArr.push($(this).val());
        });
        var agreementextras = [];
        $("#"+dom+" input[name='extraPrice']").each(function (i) {
            if($(this).val() != '')
            {
                var extrasDataItem = {
                    extraName:extraNameArr[i],
                    extraDescription:extraDescriptionArr[i],
                    extraID:extraIdArr[i],
                    extraPrice:$(this).val()
                }
            }
            agreementextras.push(extrasDataItem)
        });

        // 租约属性
        var rentalAgreementPropertyNameArr = [];
        var rentalAgreementPropertiesIdArr = [];
        var selectionIdArr = [];
        var rentalAgreementPropertyValue =[];

        $("#"+dom+" input[name='rentalAgreementPropertyName']").each(function (i) {
            rentalAgreementPropertyNameArr.push($(this).val());
        });

        $("#"+dom+" input[name='rentalAgreementPropertyName']").each(function (i) {
            rentalAgreementPropertyNameArr.push($(this).val());
        });
        $("#"+dom+" input[name='rentalAgreementPropertiesId']").each(function (i) {
            rentalAgreementPropertiesIdArr.push($(this).val());
        });

        $("#"+dom+" input[name='selectionId']").each(function (i) {
            selectionIdArr.push($(this).val());
        });
        console.log(selectionIdArr.length)
        var agreementproperties = [];
        $("#"+dom+" input[name='rentalAgreementPropertyValue']").each(function (i) {

            if(selectionIdArr.length > 0){
                if($(this).val() != '')
                {
                    var agreementpropertiesItem = {
                        rentalAgreementPropertiesId:rentalAgreementPropertiesIdArr[i],
                        rentalAgreementPropertyName:rentalAgreementPropertyNameArr[i],
                        selectionId:selectionIdArr[i],
                        rentalAgreementPropertyValue:$(this).val()
                    }
                    agreementproperties.push(agreementpropertiesItem)
                }
            }else{
                if($(this).val() != '')
                {
                    var agreementpropertiesItem = {
                        rentalAgreementPropertiesId:rentalAgreementPropertiesIdArr[i],
                        rentalAgreementPropertyName:rentalAgreementPropertyNameArr[i],
                        rentalAgreementPropertyValue:$(this).val()
                    }
                    agreementproperties.push(agreementpropertiesItem)
                }
            }


        });

        agreementData = {
            agreement:data,
            agreementproperties:agreementproperties,
            agreementextras:agreementextras,
        }
        return agreementData;
    },
    extraList :function (dom,datalist){
        rentalagreementextrasData = [];
        console.log(datalist)
        for(var extra_i=0;extra_i < datalist.length ;extra_i++)
        {

            var item = datalist[extra_i];
            rentalagreementextrasData.push("<div class='row'>" +
                " <div class='col-md-3'> extraName </div> " +
                "<div class='col-md-6'> extraDescriptionn </div>" +
                " <div class='col-md-2'> extraPricen </div>" +
                " <div class='col-md-3'>" +
                "<input type='text' class='form-control' id='extraName' name='extraName' value='"+item.extraName +"' required> </div> " +
                "<div class='col-md-6'>" +
                " <input type='text' class='form-control' id='extraDescription' name='extraDescription' value='"+item.extraDescription +"' required>" +
                " </div> <div class='col-md-2'> <input type='text' class='form-control' id='extraPrice' name='extraPrice' value='"+item.extraPrice +"' required>" +
                "<input type='hidden' class='form-control' id='extraID' name='extraID' value='"+item.extraspropertyID+"' required>" +
                " </div> <div class='col-md-1 deletedProperty' style='cursor:wait' > 🆇 </div> </div>")
        }
        $(dom).append(rentalagreementextrasData.join(' '))
    },
    rentalAgreement_properties:function (dom,datalist){
        rentalagreementpropertiesData =[];
        var rentalagreementpropertiesList = []
        $(dom+" input[name='rentalAgreementPropertyName']").each(function (i) {
            for(var extra_i=0;extra_i < datalist.length ;extra_i++)
            {
                if(datalist[extra_i].rentalAgreementPropertyName == $(this).val() )
                {
                    $(dom+" input[name='rentalAgreementPropertyValue']")[i].value= datalist[extra_i].rentalAgreementPropertyValue;
                    $(dom+" input[name='selectionId']")[i].value= datalist[extra_i].selectionId;
                }
            }
        });

    }
})

$('.AddProperty').click(function (){
    $(".AddPropertList").append(
        "        <div class=\"row\">\n" +
        "            <div class=\"col-md-3\">\n" +
        "                extraName\n" +
        "            </div>\n" +
        "            <div class=\"col-md-6\">\n" +
        "                extraDescription\n" +
        "            </div>\n" +
        "            <div class=\"col-md-2\">\n" +
        "                extraPrice\n" +
        "            </div>\n" +
        "            <div class=\"col-md-3\"><input type=\"text\"  class=\"form-control\" id=\"salesVolume\" name=\"extraName\" value=\"\" autocomplete\n" +
        "                                required>\n" +
        "            </div>\n" +
        "            <div class=\"col-md-6\">\n" +
        "                <input type=\"text\"  class=\"form-control\" id=\"salesVolume\" name=\"extraDescription\" value=\"\" \n" +
        "                       required>\n" +
        "            </div>\n" +
        "            <div class=\"col-md-2\">\n" +
        "                <input type=\"text\"  class=\"form-control\" id=\"salesVolume\" name=\"extraPrice\" value=\"\" \n" +
        "                       required>\n" +
        "            </div>\n" +
        "            <div class=\"col-md-1 deletedProperty\"  style=\"cursor:wait\" >\n" +
        "                🆇\n" +
        "            </div>\n" +
        "        </div>"
    )
})


//
$('.UpdateProperty').click(function (){
    $(".UpdatePropertySTyle").append(
        "        <div class=\"row\">\n" +
        "            <div class=\"col-md-3\">\n" +
        "                property\n" +
        "            </div>\n" +
        "            <div class=\"col-md-8\">\n" +
        "                description\n" +
        "            </div>\n" +
        "            <div class=\"col-md-3\"><input type=\"text\"  class=\"form-control\" id=\"salesVolume\" name=\"propertyName\" value=\"\" autocomplete\n" +
        "                                required>\n" +
        "            </div>\n" +
        "            <div class=\"col-md-8\">\n" +
        "                <input type=\"text\"  class=\"form-control\" id=\"salesVolume\" name=\"propertyDescription\" value=\"\" \n" +
        "                       required>\n" +
        "            </div>\n" +
        "            <div class=\"col-md-1 deletedProperty\"  style=\"cursor:wait\" >\n" +
        "                🆇\n" +
        "            </div>\n" +
        "        </div>"
    )
})


// 维修记录表单 Maintenance record form
$.extend({
    damages_propertiesBiaodan:function(dom,e) {
        var damageName = $("#"+dom+" input[name=damageName]").val();
        var damageDescription = $("#"+dom+" input[name=damageDescription]").val();
        if(damageName != ''){
            damageName = damageName.trim();
        }else{
            commonUtil.message('damageName must exist','danger')
            Prevented(e)
        }
        if(damageDescription != ''){
            damageDescription = damageDescription.trim();
        }else{
            commonUtil.message('damageDescription must exist','danger')
            Prevented(e)
        }
        var damageproperties = {};
        damageproperties = {
            damageName:damageName,
            damageDescription:damageDescription ,
        }
        return damageproperties

    }
})
//

// 出租单元状态表单Rental unit status form
$.extend({
    statustypBiaodan:function(dom,e,type) {
        var type = $("#"+dom+" input[name=type]").val();
        var description = $("#"+dom+" input[name=description]").val();
        if(type != ''){
            type = type.trim();
        }else{
            commonUtil.message('type must exist','danger')
            Prevented(e)
        }
        if(description != ''){
            description = description.trim();
        }else{
            commonUtil.message('type must exist','danger')
            Prevented(e)
        }

        var statustyp = {};
        statustyp = {
            description:description,
            type:type ,
        }
        return statustyp

    }
})

// 出租单元列表字段管理

$.extend({
    unitnameBiaodan:function(dom,e,type) {
        var name = $("#"+dom+" input[name=name]").val();
        var manufacture = $("#"+dom+" input[name=manufacture]").val();
        if(name != ''){
            name = name.trim();
        }else{
            commonUtil.message('name must exist','danger')
            Prevented(e)
        }
        if(manufacture != ''){
            manufacture = manufacture.trim();
        }else{
            commonUtil.message('manufacture must exist','danger')
            Prevented(e)
        }
        var unitnames = {};
        unitnames = {
            manufacture:manufacture,
            name:name ,
        }
        return unitnames

    }
})

// 出租单元字段属性
$.extend({
    rentalpropertiesBiaodan:function(dom,e,type) {
        var propertieName = $("#"+dom+" input[name=propertieName]").val();
        var propertyDescription = $("#"+dom+" input[name=propertyDescription]").val();
        if(propertieName != ''){
            propertieName = propertieName.trim();
        }else{
            commonUtil.message('propertieName must exist','danger')
            Prevented(e)
        }
        if(propertyDescription != ''){
            propertyDescription = propertyDescription.trim();
        }else{
            commonUtil.message('propertyDescription must exist','danger')
            Prevented(e)
        }
        var  rentalproperties = {};
        rentalproperties = {
            propertyDescription:propertyDescription,
            propertieName:propertieName ,
        }
        return  rentalproperties

    }
})

// 出租单元获取相关信息
$.extend({
    RentalUnitRelevantInformatio:function(dom) {
        data='';
        var responses = formAjaxHy('/api/rental_properties_list', data, 'get');
        if(responses.payload.code==200){
            var properties =  responses.payload.data;
            if(properties.locationList){
                var locationListItem =[];
                for(var i = 0 , length = properties.locationList.length ;i<length ;i++)
                {
                    var itemdata = properties.locationList[i];
                    locationListItem.push('<option value="' + itemdata.id + '">' + itemdata.location + '</option>')
                }
                $('#'+dom+' #unitLocationPropertiesId').html(locationListItem.join(' '))
            }

            if(properties.locationList){
                var locationListItem =[];
                for(var i = 0 , length = properties.locationList.length ;i<length ;i++)
                {
                    var itemdata = properties.locationList[i];
                    locationListItem.push('<option value="' + itemdata.id + '">' + itemdata.location + '</option>')
                }
                $('#unitLocationPropertiesId').html(locationListItem.join(' '))
            }

            if(properties.names){
                var namesListItem =[];
                for(var i = 0 , length = properties.names.length ;i<length ;i++)
                {
                    var namesitemdata = properties.names[i];
                    namesListItem.push('<option value="' + namesitemdata.id + '">' + namesitemdata.name + '</option>')
                }
                $('#'+dom+' #unitNamesId').html(namesListItem.join(' '))
            }
            if(properties.statusType){
                var statusTypeitemdataListItem =[];
                for(var i = 0 , length = properties.statusType.length ;i<length ;i++)
                {
                    var statusTypeitemdata =  properties.statusType[i];
                    statusTypeitemdataListItem.push('<option value="' + statusTypeitemdata.id + '">' + statusTypeitemdata.type + '</option>')
                }
                $('#'+dom+' #statusTypId').html(statusTypeitemdataListItem.join(' '));
            }
            $('#'+dom+'  .Rental_properties').html('')
            if(properties.properties){
                for(var i = 0 , length = properties.properties.length ;i<length ;i++)
                {
                    var rentPropertyarr =  properties.properties[i];
                    $('#'+dom+'  .Rental_properties').append("<div class='row propertyNameClss'>"+
                        "<div class='col-md-3'>"+rentPropertyarr.propertieName+"</div>"+
                        " <div class='col-md-4'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalPropertiesId' value='"+rentPropertyarr.id+"'>"+
                        "<input type='text' maxlength='11' class='form-control' id='rentalPropertiesValue' name='rentalPropertiesValue' value=''>"+
                        "</div>"+
                        " </div>")
                }
            }
        }
    },
    // 获取维修属性单元
    GetDamage:function(dom){
        data = ''
        var damage_propertieCurl = formAjaxHy('/api/rental_damage_properties', data, 'get');
        delete damage_propertieCurl.payload.data.draw;
        var damage_properties =  Object.values(damage_propertieCurl.payload.data)
        if(damage_properties.length>0)
        {
                var damage_properArr =[];
                for(var i = 1 , length = damage_properties.length ;i<length ;i++)
                {
                    var itemdata = damage_properties[i];
                    damage_properArr.push('<option value="' + itemdata.id + '">' + itemdata.damageName + '</option>')
                }
                $('#'+dom+' #damageProperties').html(damage_properArr.join(' '))
        }
    },

    rental_damage_Biaodan:function (dom){
        var damagePropertiesId = $("#"+dom+" #damageProperties").val();
        var unitLocationPropertiesId = $("#"+dom+" #unitLocationPropertiesId").val();
        var repairDateTime = $("#"+dom+" input[name=repairDateTime]").val();
        var damageTime = $("#"+dom+" input[name=damageTime]").val();
        var damageStatus = $("#"+dom+" #damageStatus").val();
        var damageImgPath = $("#"+dom+" input[name=damageImgPath]").val();
        var rentalUnitsId = $("#"+dom+" input[name=rentalUnitsId]").val();
        var rentalDamage = {
            "imageData": damageImgPath,
            "damageStatus": damageStatus,
            "damageTime": damageTime,
            "rentalUnitsId": rentalUnitsId,
            "repairDateTime": repairDateTime,
            "unitLocationPropertiesId": unitLocationPropertiesId,
            "damagePropertiesId": damagePropertiesId
        }
        return rentalDamage;
    }
})


// 获取出租单元表单提交字段信息


$.extend({
    rentalUnitInfoBiaodan:function(dom,e,type) {
        var unitNamesId = $("#"+dom+" #unitNamesId").val();

        var statusTypId = $("#"+dom+" #statusTypId").val();
        var unitLocationPropertiesId = $("#"+dom+" #unitLocationPropertiesId").val();
        var typName = $("#"+dom+" input[name=typName]").val();
        var typDescription = $("#"+dom+" #typDescription").val();
        var priceGroup = $("#"+dom+" input[name=priceGroup]").val();
        var Price = $("#"+dom+" input[name=price]").val();
        if( $("#"+dom+" input[name=unittyp_id]"))
        var unitTypId = $("#"+dom+" input[name=unittyp_id]").val();

        if(priceGroup != ''){
            priceGroup = priceGroup.trim();
        }else{
            commonUtil.message('priceGroup must exist','danger')
            Prevented(e)
        }
        if(typName != ''){
            typName = typName.trim();
        }else{
            commonUtil.message('typName must exist','danger')
            Prevented(e)
        }

        if(Price != ''){
            Price = Price.trim();
        }else{
            commonUtil.message('Price must exist','danger')
            Prevented(e)
        }

        var rentalPropertiesIdArr = [];
        var rentalPropertiesValueArr = []

        $("#"+dom+" input[name='rentalPropertiesId']").each(function (i) {
            rentalPropertiesIdArr.push($(this).val());
        });
        console.log('*****selectionId*******')
        if($("#"+dom+" input[name='selectionId']")){
            var rentalPropertySelectionIdArr = [];
            $("#"+dom+" input[name='selectionId']").each(function (i) {
                rentalPropertySelectionIdArr.push($(this).val());
            });
        }
        console.log(rentalPropertySelectionIdArr)
        if($("#"+dom+" input[name='rentalPropertiesId']")){
            var rentalPropertiesIdArr = [];
            $("#"+dom+" input[name='rentalPropertiesId']").each(function (i) {
                rentalPropertiesIdArr.push($(this).val());
            });
        }
        console.log(rentalPropertiesIdArr)
        console.log('******selectionId******')

        var rentalproperties = [];
        if($("#"+dom+" input[name='rentalPropertiesValue']")){
            console.log('&&&')
            $("#"+dom+" input[name='rentalPropertiesValue']").each(function (i) {
                if(rentalPropertySelectionIdArr.length > 0){
                    var data ={
                        rentalPropertiesId : rentalPropertiesIdArr[i],
                        selectionId : rentalPropertySelectionIdArr[i],
                        rentalPropertiesValue : $(this).val()
                    }
                    rentalproperties.push(data);
                }else{
                    if($(this).val() !=''){
                        var data ={
                            rentalPropertiesId : rentalPropertiesIdArr[i],
                            rentalPropertiesValue : $(this).val()
                        }
                        rentalproperties.push(data);
                    }
                }

            });
        }
        var rental = {
            "unitNamesId": unitNamesId,
            "unitLocationPropertiesId": unitLocationPropertiesId,
            "statusTypId": statusTypId,
            "typName": typName,
            "typDescription": typDescription,
            "priceGroup": priceGroup,
            "price": Price
        }
        if (unitTypId != '')
        {
            rental.unitTypId =unitTypId;
        }
        var rentalUnitData = {
            rental:rental,
            rentalproperties:rentalproperties,
        }

        console.log(rentalUnitData)
        return rentalUnitData;
    }
})

$.extend({
    unitlocationBiaodan :function(dom,e,type) {
        var description = $("#"+dom+" input[name=description]").val();
        var location = $("#"+dom+" input[name=location]").val();
        var number = $("#"+dom+" input[name=number]").val();
        var remarks = $("#"+dom+" .remarks").val();

        if(location != ''){
            location = location.trim();
        }else{
            commonUtil.message('location must exist','danger')
            Prevented(e)
        }

        if(description != ''){
            description = description.trim();
        }else{
            commonUtil.message('description must exist','danger')
            Prevented(e)
        }

        var unitlocation = {};
        unitlocation = {
            description:description,
            number:number,
            location:location,
            remarks:remarks,
        }
        return unitlocation

    }
})

$.extend({
    getImageBase:function (img_file_dom,show_site,data_site){
        run(img_file_dom,show_site,data_site);
    }
})

function run(input_file,show_site,data_site) {
    var reader = '';
    // console.log(input_file)
    var flag = true;
    if (typeof (FileReader) === 'undefined') {
        return false;
    } else {
        try {
            var file = input_file.files;
            for(var i= 0;i<file.length;i++) {
                if (!/image\/\w+/.test(file[i].type)) {
                    return false;
                }
                reader = new FileReader();
                reader.onload = function () {
                    if(flag){
                        $('#'+data_site).val(this.result);
                        $('.'+show_site).attr( "src",this.result);
                    }
                }
               reader.readAsDataURL(file[i]);
            }
        } catch (e) {
            alert('图片转Base64出错啦！' + e.toString())
        }
    }
}


var commonUtil = {
    /**
     * popup message box
     * @param msg message content
     * @param type message box type (refer to bootstrap's alert)
     */
    alert: function(msg, type){
        if(typeof(type) =="undefined") { // 未传入type则默认为success类型的消息框
            type = "success";
        }
        // 创建bootstrap的alert元素
        var divElement = $("<div>DUISUIDUSISFDSFJLKSJ</div>").addClass('alert').addClass('alert-'+type).addClass('alert-dismissible').addClass('col-md-offset-4');
        divElement.css({ // 消息框的定位样式
            "position": "fixed",
            "top": "20%",
            "left": "50%",
            "z-index":'99999'
        });
        divElement.text(msg); // 设置消息框的内容
        // 消息框添加可以关闭按钮

        // 消息框放入到页面中
        $('body').append(divElement);
        return divElement;
    },

    /**
     * 短暂显示后上浮消失的消息框
     * @param msg 消息内容
     * @param type 消息框类型
     */
    message: function(msg, type) {
        var divElement = commonUtil.alert(msg, type); // 生成Alert消息框
        var isIn = false; // 鼠标是否在消息框中

        divElement.on({ // 在setTimeout执行之前先判定鼠标是否在消息框中
            mouseover : function(){isIn = true;},
            mouseout  : function(){isIn = false;}
        });

        // 短暂延时后上浮消失
        setTimeout(function() {
            var IntervalMS = 20; // 每次上浮的间隔毫秒
            var floatSpace = 60; // 上浮的空间(px)
            var nowTop = divElement.offset().top; // 获取元素当前的top值
            var stopTop = nowTop - floatSpace;    // 上浮停止时的top值
            divElement.fadeOut(IntervalMS * floatSpace); // 设置元素淡出

            var upFloat = setInterval(function(){ // 开始上浮
                if (nowTop >= stopTop) { // 判断当前消息框top是否还在可上升的范围内
                    divElement.css({"top": nowTop--}); // 消息框的top上升1px
                } else {
                    clearInterval(upFloat); // 关闭上浮
                    divElement.remove();    // 移除元素
                }
            }, IntervalMS);

            if (isIn) { // 如果鼠标在setTimeout之前已经放在的消息框中，则停止上浮
                clearInterval(upFloat);
                divElement.stop();
            }

            divElement.hover(function() { // 鼠标悬浮时停止上浮和淡出效果，过后恢复
                clearInterval(upFloat);
                divElement.stop();
            },function() {
                divElement.fadeOut(IntervalMS * (nowTop - stopTop)); // 这里设置元素淡出的时间应该为：间隔毫秒*剩余可以上浮空间
                upFloat = setInterval(function(){ // 继续上浮
                    if (nowTop >= stopTop) {
                        divElement.css({"top": nowTop--});
                    } else {
                        clearInterval(upFloat); // 关闭上浮
                        divElement.remove();    // 移除元素
                    }
                }, IntervalMS);
            });
        }, 2000);
    }
}



$('#drawing').click(function (){
    $('#damages_drawing_model').modal('show')

    let canEle = document.getElementsByTagName("canvas")[0];
    let pen = canEle.getContext("2d");
    let image = new Image()
    image.src = '/public/Img/blueprints.jpg'
    image.onload = function(){
        var canvasTemp = document.getElementsByTagName("canvas")[0];
        var contextTemp = canvasTemp.getContext('2d');
        canvasTemp.width = 1400; // 目标宽度
        canvasTemp.height = 913; // 目标高度
        contextTemp.drawImage(this,0,0,1400,913)
        var ptn = pen.createPattern(canvasTemp,'no-repeat')
        pen.fillStyle = ptn;
        pen.fill()

    }

    let colEle = document.getElementsByClassName("yanSe")[0];
    let qcEle = document.getElementsByClassName("qingChu")[0];
    let chuxiEle = document.getElementsByClassName("chuxi")[0];

    canEle.addEventListener("mousedown", function (e) {
        let event1 = e || window.event1;
        pen.moveTo(event1.clientX - canEle.offsetLeft, event1.clientY - canEle.offsetTop);
        pen.beginPath();
        canEle.addEventListener("mousemove", hua);
        function hua(e) {
            let event = e || window.event;
            pen.strokeStyle = colEle.value;
            pen.lineWidth = chuxiEle.value;
            pen.lineTo(event.offsetX, event.offsetY);
            console.log(pen.lineWidth);
            pen.stroke();
        }

        canEle.addEventListener("mouseup", function () {
            canEle.removeEventListener("mousemove", hua);
        })

    })

    qcEle.addEventListener("click", qingchu);
    function qingchu() {
        pen.clearRect(0, 0, canEle.offsetWidth, canEle.offsetHeight);

        $('#damages_drawing_model').modal('hide')
    }





})
function downLoadImage() {
    var canvas = document.getElementById("canvas");
    var dataURL = canvas.toDataURL();
    $('.damage_imag').attr( "src",dataURL);
    $('#damageImgPath').val(dataURL);
    $('#damages_drawing_model').modal('hide')

}


/**
 *权限相关表单信息 Permission_data_biaodan
 * 权限循环顶级表单 Permission_top_list
 */
$.extend({
    Permission_data_biaodan  :function(dom,e='') {
        var menu_name = $("#"+dom+" input[name=menu_name]").val();
        var father_id = $("#"+dom+" #father_id").val();
        var menu_icon = $("#"+dom+" input[name=menu_Id]").val();
        var menu_url = $("#"+dom+" input[name=menu_url]").val();
        var menu_order = $("#"+dom+" input[name=menu_order]").val();
        var method = $("#"+dom+" #method").val();
        var type = $("#"+dom+" #type").val();

        var Permission_data = {};
        Permission_data = {
            menu_name: $("#"+dom+" input[name=menu_name]").val(),
            father_id: $("#"+dom+" #father_id").val(),
            menu_icon: $("#"+dom+" input[name=menu_icon]").val(),
            id: $("#"+dom+" input[name=id]").val(),
            menu_url: $("#"+dom+" input[name=menu_url]").val(),
            menu_order: $("#"+dom+" input[name=menu_order]").val(),
            order: $("#"+dom+" input[name=order]").val(),
            method:$("#"+dom+" #method").val(),
            type: $("#"+dom+" #type").val(),
        }

        if(Permission_data.father_id == 0){
            if(Permission_data.menu_icon  == ''){
                commonUtil.message('menu_Id is required for top-level menu');
                if (e.isDefaultPrevented()) {
                    return;
                }
                e.preventDefault()
                Permission_data.method='';
                return false;
            }

            Permission_data.menu_url='';
            Permission_data.method='';
        }else{
            console.log(Permission_data)
            if(Permission_data.menu_url == '')
            {
                commonUtil.message('The permission address cannot be empty');

                return  false;
            }
            if (Permission_data.type == 2 ){//操作
                if(Permission_data.method == '' || Permission_data.method == null)
                {
                    commonUtil.message('You need to select a submission mode when performing this operation');
                    return  false;
                }
            }else{
                Permission_data.method='';
            }

        }
        return Permission_data

    },
    Permission_top_list(permissionList,dom){
        var permissionUptons = [];
        permissionUptons.push("<option value='0'>---top-level ---</option>");
        for (var j = 0, perlen = permissionGlobal.length; j < perlen; j++) {
            var peritemdata = permissionGlobal[j];
            permissionUptons.push(" <option value='"+peritemdata.info.id+"'>"+peritemdata.info.menu_name+"</option>")
        }
        $(dom).append(permissionUptons.join(' '))
    }
})

$.extend({
    publicDelete:function (){
    $.ajax({
        method: "GET",      // 设置请求方式为GET
        url:"\\resources\\assets\\language\\"+lagtype+".json", // 加载本地json文件
        dataType: "json",   // 设置数据类型为json
        success: function(e){
            $(".lang").each(function(index, element) {
                $(this).text(e[$(this).attr("key")]);
            });
        }
    })
}
})

function publicDelete(){

    if($('#del_id').text() ==''){
        commonUtil.message('Error ID')
        return false;
    }
    if($('#del_url').text() ==''){
        commonUtil.message('The delete topic could not be found！')
        return false;
    }
    var datadelete ={id:$('#del_id').text()};
    var datadeleteurl = $('#del_url').text();
    var dataderesponses = formAjaxHy(datadeleteurl,datadelete, 'DELETE');
    if (dataderesponses.payload.code == 200) {
        commonUtil.message('Delete Success！')
        table.ajax.reload();
    } else {
        commonUtil.message(dataderesponses.payload.data)
    }

    $('#delcfmOverhaul').modal('hide')
    table.ajax.reload();
}