<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class RentalStatusTypModel extends BaseModel
{
    protected $table = 'statustyp';

    public function getIndex($start, $length, $search, $order)
    {
        $whereSql = '';
        if (!$search == '') {
            $whereSql = vsprintf("type REGEXP '%s'", array($search, $search));
        }
        return $this->baseGetIndex($this->table, $start, $length, $search, $order, $whereSql);
    }

    public function getOne($id)
    {
        $returnArray = $this->getSingle($this->table, $id);
        return $returnArray;
    }

    public function postItem($data)
    {
        $returnBool = true;
        try {
            if (isset($data['id'])) {
                $isDuplicateData = DB::link()->table($this->table)->where("type = '" . $data['type'] . "' AND id <> " . $data['id'])->field('id')->get();
                if (!isset($isDuplicateData[0]['id'])) {
                    DB::link()->table($this->table)->where("id = " . $data['id'])->update($data);
                } else {
                    throw new PDOException('The value of type already exists');
                }
            } else {
                // check for duplicates
                $isDuplicateData = DB::link()->table($this->table)->where("type = " . $data['type'])->field('id')->get();
                if (!isset($isDuplicateData[0]['id'])) {
                    DB::link()->table($this->table)->insert($data);
                } else {
                    throw new PDOException('The value of type already exists');
                }
            }
        } catch (PDOException $e) {
            FileLogger::error('Error Inserting data: ' . $e->getMessage());
            $returnBool = false;
        }

        $returnCode = 200;
        if (!$returnBool) {
            $returnCode = 502;
        }

        return $returnCode;
    }

    public function deleteItem($id)
    {
        $returnBool = true;
        try {
            // Check whether to modify the first three states
            $baseStatusTypData = config('rentalStatusTyp');
            $baseStatusTypDataSql = "type IN ('" . implode("','", $baseStatusTypData) . "')";
            $baseStatusTypId = DB::link()->table($this->table)->where($baseStatusTypDataSql)->field('id')->get();
            $baseStatusTypId = array_column($baseStatusTypId, 'id');
            if (!in_array($id, $baseStatusTypId)) {
                DB::link()->table($this->table)->where("id = $id")->delete();
            } else {
                throw new PDOException('The initial rentalUnit status cannot be deleted');
            }
        } catch (PDOException $e) {
            FileLogger::error('Error deleting data: ' . $e->getMessage());
            $returnBool = false;
        }
        $returnCode = 200;
        if (!$returnBool) {
            $returnCode = 502;
        }
        return $returnCode;
    }
}
