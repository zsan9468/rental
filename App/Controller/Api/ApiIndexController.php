<?php

namespace App\Controller\Api;

use App\Controller\ApiOutputController;

class ApiIndexController
{
    /**
     * @Description It can be used for testing, the Url: /api
     * @DateTime 2023-03-08
     */
    public function index()
    {

        /**
         * Test image upload interface
         */
        // $jsonData = json_decode(file_get_contents('php://input'), true);
        // if (is_null($jsonData)) {
        //     $jsonData = [];
        // }
        // dd($jsonData['imageData']);

        /**
         * Test the config function
         */
        // $configDataTest = config('frontEndIndex');
        // dd($configDataTest);

        /**
         * Test the PDF generation module
         */
        // $pdfContent = "Hello, world!";
        // $pdfGenerator = new \App\Lib\PDFGenerator($pdfContent);
        // $pdfString = $pdfGenerator->generatePDF();
        // header('Content-Type: application/pdf');
        // header('Content-Disposition: attachment; filename="example.pdf"');
        // echo $pdfString;
        // ApiOutputController::ApiOutput($pdfString, 200,  $header);

        /**
         * Test the data validator
         */
        // $data = [
        //     'name' => 'Ethan Cheng',
        //     'email' => 'sheng@asc-vision.de',
        //     'password' => 'admin123',
        //     'password_confirmation' => 'admin1234',
        // ];

        // $rules = [
        //     'name' => ['required'],
        //     'email' => ['required', 'email'],
        //     'password' => ['required', 'min:8'],
        //     'password_confirmation' => ['required', 'same:password'],
        // ];

        // $messages = [
        //     'name.required' => 'The name field is required.',
        //     'email.required' => 'The email field is required.',
        //     'email.email' => 'The email field must be a valid email address.',
        //     'password.required' => 'The password field is required.',
        //     'password.min' => 'The password must be at least 8 characters.',
        //     'password_confirmation.required' => 'The password confirmation field is required.',
        //     'password_confirmation.same' => 'The password confirmation field must same to password.',
        // ];

        // $validator = new \App\Lib\Validator($data, $rules, $messages);

        // $result = $validator->validate();

        // if ($result === true) {
        //     echo 'success!';
        // } else {
        //     ApiOutputController::ApiOutput($result, 412);
        // }

        if (false) {
            echo 'true';
        } else {
            echo 'false';
        }
    }
}
