<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Lib\DB;
use App\Models\AgreementModel;
use App\Models\BaseModel;

class AgreementResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new AgreementModel;
        // Reduce the degree of coupling and return the status code uniformly
        $code = 200;

        if (empty($params)) {
            $result['data'] = '';
            $result['code'] = 501;
        } else {
            if (isset($params['id'])) {
                // GetOne
                $result['data'] = $newModel->getOne($params['id']);
            } else {
                // GetAll
                $start = $params['start'];
                $length = $params['length'];
                $search = $params['search']['value'];


                $field = $params['columns'][$params['order'][0]['column']]['data'];
                switch ($field) {
                    case 'id':
                        $field = "a.`id`";
                        break;
                    default:
                        $field = "a.`id`";
                        break;
                }
                $collation  = $params['order'][0]['dir'];
                $order = "$field $collation";
                $result['data'] = $newModel->getIndex($start, $length, $search, $order);
                $result['data']['draw'] = (int)$params['draw'];
            }
            $result['code'] = $code;
        }

        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data)
    {
        $rules = [
            'rentalUnitsId' => ['required'],
            'costumerId' => ['required'],
            'insurenceId' => ['required'],
            'cost' => ['required', 'numeric'],
            'rentalBeginDateTime' => ['required'],
            'rentalEndDateTime' => ['required'],
        ];
        $messages = [
            'rentalUnitsId.required' => 'The rentalUnitsId field is required.',
            'costumerId.required' => 'The costumerId field is required.',
            'insurenceId.required' => 'The insurenceId field is required.',
            'cost.required' => 'The cost field is required.',
            'cost.numeric' => 'The cost field must be a number.',
            'rentalBeginDateTime.required' => 'The rentalBeginDateTime field is required.',
            'rentalEndDateTime.required' => 'The rentalEndDateTime field is required.',
        ];
        $validator = new \App\Lib\Validator($data['agreement'], $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        /**
         * Validate the requested data,
         * 1. Verify that the Agreement date is legal
         * 2. Check if the rentalUnit can be leased
         */
        $rentalUnitsId = $data['agreement']['rentalUnitsId'];
        $isFree = DB::link()->table('rentalunits')->where("id = $rentalUnitsId")->field('statusTypId')->limit(1)->get();
        if (isset($data['agreement']['id'])) {
            $id = $data['agreement']['id'];
            $isChangeRentalUnitID = DB::link()->table('rentalagreement')->where("id = $id")->get();
            if ($isChangeRentalUnitID[0]['rentalUnitsId'] !== $data['agreement']['rentalUnitsId']) {
                ApiOutputController::ApiOutput([], 11401);
            }
        }

        if ($data['agreement']['rentalBeginDateTime'] > $data['agreement']['rentalEndDateTime']) {
            $resultCode = 11403;
        } elseif (!isset($data['agreement']['id']) && (int)$isFree[0]['statusTypId'] !== 1) {
            $resultCode = 11400;
        } else {
            $newModel = new AgreementModel;
            $resultCode = $this->baseUpdate($newModel, $data);
        }

        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($id)
    {
        $newModel = new AgreementModel;
        $resultCode = $this->baseDestroy($newModel, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function InsurenceList()
    {
        $newModel = new AgreementModel;
        $result = $newModel->getInsurencesList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function PropertiesList()
    {
        $newModel = new AgreementModel;
        $result = $newModel->getPropertiesList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function ExtraList()
    {
        $newModel = new AgreementModel;
        $result = $newModel->getExtrasList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function updateRentalunitStatusTyp($data)
    {
        if (empty($data['id'])) {
            ApiOutputController::ApiOutput([], 503, '');
        }

        $newModel = new AgreementModel;
        $resultCode = $newModel->updateRentalunitStatusTyp($data['id']);
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
