<?php

namespace App\Models;

use App\Controller\ApiOutputController;
use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class RentalModel extends BaseModel
{
    protected $table = 'rentalunits';
    protected $tablePropertySelection = 'rentalpropertiesselection';
    protected $tableTyp = 'unittyp';
    protected $tablePrice = 'unitprice';

    protected $tableProperties = 'rentalproperties';
    protected $tableLocation = 'unitlocation';
    protected $tableNames = 'unitnames';
    protected $tableStatusType = 'statustyp';

    public function getIndex($start, $length, $search, $order)
    {
        $limitSql = 'LIMIT ' . $start . ',' . $length;
        if ($search == '') {
            $whereSql = '';
        } else {
            $whereSql = vsprintf("WHERE un.name REGEXP '%s' or ut.typName REGEXP '%s'", array($search, $search));
        }

        // Query All Rental Data
        $sql = "SELECT r.id, ut.id AS unittyp_id,st.id AS statustyp_id,ult.id AS unitlocation_id, un.id AS unitnames_id ,ut.typName,up.price,up.priceGroup,ult.location,un.name ,un.manufacture,st.type,ut.description 
        FROM rentalunits AS r 
        LEFT JOIN unittyp AS ut  ON  ut.id = r.unitTypId 
        LEFT JOIN unitprice AS up ON  up.unitTypId = ut.id
        LEFT JOIN statustyp AS st  ON  st.id = r.statusTypId
        LEFT JOIN unitlocation AS ult ON  ult.id = r.unitLocationPropertiesId
        LEFT JOIN unitnames AS un ON  un.id = r.unitNamesId
        {$whereSql}
        ORDER BY {$order}              
        {$limitSql}";
        $returnArray['data'] = DB::link()->query($sql);

        // Query Rental Count
        $toTal = DB::link()->table($this->table)->count();
        $returnArray['recordsFiltered'] = count($returnArray['data']);
        $returnArray['recordsTotal'] = (int)$toTal;

        return $returnArray;
    }

    public function getOne($id)
    {
        // Get one main data
        $whereSql = "WHERE r.id = $id";
        $sql = "SELECT r.id, 
        ut.id AS unittyp_id, ut.typName, ut.description,
        up.price, up.priceGroup,
        ult.id AS unitlocation_id, ult.location, ult.description AS location_description,
        un.id AS unitnames_id, un.name, un.manufacture,
        st.id AS statustyp_id, st.type, st.description AS status_description
        FROM rentalunits AS r 
        LEFT JOIN unittyp AS ut  ON  ut.id = r.unitTypId 
        LEFT JOIN unitprice AS up ON  up.unitTypId = ut.id
        LEFT JOIN statustyp AS st  ON  st.id = r.statusTypId
        LEFT JOIN unitlocation AS ult ON  ult.id = r.unitLocationPropertiesId
        LEFT JOIN unitnames AS un ON  un.id = r.unitNamesId
        {$whereSql}";
        $returnArray['data'] = DB::link()->query($sql);

        // Get one proerties data
        $result['properties'] = DB::link()->table($this->tableProperties)->get();
        $result['selection'] = $this->getSingle($this->tablePropertySelection, $id, 'rentalUnitsId');

        // Processing properties data
        $returnArray['data']['properties'] = [];
        foreach ($result['properties'] as $v) {
            $sortData['rentalPropertiesId'] = $v['id'];
            $sortData['rentalPropertieName'] = $v['propertieName'];
            $sortData['propertyDescription'] = $v['propertyDescription'];
            $sortData['selectionId'] = '';
            $sortData['rentalPropertiesValue'] = '';
            foreach ($result['selection'] as $vv) {
                if ($vv['rentalPropertiesId'] == $v['id']) {
                    $sortData['selectionId'] = $vv['id'];
                    $sortData['rentalPropertiesValue'] = $vv['rentalPropertiesValue'];
                }
            }
            $returnArray['data']['properties'][] = $sortData;
        }
        return $returnArray;
    }

    public function postItem($data)
    {
        try {
            // Begin the transaction
            DB::link()->db->beginTransaction();

            if (isset($data['rental']['id'])) {
                // update
                $id = $data['rental']['id'];
                unset($data['rental']['id']);
                $unitTypId = $data['rental']['unitTypId'];
                // update the main data
                DB::link()->table($this->table)->where("id = $id")->update($data['rental']);
                DB::link()->table($this->tableTyp)->where("id = $unitTypId")->update($data['typ']);
                DB::link()->table($this->tablePrice)->where("unitTypId = $unitTypId")->update($data['price']);

                // Insert the additional information data
                if (!empty($data['rentalproperties'])) {
                    foreach ($data['rentalproperties'] as $v) {
                        if (!empty($v['rentalPropertiesValue'])) {
                            // If there is a value
                            // it is determined whether to add or update according to the selectionId
                            if (isset($v['selectionId']) && !empty($v['selectionId'])) {
                                // update
                                $propertiesId = $v['selectionId'];
                                unset($v['selectionId']);
                                DB::link()->table($this->tablePropertySelection)->where("id = $propertiesId")->update($v);
                            } else {
                                // add
                                $v['rentalUnitsId'] = $id;
                                unset($v['selectionId']);
                                DB::link()->table($this->tablePropertySelection)->insert($v);
                            }
                        } else {
                            // Value is empty, delete or do nothing
                            if (isset($v['selectionId']) && !empty($v['selectionId'])) {
                                // delete
                                $propertiesId = $v['selectionId'];
                                DB::link()->table($this->tablePropertySelection)->where("id = $propertiesId")->delete();
                            }
                        }
                    }
                }
            } else {
                // Insert
                // Insert the main data
                DB::link()->table($this->tableTyp)->insert($data['typ']);
                $unitTypId = DB::link()->db->lastInsertId();
                $data['price']['unitTypId'] = $unitTypId;
                $data['rental']['unitTypId'] = $unitTypId;
                DB::link()->table($this->tablePrice)->insert($data['price']);
                DB::link()->table($this->table)->insert($data['rental']);
                $rentalUnitsId = DB::link()->db->lastInsertId();

                // Insert the additional information data
                if (!empty($data['rentalproperties'])) {
                    foreach ($data['rentalproperties'] as $v) {
                        if (!empty($v['rentalPropertiesValue'])) {
                            $v['rentalUnitsId'] = $rentalUnitsId;
                            DB::link()->table($this->tablePropertySelection)->insert($v);
                        }
                    }
                }
            }

            // Commit the transaction
            DB::link()->db->commit();

            $ReturnBool = true;
        } catch (PDOException $e) {
            // Roll back the transaction on error
            DB::link()->db->rollBack();

            // Log the error
            FileLogger::error('Error inserting data: ' . $e->getMessage());

            // Return failure
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function deleteItem($id)
    {
        try {
            DB::link()->db->beginTransaction();
            $unitTypIdData = DB::link()->table($this->table)->where("id = $id")->field('unitTypId')->get();
            $unitTypId = $unitTypIdData[0]['unitTypId'];
            DB::link()->table($this->tablePrice)->where("unitTypId = $unitTypId")->delete();
            DB::link()->table($this->tablePropertySelection)->where("rentalUnitsId = $id")->delete();
            DB::link()->table($this->table)->where("id = $id")->delete();
            DB::link()->table($this->tableTyp)->where("id = $unitTypId")->delete();
            DB::link()->db->commit();
            $ReturnBool = true;
        } catch (PDOException $e) {
            DB::link()->db->rollBack();
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            if (strstr($e->getMessage(), "a foreign key constraint fails")) {
                ApiOutputController::ApiOutput('', 801, '');
            }
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function getPropertiesList()
    {
        try {
            $returnArray['data']['locationList'] = DB::link()->table($this->tableLocation)->get();
            $returnArray['data']['names'] = DB::link()->table($this->tableNames)->get();
            $returnArray['data']['statusType'] = DB::link()->table($this->tableStatusType)->get();
            $returnArray['data']['properties'] = DB::link()->table($this->tableProperties)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }

    public function getPropertiesSelectionList()
    {
        try {
            $returnArray['data'] = DB::link()->table($this->tableProperties)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }
}
