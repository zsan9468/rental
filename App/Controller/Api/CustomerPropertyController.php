<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\CustomerPropertyResource;

class CustomerPropertyController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new CustomerPropertyResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new CustomerPropertyResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new CustomerPropertyResource)->destroy($data);
    }
}
