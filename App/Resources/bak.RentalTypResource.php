<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\RentalTypModel;

class RentalTypResource extends BaseResource
{
    public function index($params)
    {
        if (isset($params['id'])) {
            $result['data'] = '';
            $result['code'] = 400;
        } else {
            $newModel = new RentalTypModel;
            $result = $this->baseIndex($newModel, $params);
        }
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'typName' => ['required'],
        ];
        $messages = [
            'typName.required' => 'The type field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new RentalTypModel;
        $resultCode = $this->baseUpdate($newModel, $data);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new RentalTypModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
