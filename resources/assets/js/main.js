/**
* Name: Rental-App
* Author: Haiyang Wang
* 
*/
(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    if (all) {
      select(el, all).forEach(e => e.addEventListener(type, listener))
    } else {
      select(el, all).addEventListener(type, listener)
    }
  }

  /**
   * Easy on scroll event listener
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Sidebar toggle
   */
  if (select('.toggle-sidebar-btn')) {
    on('click', '.toggle-sidebar-btn', function (e) {
      select('body').classList.toggle('toggle-sidebar');
      // select('#sidebar-nav').classList.toggle('secontpassWarning')
      // select('#sidebar-navs').classList.toggle('showpassWarning')
    })
  }
  /**
   * Search bar toggle
   */
  if (select('.search-bar-toggle')) {
    on('click', '.search-bar-toggle', function (e) {
      select('.search-bar').classList.toggle('search-bar-show')
    })
  }

  let selectHeader = select('#header')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
      } else {
        selectHeader.classList.remove('header-scrolled')
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Initiate tooltips
   */
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

  /**
   * Initiate quill editors
   */
  if (select('.quill-editor-default')) {
    new Quill('.quill-editor-default', {
      theme: 'snow'
    });
  }

  if (select('.quill-editor-bubble')) {
    new Quill('.quill-editor-bubble', {
      theme: 'bubble'
    });
  }

  if (select('.quill-editor-full')) {
    new Quill(".quill-editor-full", {
      modules: {
        toolbar: [
          [{
            font: []
          }, {
            size: []
          }],
          ["bold", "italic", "underline", "strike"],
          [{
            color: []
          },
            {
              background: []
            }
          ],
          [{
            script: "super"
          },
            {
              script: "sub"
            }
          ],
          [{
            list: "ordered"
          },
            {
              list: "bullet"
            },
            {
              indent: "-1"
            },
            {
              indent: "+1"
            }
          ],
          ["direction", {
            align: []
          }],
          ["link", "image", "video"],
          ["clean"]
        ]
      },
      theme: "snow"
    });
  }

  /**
   * Initiate TinyMCE Editor
   */

  var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

  tinymce.init({
    selector: 'textarea.tinymce-editor',
    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
    imagetools_cors_hosts: ['picsum.photos'],
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    toolbar_sticky: true,
    autosave_ask_before_unload: true,
    autosave_interval: '30s',
    autosave_prefix: '{path}{query}-{id}-',
    autosave_restore_when_empty: false,
    autosave_retention: '2m',
    image_advtab: true,
    link_list: [{
      title: 'My page 1',
      value: 'https://www.tiny.cloud'
    },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_list: [{
      title: 'My page 1',
      value: 'https://www.tiny.cloud'
    },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_class_list: [{
      title: 'None',
      value: ''
    },
      {
        title: 'Some class',
        value: 'class-name'
      }
    ],
    importcss_append: true,
    file_picker_callback: function (callback, value, meta) {
      /* Provide file and text for the link dialog */
      if (meta.filetype === 'file') {
        callback('https://www.google.com/logos/google.jpg', {
          text: 'My text'
        });
      }

      /* Provide image and alt text for the image dialog */
      if (meta.filetype === 'image') {
        callback('https://www.google.com/logos/google.jpg', {
          alt: 'My alt text'
        });
      }

      /* Provide alternative source and posted for the media dialog */
      if (meta.filetype === 'media') {
        callback('movie.mp4', {
          source2: 'alt.ogg',
          poster: 'https://www.google.com/logos/google.jpg'
        });
      }
    },
    templates: [{
      title: 'New Table',
      description: 'creates a new table',
      content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
    },
      {
        title: 'Starting my story',
        description: 'A cure for writers block',
        content: 'Once upon a time...'
      },
      {
        title: 'New list with dates',
        description: 'New List with dates',
        content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
      }
    ],
    template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
    template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
    height: 600,
    image_caption: true,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_noneditable_class: 'mceNonEditable',
    toolbar_mode: 'sliding',
    contextmenu: 'link image imagetools table',
    skin: useDarkMode ? 'oxide-dark' : 'oxide',
    content_css: useDarkMode ? 'dark' : 'default',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
  });

  /**
   * Initiate bootstrap validation check
   */
  var needsValidation = document.querySelectorAll('.needs-validation')

  Array.prototype.slice.call(needsValidation)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })


  /**
   * Autoresize echart charts
   */
  const mainContainer = select('#main');
  if (mainContainer) {
    setTimeout(() => {
      new ResizeObserver(function () {
        select('.echart', true).forEach(getEchart => {
          echarts.getInstanceByDom(getEchart).resize();
        })
      }).observe(mainContainer);
    }, 200);
  }

  /**
   * 获取本地缓存用户信息
   */

  function getUserInfo()
  {
     var user =  JSON.parse(localStorage.getItem('userinfo'));
     return user;
  }

  /**
   * 配置页面户信息
   */
  var pageuser = getUserInfo();
  $('.namesurname').text(pageuser.name+'&'+pageuser.surname);
  if(pageuser.user_type == 1) $('.userType').text('IT');
  if(pageuser.user_type == 2) $('.userType').text('Admin');
  if(pageuser.user_type == 3) $('.userType').text('Employee');
  if(pageuser.profile_photo) $('.profile_photo').attr('src',host+'/'+pageuser.profile_photo);

  /**
   * 菜单栏动态添加样式
   * The menu bar dynamically adds styles
   */

  console.log(window.location.href)
  console.log( host+'/web/index/index')
  if(window.location.href != host+'/web/index/index'){
      $("#home").removeClass('nav-link-home-color')
    console.log(1)
  }else{
    console.log(2)
      $("#home").addClass('nav-link-home-color')

  }
  $('.nav-content a').each(function () {
    if ($($(this))[0].href == String(window.location)){
      $(this).addClass('active');
      $(this).parent().parent().addClass('show');
    }
  });
  /**
   * 语言设置
   * language setting
   */
  if(localStorage.language){
    var  lagtype = localStorage.getItem('language');

    $("#chanageLag").val(lagtype)
  }else{
    var lagtype = 'en-US';
    localStorage.language = lagtype
  }
// 请求json文件Request json file
  $.ajax({
    method: "GET",      // 设置请求方式为GET
    url:"\\resources\\assets\\language\\"+lagtype+".json", // 加载本地json文件
    dataType: "json",   // 设置数据类型为json
    success: function(e){
      $(".dateChange").each(function(index, element) {
        console.log(element)
      });
      $(".lang").each(function(index, element) {
        $(this).text(e[$(this).attr("key")]);
      });
    }
  })
// 默认英文 English by default
  var lag_url ="\\resources\\assets\\language\\en-CN.json";
  $("#chanageLag").on("change",
    function() {
      localStorage.language = $(this).val();
      if($(this).val()=='de-DE')
      {//德文 German language
        var lag_url ="\\resources\\assets\\language\\de-DE.json";
      }else if($(this).val()=='en-US'){//英文
        var lag_url ="\\resources\\assets\\language\\en-US.json";
      }else if($(this).val()=='cn-CN')
      {//简体中文 simplified Chinese
        var lag_url ="\\resources\\assets\\language\\cn-CN.json";
      }else if($(this).val()=='it-IT')
      {//it
        var lag_url ="\\resources\\assets\\language\\it-IT.json";
      }
      window.location.reload()
  });
  //
  // 设置本地显示时间格式；获取时间格式类型
  if(localStorage.dateFormat){
    var  dateFormat = localStorage.getItem('dateFormat');
    console.log(dateFormat)
  }else{
    ajaxGeneral()
  }


  if(localStorage.language_forbid){
    var  language_forbid = localStorage.getItem('language_forbid');
    if(language_forbid  == 1 ){
      $('#chanageLag').removeClass('secontpassWarning');
    }else{
      $('#chanageLag').addClass('secontpassWarning');
    }
    console.log(language_forbid)
  }else{
    ajaxGeneral()
  }
  // 设置本地显示时间格式；获取时间格式类型
  if(localStorage.appname){
    var  appname = localStorage.getItem('appname');
    $('.appname').text(appname);
  }else{
    ajaxApp()
  }
  if(localStorage.appLogo){
    var  appLogo = localStorage.getItem('appLogo');
    $('.logo_imag').attr('src',appLogo);
  }else{
    ajaxApp();
  }
  if(localStorage.faviconPath){
    var  faviconPath = localStorage.getItem('faviconPath');
    $('.icon').attr('href',host+'/'+faviconPath);
  }else{
    ajaxApp()
  }
  function ajaxApp(){
    $.ajax({
      method: "GET",      // 设置请求方式为GET
      url:host+"/api/appsetting", // 加载本地json文件
      dataType: "json",   // 设置数据类型为json
      success: function(data){
        console.log(data)
        if(data.payload.data.length > 0){
          localStorage.appname = data.payload.data[0].AppName;
          localStorage.appLogo = data.payload.data[0].logoPath;
          localStorage.faviconPath = data.payload.data[0].faviconPath;
          $('.logo_imag').attr('src',appLogo);
          if( data.payload.data[0].AppName)  $('.appname').text(appname);
          if(data.payload.data[0].faviconPath)  $('.icon').attr('href',host+'/'+data.payload.data[0].faviconPath);
        }else {
          localStorage.appname = 'Rental App';
          localStorage.appLogo = '/resources/assets/img/logo.png';
          localStorage.faviconPath = '/resources/assets/img/logo.png';
        }
      }
    })
  }

  $.extend({
    ajaxUpdateAppinfo:function (){
        ajaxApp();
    }
  })
  function ajaxGeneral(){
    $.ajax({
      method: "GET",      // 设置请求方式为GET
      url:host+"/api/general", // 加载本地json文件
      dataType: "json",   // 设置数据类型为json
      success: function(data){
        if(data.payload.data.length > 0){
          localStorage.dateFormat = data.payload.data[0].date_format;
          localStorage.language_forbid = data.payload.data[0].language_forbid;
        }else {
          localStorage.dateFormat = 1;
          localStorage.language_forbid = 2;
        }
      }
    })
  }
  // $("#date").datepicker();

  // $(".date").datepicker({
  //   changeMonth: true,
  //   changeYear: true,
  //   minView:"Hours"
  //   // numberOfMonths:1,//显示几个月
  //   // showButtonPanel:true,//是否显示按钮面板
  //   // dateFormat: 'yy-mm-dd',//日期格式
  //   // clearText:"清除",//清除日期的按钮名称
  //   // closeText:"关闭",//关闭选择框的按钮名称
  //   // showMonthAfterYear:true,//是否把月放在年的后面
  //   // onSelect: function(selectedDate) {//选择日期后执行的操作
  //   //   alert(selectedDate);
  //   // }
  //
  // });
})();

/**
 *
 * @param data 时间格式
 * @param type  is_detail == 1 显示时分秒
 * @returns {*|string}
 */
function dateFormaChanget(data,is_detail= '')
{
  var type = localStorage.getItem('dateFormat');
  if(type == 1){
    return data;
  }
  var d2=new Date(data)

  if(type == 2)  data = (d2.getMonth()+1)+"/"+d2.getDate()+"/"+d2.getFullYear();

  if(type == 3) data =d2.getDate() +"-"+(d2.getMonth()+1)+"-"+d2.getFullYear();

  if(is_detail == 1)   data = data + " "+d2.getHours()+":"+d2.getMinutes()

  return data;

}


//获取指定名称的cookie的值
function getcookie(objname){
  var arrstr = document.cookie.split("; ");
  for(var i = 0;i < arrstr.length;i ++){
    var temp = arrstr[i].split("=");
    if(temp[0] == objname) return unescape(temp[1]);
  }
}