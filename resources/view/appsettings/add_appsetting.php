<!--  Add General Content -->
<div class="modal fade" id="appsettings_form_add" tabindex="-1">
    <div class="modal-dialog modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Set APP Info </h5>

                         <!-- Profile Edit Form -->
                        <form id="add_appsettings_info" class="needs-validation" novalidate>
                            <div class="row mb-3">
                                <label for="AppName" class="col-md-3 col-lg-3 col-form-label">AppName</label>
                                <div class="col-md-4 col-lg-4">
                                    <input name="AppName" type="text" class="form-control eidt_AppName" id="AppName" value="" required>
                                    <div class="valid-feedback">
                                        Correct input format
                                    </div>
                                    <div class="invalid-feedback">
                                        Please provide a Name
                                    </div>
                                </div>
                            </div>

<!--                            <div class="row mb-3">-->
<!--                                <label for="surname" class="col-md-3 col-lg-3 col-form-label">OnPremise</label>-->
<!--                                <div class="col-md-2 col-lg-2">-->
<!--                                    <input name="OnPremise" type="number" class="form-control eidt_OnPremise" id="OnPremise" value="" required>-->
<!--                                    <div class="valid-feedback">-->
<!--                                        Correct input format-->
<!--                                    </div>-->
<!--                                    <div class="invalid-feedback">-->
<!--                                        Please provide a Surname-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->


                            <div class="row mb-6">
                                <label for="surname" class="form-check-label col-md-3 col-lg-3 col-form-label">OnPremiseEnable</label>
                                <div class="col-md-2 col-lg-2">
                                    <input  class='form-check-input  OnPremisead' type='checkbox' name='OnPremise'  > <i class="bi bi-check"></i>
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="email"class="col-md-3 col-lg-3 col-form-label">IP</label>
                                <div class="col-md-2 col-lg-2">
                                    <input name="IP" type="text" class="form-control eidt_IP" id="IP" value="" required>
                                    <div class="valid-feedback">
                                        Correct input format
                                    </div>
                                    <div class="invalid-feedback">
                                        Please provide a Email
                                    </div>
                                </div>
                            </div>
                            <div class=" mb-3">
                                    <div>
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </div>
                            </div>

                        </form><!-- End Profile Edit Form -->

                    </div>
                </div>

            </div>

        </div>
    </div>
</div><!-- End Modal Dialog Scrollable-->
