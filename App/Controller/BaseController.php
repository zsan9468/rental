<?php

namespace App\Controller;

use App\Resources\PermissionResource;

class BaseController
{

    /**
     *Processing authority
     */
    public function __construct()
    {

            if(!empty($_REQUEST['token']) && $_REQUEST['token'] == 'viKnctQMWr'){

            }else{
                if(empty($_SESSION['userInfo'])){
                    ApiOutputController::ApiOutput('',607,'');
                }
                $currentUserPermission = $_SESSION['permission']['sqlAction'];
//                     获取uri和提交方式
                $method = $_SERVER['REQUEST_METHOD'];
                $uri = $_SERVER['REQUEST_URI'];
                if($method == "GET")
                {
                    $uri = (explode('?',$uri))[0];
                }
                //自己修改自己
                if (isset($requestData['modify_type']) && isset($requestData['modify']) == 1 ) {
                    $tablesData['email'] =   $requestData['email'];
                }
                $requestData =  $this->getQueryJson();
                if($requestData['modify_type'] != 1){
                    //获取当前权限集合
                    $permissionResource =  new PermissionResource();
                    $permissionId = $permissionResource->checkPermission($method,$uri);
                    if(count($permissionId['sqlAction'])>0){
                        $permissionLisId = $permissionId['sqlAction'][0]['id'];
                        $result= in_array($permissionLisId,$currentUserPermission);
                        if ($result==false)
                        {
                            ApiOutputController::ApiOutput('',608,'');
                        }
                    }
                }

            }

    }


    public static $headerTexts = [
        200 => 'HTTP/1.1 200 OK',
        400 => 'HTTP/1.1 500 Internal Server Error',
        404 => 'HTTP/1.1 404 Not Found',
    ];

    /**
     * Get URI elements.
     */
    protected function getUriSegments()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', $uri);
        return $uri;
    }

    /**
     * Get querystring params.
     */
    protected function getQueryStringParams()
    {
        parse_str($_SERVER['QUERY_STRING'], $query);
        return $query;
    }

    /**
     * Get queryjson data.
     */
    protected function getQueryJson()
    {
        // FIXME Query builders can only accept arrays
        $queryJson = json_decode(file_get_contents('php://input'), true);
        if (is_null($queryJson)) {
            $queryJson = [];
        }
        return $queryJson;
    }
}
