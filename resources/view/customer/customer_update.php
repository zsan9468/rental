<form class="row g-3 was-validated" id="customer_form_update" novalidate>
    <div class="col-md-4">
        <label for="firstName" class="form-label">firstName</label>
        <input type="text" class="form-control" id="firstName" name="firstName" value="" required>
        <input type="hidden" class="form-control" id="id" name="id" value="" required>
        <div class="valid-feedback">
            firstName Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a firstName
        </div>
    </div>

    <div class="col-md-4">
        <label for="lastName" class="form-label">lastName</label>
        <input type="text" class="form-control" id="lastName" name="lastName" value="" autocomplete
               required>
        <div class="valid-feedback">
            lastName Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a lastName
        </div>
    </div>
    <div class="col-md-4">
        <label for="birthDate" class="form-label">birthDate</label>
        <input type="date" class="form-control" id="birthDate" name="birthDate" value="" required>
        <div class="valid-feedback">
            birthDate Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide birthDate
        </div>
    </div>
    <div class="col-md-6">
        <label for="companyName" class="form-label">companyName</label>
        <input type="text" class="form-control" id="companyName" name="companyName" value="">
    </div>


    <div class="col-md-10">
        <label for="street1" class="form-label">street1</label>
        <input type="text" class="form-control" id="street1" name="street1" value="" autocomplete
               required>
        <div class="valid-feedback">
            street1 Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a street2
        </div>
    </div>

    <div class="col-md-10">
        <label for="street2" class="form-label">street2</label>
        <input type="text" class="form-control" id="street2" name="street2" value="15">
    </div>
    <div class="col-md-3">
        <label for="city" class="form-label">city</label>
        <input type="text" class="form-control" id="city" name="city" value=""  required>
        <div class="valid-feedback">
            city Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a city
        </div>
    </div>

    <div class="col-md-4">
        <label for="country" class="form-label">country</label>
        <input type="text" class="form-control" id="country" name="country" value=""        required>
        <div class="valid-feedback">
            country Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a country
        </div>

    </div>
    <div class="col-md-4">
        <label for="postalCode" class="form-label">postalCode</label>
        <input type="number" maxlength="8" class="form-control" id="postalCode" name="postalCode" value=""       required>
        <div class="valid-feedback">
            postalCode Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a postalCode
        </div>
    </div>

    <div class="col-md-4">
        <label for="postalCode" class="form-label">telephone</label>
        <input type="text" maxlength="50" class="form-control" id="telephone" name="telephone" value="" >
    </div>

    <div class="col-md-4">
        <label for="mobile" class="form-label">mobile</label>
        <input type="text" maxlength="50" class="form-control" id="mobile" name="mobile" value="">
    </div>

    <div class="col-md-8">
        <label for="email" class="form-label">Email</label>
        <i>@</i><input type="email" class="form-control" id="email" name="email" value="">
    </div>


    <div class="col-md-4">
        <label for="rateCode" class="form-label">rateCode</label>
        <input type="number"  class="form-control" id="rateCode" name="rateCode" value="">
    </div>

    <div class="col-md-4">
        <label for="salesVolume" class="form-label">salesVolume</label>
        <input type="number" maxlength="11" class="form-control" id="salesVolume" name="salesVolume" value="">
    </div>


    <div class="col-md-12">
        <label for="" class="form-label"> <text class="text-primary" > CustomerProperty</text></label>
        <div class="customer_properties">

        </div>

    </div>


    <div class="col-md-12">
        <label for="remarks" class="form-label">remarks</label>
        <textarea type="text" class="form-control remarks" id=""  value="" ></textarea>
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary"   type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
