<?php
/**
 * Created by : phpstorm
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/3/14
 * Time:  15:25
 */


class DB
{
    // Private a static variable to determine whether to instantiate
    private static $db_instance;
    //statement host

//    Private a clone method to prevent cloning outside the object
    private function __clone()
    {
    }

    /**
     *  $dsn = "mysql:host=192.168.10.85;dbname=mydb;charset=utf8;port=49153";
    try{
    $this->db = new \PDO($dsn,'rental_user','4AxirucAbozA');
     * A private constructor prevents the object from being instantiated outside
     */
    private function __construct()
    {
        $dsn = "mysql:host=192.168.10.85;dbname=mydb;charset=utf8;port=49153";
        try{
            $this->db = new \PDO($dsn,'rental_user','4AxirucAbozA');
            $this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            die('Database connection failure!' . $e->getMessage());
        }
    }

    // Exposes a static method for easy invocation outside of an object
    public static function connect_database()
    {
        if (self::$db_instance == null) {
            self::$db_instance = new self;
        }
        return self::$db_instance;
    }
}
