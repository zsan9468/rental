<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date: 2023/2/23
 * Time: 22:39
 * The singleton pattern builds the database connection
 */

namespace App\Models;

use App\Lib\DB;
use App\Resources\CostcenterSelectionResource;

class UserModel
{

    protected $table = 'user';
    protected $DB;
    protected $field =  'u.userId,u.profile_photo,u.name,u.username, u.user_type,u.surname,u.email,c.name as costName,c.city,c.id as costcerter_id,c.postalCode';
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }

    /**
     * @param $username Username
     * @return mixed|string User information
     */
    public function getUserInfo($username)
    {
        $sql = "select * from user where username = '$username' limit 1 ";
        $res = $this->DB->query($sql);
        $result = $res->fetch();
        return  $result;
    }

    /**
     * @return void
     */
    public function create($data)
    {
        // 检查用户名是否存在 ，存在返回提示信息
        $userresult = DB::link()->table($this->table)->where("username = '" . $data['username'] . "'")->get();
        if ($userresult) {
            return false;
        }
        // 不存在创建新的用户
        $newUserInfo =  DB::link()->table($this->table)->insert($data);
        if ($newUserInfo) {
            return $this->DB->lastInsertId();
        }
    }

    /**
     * userId = 1 超级管理员
     * @param $start
     * @param $length
     * @param $search
     * @param $draw
     * @param $order
     * @return array
     */
    public function getUserList($start = 0, $length = 0, $search = '', $order = '')
    {
        $returnArray = array();
        if ($search == '') {
            $sqlList = "select $this->field from `user` as u
                        LEFT JOIN costcenterselection as cs ON  u.userId = cs.userId
                        LEFT JOIN costcenter as c ON c.id  = cs.costCenterId 
                        WHERE u.userId <> 1
                        ORDER BY u.$order limit $start,$length";
            $toTal = DB::link()->table($this->table)->where('userId <> 1')->count();
        } else {
            $whereSql = vsprintf(" u.userId <> 1 and u.name REGEXP '%s' or c.city REGEXP '%s' or c.postalCode REGEXP '%s'", array($search, $search, $search));

            $sqlList = "select  $this->field from `user` as u
                        LEFT JOIN costcenterselection as cs ON  u.userId = cs.userId
                        LEFT JOIN costcenter as c ON c.id  = cs.costCenterId 
                        WHERE $whereSql 
                        ORDER BY u.$order limit $start,$length";
            $toTal = "select  count(*) from `user` as u
                        LEFT JOIN costcenterselection as cs ON  u.userId = cs.userId
                        LEFT JOIN costcenter as c ON c.id  = cs.costCenterId 
                        WHERE $whereSql ";
        }

        $result = DB::link()->query($sqlList);

        $returnArray['recordsFiltered'] = (int)$toTal;
        $returnArray['data'] = $result;
        $returnArray['recordsTotal'] = count($result);
        return $returnArray;
    }
    public function getUserAccordingId($id, $details = false)
    {
        if ($details == true) {
            $sqlList = "select  $this->field from `user` as u
                        LEFT JOIN costcenterselection as cs ON  u.userId = cs.userId
                        LEFT JOIN costcenter as c ON c.id  = cs.costCenterId 
                        WHERE u.userId = " . $id;
            $dbhandle = $this->DB->query($sqlList);
            $result = $dbhandle->fetchAll();
            return  $result;
        }
        return DB::link()->table($this->table)->where("userId = $id")->get();
    }


    public function update($data, $userId)
    {
        return DB::link()->table($this->table)->where("userId = $userId")->update($data);
    }

    public function delete($id)
    {
        try {
            $this->DB->beginTransaction();
            //            Delete user permissions
            $userPermissionSelection = new UserPermiasionSelectionModel();
            $userPermissionSelection->delete($id);
            //            Delete user correlation costcerter

            $costcenterSelection = new CostcenterSelectionResource();
            $costcenterSelection->delete($id);

            //           Delete user information
            DB::link()->table($this->table)->where("userId = " . $id)->delete();
            $this->DB->commit();
            return true;
        } catch (\PDOException $e) {
            $this->DB->rollback();
            return false;
        }
    }
}
