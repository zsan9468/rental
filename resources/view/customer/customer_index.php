<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">CUSTOMER <span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary" id="customer_add_view">
                                            Create Customer Info
                                        </button>
                                    </div>
                                </div>
                                <table id="customer_user_tbale" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="customer_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.customer">Create New
                            Customer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'customer_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="update_customer_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.customer">Update
                            Customer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'customer_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {


        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'firstName', title: "firstName", name: "firstName" },
            { data: 'lastName', title: "lastName", name: "lastName" },
            { data: 'birthDate', title: "birthDate", name: "birthDate",render:(data,type,row)=>{
                    var dateInfo  =  dateFormaChanget(data)
                    return dateInfo;
                } },
            { data: 'city', title: "city", name: "city", "orderable" : false },
            // { data: 'country', title: "country", name: "country", },
            { data: 'street1', title: "street1", name: "street1" },
            // { data: 'postalCode', title: "postalCode", name: "postalCode" },
            { data: 'telephone', title: "telephone", name: "telephone" },
            { data: 'email', title: "email", name: "email" },
            { data: 'companyName', title: "companyName", name: "companyName" },
            { data: 'createdDateTime', title: "create  Time", name: "createdDateTime" },
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#customer_user_tbale').undelegate('tbody #' + upodateBtn, 'click');
                    $('#customer_user_tbale').on('click', 'tbody #' + upodateBtn, function (e) {

                        $('#update_customer_add_model').modal('show');

                        $(".UpdatePropertySTyle .row").remove()
                        var responses = formAjaxHy('/api/customer?id='+row.id, data, 'get');
                        console.log(1)
                        console.log(responses)
                        // get_user_correlation(responses,'update_menu','update_update_cost_center')
                        // var userInfo = formAjaxHy('/api/user_action?id='+row.userId,'', 'get');
                        $("#customer_form_update input[name=id]").val(row.id);
                        $("#customer_form_update input[name=firstName]").val(row.firstName);
                        $("#customer_form_update input[name=lastName]").val(row.lastName);
                        $("#customer_form_update input[name=birthDate]").val(row.birthDate);
                        $("#customer_form_update input[name=companyName]").val(row.companyName);
                        $("#customer_form_update input[name=street1]").val(row.street1);
                       $("#customer_form_update input[name=street2]").val(row.street2);
                       $("#customer_form_update input[name=city]").val(row.city);
                       $("#customer_form_update input[name=country]").val(row.city);
                        $("#customer_form_update input[name=postalCode]").val(row.postalCode);
                       $("#customer_form_update input[name=telephone]").val(row.telephone);
                        $("#customer_form_update input[name=mobile]").val(row.mobile);
                       $("#customer_form_update input[name=email]").val(row.email);
                        $("#customer_form_update input[name=rateCode]").val(row.rateCode);
                        $("#customer_form_update input[name=salesVolume]").val(row.salesVolume);
                      $("#remarks").val(row.salesVolume);
                        if(responses.payload.data.customerproperties){
                            var properties =  responses.payload.data.customerproperties;
                            $("#customer_form_update .customer_properties").html('')
                            for(var i = 0 , length = properties.length ;i<length ;i++)
                            {
                                $("#customer_form_update .customer_properties").append("<div class='row propertyNameClss'>"+
                                    "<div class='col-md-3'>"+properties[i].costumerPropertyName+"</div>"+
                                    " <div class='col-md-4'>"+
                                    "<input type='hidden' maxlength='11' class='form-control' id='propertyName' name='costumerPropertyName' value='"+properties[i].costumerPropertyName+"'>"+
                                    "<input type='hidden' maxlength='11' class='form-control' id='' name='customerPropertiesId' value='"+properties[i].customerPropertiesId+"'>"+
                                    "<input type='hidden' maxlength='11' class='form-control' id='' name='selectionId' value='"+properties[i].selectionId+"'>"+
                                    "<input type='text' maxlength='11' class='form-control' id='propertyName' name='customerPropertyValue' value='"+properties[i].customerPropertyValue+"'>"+
                                    "</div>"+
                                    " </div>")
                            }
                        }

                    })

                    $('#customer_user_tbale').undelegate('tbody #' + deleteBtn, 'click');
                    $('#customer_user_tbale').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show')
                        $('#del_id').text(row.id)
                        $('#del_url').text('/api/customer')

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/customer', 'get', 'customer_user_tbale', columns,);

        $('#customer_add_view').click(function (e) {
            $(".customer_properties").html('');
            $('#customer_add_model').modal('show');
            data = '';
            var responses = formAjaxHy('/api/customer_properties_list', data, 'get');
            if(responses.payload.code==200){
                var properties =  responses.payload.data;
                for(var i = 0 , length = properties.length ;i<length ;i++)
                {
                    $("#customer_form_create .customer_properties").append("<div class='row propertyNameClss'>"+
                    "<div class='col-md-3'>"+properties[i].propertyName+"</div>"+
                        " <div class='col-md-4'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='propertyName' name='costumerPropertyName' value='"+properties[i].propertyName+"'>"+
                        "<input type='hidden' maxlength='11' class='form-control' id='' name='customerPropertiesId' value='"+properties[i].id+"'>"+
                        "<input type='text' maxlength='11' class='form-control' id='propertyName' name='customerPropertyValue' value=''>"+
                        "</div>"+
                        " </div>")
                }
            }

        })
// Obtain information about adding customer information
        $('#customer_form_create').submit(function (e) {
            var data =  $.customerInfoBiaodan('customer_form_create',e,'create')
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/customer','post',data,
                function(data){
                    if(data.payload.code == 200){
                        commonUtil.message('Create success!')
                        $('#customer_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                        commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })

        })
        $('#customer_form_update').submit(function (e) {
            var id = $("#customer_form_update input[name=id]").val();
            var data =  $.customerInfoBiaodan('customer_form_update',e,'update')
            data.customer.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/customer','put',data,
                function(data){
                    if(data.payload.code == 200){
                        $('#update_customer_add_model').modal('hide');
                        commonUtil.message('Update success！')
                        table.ajax.reload();
                    }else{
                        commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>