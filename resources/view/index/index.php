<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>
<body>
  <!-- ======= Header ======= -->
  <?php include './../../layout/header.php';?>
  <!-- ======= Sidebar ======= -->
  <?php include './../../layout/sidebar.php';?>

  <main id="main" class="main">

      <div class="pagetitle">
          <h1 class="lang" key="dashboard.widget2.title">Dashboard</h1>
          <nav>
              <ol class="breadcrumb"
                  <li class="breadcrumb-item" ><a href="../index/index.php"  class="lang"  key="dashboard.widget2.title" >Home</a>/</li>
                  <li class="breadcrumb-item active lang" key="dashboard.widget1.title" >Dashboard</li>
              </ol>
          </nav>
      </div><!-- End Page Title -->

      <section class="section dashboard">
          <div class="row">

              <!-- Left side columns -->
              <div class="col-lg-8">
                  <div class="row">
                          <!-- Sales Card -->
                          <div class="col-xxl-4 col-md-6">
                              <div class="card info-card sales-card">

                                  <div class="card-body">
                                      <h5 class="card-title lang" key="dashboard.total.units"> Total Units</h5>

                                      <div class="d-flex align-items-center">
                                          <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                              <i class="bi bi-check-circle-fill"></i>
                                          </div>
                                          <div class="ps-3  card-title">
                                              <h6 class="rentalUnitCount"></h6>
                                          </div>
                                      </div>
                                  </div>

                              </div>
                          </div><!-- End Sales Card -->

                                <!-- Revenue Card -->
                          <div class="col-xxl-4 col-md-6">
                              <div class="card info-card revenue-card">

                                  <div class="card-body">
                                      <h5 class="card-title lang" key="dashboard.total.units">Available Units <span> </span></h5>

                                      <div class="d-flex align-items-center">
                                          <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
<!--                                              <i class="bi bi-currency-dollar"></i>-->
                                              <i class="bi bi-clipboard-minus"></i>
                                          </div>
                                          <div class="ps-3 card-title">
                                              <h6 class="AvailableUnits" ></h6>
                                          </div>
                                      </div>
                                  </div>

                              </div>
                          </div><!-- End Revenue Card -->

                      <!-- Customers Card -->
                      <div class="col-xxl-4 col-xl-12">

                          <div class="card info-card customers-card">



                              <div class="card-body">
                                  <h5 class="card-title lang " key='dashboard.customer.number'>Customers <span></span></h5>

                                  <div class="d-flex align-items-center">
                                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                          <i class="bi bi-people"></i>
                                      </div>
                                      <div class="ps-3  card-title">
                                            <h6 class="customer_number">0</h6>
<!--                                           <div> <span>Added today:<text class="toady_customer_num">0</text> </span></div>-->
<!--                                          <div> <span>Added yesterday <text class="yesterday_customer_num">0</text></span></div>-->
                                      </div>
                                  </div>

                              </div>
                          </div>

                      </div><!-- End Customers Card -->

                  </div>
              </div><!-- End Left side columns -->
          </div>
      </section>

  </main><!-- End #main -->
  <!-- End #main -->

  <!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
  <!-- Vendor JS Files -->
 <?php include './../../layout/js.html' ?> 
  <script>
    $(document).ready(function () {
        var agreementTotalNum = 0,agreementValidNum=0,AvailableUnitsNum=0,rentalUnitCount = 0;
        $.ajaxUnit('/api/panel_index','get', {},
            function (data){
                if(data.payload.code == 200){
                    if (data.payload.data.customer.length !== 0)
                    {
                        $(".customer_number").text(data.payload.data.customer.totalCount)
                        $(".toady_customer_num").text(data.payload.data.customer.today.length)
                        $(".yesterday_customer_num").text(data.payload.data.customer.yesterday.length)
                    }
                    agreementTotalNum = data.payload.data.agreement.agreementTotalNum;
                    agreementValidNum = data.payload.data.agreement.agreementValidNum;
                    rentalUnitCount = data.payload.data.rentalUnitCount;
                    AvailableUnitsNum = rentalUnitCount-agreementValidNum
                    $(".AvailableUnits").text(AvailableUnitsNum)
                    $(".rentalUnitCount").text(rentalUnitCount)
                }else{
                     commonUtil.message(data.payload.data);
                }
            },
            function (data)
            {
            }
        )

    });
  </script>
</body>
</html>