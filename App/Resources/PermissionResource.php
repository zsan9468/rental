<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/3/1
 * Time:  18:53
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\PermissionModel;

class PermissionResource
{
    public function permissionList($id = false)
    {
        $permissionModel = new PermissionModel();
        $result = $permissionModel->getAllPerssionList($id);
        return $result;
    }

    public function permissionListClassify()
    {
        $permissionModel = new PermissionModel();
        $result = $permissionModel->getAllPerssionListAdmin();
        return $result;
    }

    //    checkPermission
    public function checkPermission($method, $uri)
    {
        $permissionModel = new PermissionModel();
        $result = $permissionModel->getSignalPermission($method, $uri);
        return $result;
    }


    //    权限处理
    public function arrayPidProcess($data, $res = array(), $pid = '0')
    {
        //        var_dump($data);
        foreach ($data as $k => $v) {
            if ($v['father_id'] == $pid) {
                $res[$v['id']]['info'] = $v;
                $res[$v['id']]['child'] = $this->arrayPidProcess($data, array(), $v['id']);
            }
        }
        return $res;
    }

    public function getSignal($id)
    {
        $permissionModel = new PermissionModel();
        $result = $permissionModel->getSignal($id);
        if ($result) {
            ApiOutputController::ApiOutput($result, 200, '');
        }
    }

    /**
     * @param $data
     * @return void
     */
    public function create($data)
    {
        $rules = [
            'menu_name' => ['required'],
        ];
        $messages = [
            'menu_name.required' => 'The menu_name field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $stroyData['menu_name'] = $data['menu_name'];
        $permissionModel = new  PermissionModel();
        if (isset($data['father_id'])) {
            $stroyData['father_id'] = $data['father_id'];
            $stroyData['type'] =  $data['type'];
            $stroyData['menu_order'] = $data['method'];
            if ($stroyData['father_id'] != 0) {
                $stroyData['menu_url'] = $data['menu_url'];
                $stroyData['method'] = $data['method'];
                if ($stroyData['type'] == 2) {
                    $isExistRecord = $permissionModel->getSignalPermission($stroyData['method'], $stroyData['menu_url']);
                    if ($isExistRecord['sqlAction']) {
                        ApiOutputController::ApiOutput('', 702, '');
                    }
                }
            } else {
                $stroyData['menu_icon'] = $data['menu_icon'];
                $stroyData['menu_order'] = $data['menu_order'];
            }
            $result = $permissionModel->story($data);
            if ($result) {
                ApiOutputController::ApiOutput('', 200, '');
            } else {
                ApiOutputController::ApiOutput('', 701, '');
            }
        } else {
            ApiOutputController::ApiOutput('', 501, '');
        };
    }
    public function update($data)
    {
        $permissionModel = new PermissionModel();
        $id = $data['id'];
        unset($data['id']);
        $result = $permissionModel->update($id, $data);
        ApiOutputController::ApiOutput('', 200, '');
    }

    /**
     * @return void
     */
    public function delete($data)
    {
        $rules = [
            'id' => ['required'],
            'menu_name' => ['required'],
        ];
        $messages = [
            'id.required' => 'The id field is required.',
            'menu_name.required' => 'The menu_name field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $id = $data['id'];
        $perMissonModel  = new PermissionModel();
        $perMissonRecord = $perMissonModel->getSignal(false, 'father_id = ' . $id);
        if ($perMissonRecord && !empty($perMissonRecord)) {
            ApiOutputController::ApiOutput('', 703, '');
        }
        $result = $perMissonModel->delete($id);
        if ($result) {
            ApiOutputController::ApiOutput('', 200, '');
        } else {
            ApiOutputController::ApiOutput('', 704, '');
        }
    }
    /**
     * @param $res
     * @return array
     * 处理[] 数组和array数组之间的转换 :Handle conversions between [] arrays and array arrays
     */
    public function objArrayToArray($res)
    {
        $arr = array();
        foreach ($res as $item) {
            $item['son'] = array();
            if (empty($item['info']['method'])) {
                $item['info']['method'] = '<i></i>';
            }
            if (empty($item['info']['menu_url'])) {
                $item['info']['menu_url'] = '<i> </i>';
            }
            if (empty($item['info']['menu_icon'])) {
                $item['info']['menu_icon'] = '<i> </i>';
            }
            if (!empty($item['child'])) {

                foreach ($item['child'] as $childItem) {

                    if (empty($childItem['info']['method'])) {
                        $childItem['info']['method'] = '<i></i>';
                    }
                    if (empty($childItem['info']['menu_url'])) {
                        $childItem['info']['menu_url'] = '<i> </i>';
                    }
                    if (empty($childItem['info']['menu_icon'])) {
                        $childItem['info']['menu_icon'] = '<i> </i>';
                    }
                    array_push($item['son'], $childItem);
                }
            }
            array_push($arr, $item);
        }
        return $arr;
    }
}
