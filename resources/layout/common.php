<?php

session_start();
if(empty($_SESSION)){
     Header("Location: /web/login/login");
}

//权限控制
//获取权限几集合
$method = '';
$uri = $_SERVER['REQUEST_URI'];
$currentUserPermission = $_SESSION['permission']['sqlMenu'];
$curidArr = [];
foreach ($currentUserPermission as $curItem)
{
    foreach ($curItem['child'] as $v)
    {
       array_push($curidArr,$v['info']['id']);
    }
}

include_once "DB.php";
$db = DB::connect_database()->db;
$sql = 'select id,menu_url from  permission WHERE type = 1 and  father_id <> 0';
$tm = $db->query($sql);
$resultData = $tm->fetchAll();
$permissionID = [];
foreach ($resultData as $item){
    if('/web/'.$item['menu_url'] == $uri){
         $result = in_array($item['id'] ,$curidArr);
         if($result == false){
             $noPermission = true;
         }
         break;
    }
}
if($noPermission ===  true)
{
    Header("Location: /web/index/index");
}

