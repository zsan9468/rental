<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\CustomerResource;

class CustomerController extends BaseController
{
    public function create()
    {
        (new CustomerResource)->propertiesList();
    }

    public function get()
    {
        $data = $this->getQueryStringParams();

        (new CustomerResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new CustomerResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        $id = $data['id'];
        (new CustomerResource)->destroy($id);
    }
}
