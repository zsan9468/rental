<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\AgreementResource;

class AgreementController extends BaseController
{
    /**
     * Termination of agreement, return of rental unit 
     * 协议终止，归还租赁单元
     * @DateTime 2023-03-16
     */
    public function updateRentalunitStatusTyp()
    {
        $data = $this->getQueryJson();
        (new AgreementResource)->updateRentalunitStatusTyp($data);
    }

    /**
     * @Description Get Agreement Insurence options
     * @DateTime 2023-03-10
     */
    public function createInsurence()
    {
        (new AgreementResource)->InsurenceList();
    }

    /**
     * @Description Get Agreement Properties options
     * @DateTime 2023-03-10
     */
    public function createProperty()
    {
        (new AgreementResource)->PropertiesList();
    }

    /**
     * @Description Get Agreement Extra Options
     * @DateTime 2023-03-10
     */
    public function createExtra()
    {
        (new AgreementResource)->ExtraList();
    }

    public function get()
    {
        $data = $this->getQueryStringParams();
        (new AgreementResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new AgreementResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        $id = $data['id'];
        (new AgreementResource)->destroy($id);
    }
}
