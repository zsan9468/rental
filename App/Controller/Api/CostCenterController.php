<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/24
 * Time:  12:24
 */

namespace App\Controller\Api;

use App\Controller\ApiOutputController;
use App\Controller\BaseController;
use App\Resources\CostCenterResource;

class CostCenterController extends BaseController
{
    /**
     * @return void
     */
        public function costCentorAction()
        {
            $data = $this->getQueryJson();
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $costCenterRe = new CostCenterResource();
                $costCenterRe->store($data);
            }
            if($_SERVER['REQUEST_METHOD'] == 'PUT')
            {
                $costCenterRe = new CostCenterResource();
                $costCenterRe->update($data);
            }
            if($_SERVER['REQUEST_METHOD'] == 'DELETE')
            {
                $costCenterRe = new CostCenterResource();
                $costCenterRe->destroy($data);
            }
        }

    /**
     * @return void
     */
        public function GetcostCentorAction()
        {
                $data = $_GET;
                $costCenterRe = new CostCenterResource();
                $resultArr = $costCenterRe->index($data);
                ApiOutputController::ApiOutput($resultArr,200,'');
        }

}