<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalDamagePropertiesResource;

class RentalDamagePropertiesController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalDamagePropertiesResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalDamagePropertiesResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalDamagePropertiesResource)->destroy($data);
    }
}
