<?php

use App\Lib\Router;

// Api Homepage
// For Testing Functions
Router::any('/api', 'App\Controller\Api\ApiIndexController@index');

/**
 * ================================================================
 * Systems
 * ================================================================
 */
//Control panel instrument
Router::get('/api/panel_index', 'App\Controller\Api\IndexController@Index');

// App Settings
Router::get('/api/appsetting', 'App\Controller\Api\AppSettingController@get');
Router::post('/api/appsetting', 'App\Controller\Api\AppSettingController@post');
Router::put('/api/appsetting', 'App\Controller\Api\AppSettingController@post');
Router::delete('/api/appsetting', 'App\Controller\Api\AppSettingController@delete');

// General Settings
Router::get('/api/general', 'App\Controller\Api\GeneralController@get');
Router::post('/api/general', 'App\Controller\Api\GeneralController@post');
Router::put('/api/general', 'App\Controller\Api\GeneralController@post');
Router::delete('/api/general', 'App\Controller\Api\GeneralController@delete');

/**
 * ================================================================
 * User
 * ================================================================
 */
//User Login
Router::post('/api/user/login', 'App\Controller\Api\LoginController@login');

//user Settings
Router::any('/api/get_users', 'App\Controller\Api\UserController@getUserAction');
Router::any('/api/get_user_info', 'App\Controller\Api\UserController@getAddUserCorrelation');
Router::any('/api/user_action', 'App\Controller\Api\UserController@userInfoAction');
Router::post('/api/user_changepassword', 'App\Controller\Api\UserController@chanagePassword');

//CostCenter
Router::any('/api/cost_centor_action', 'App\Controller\Api\CostCenterController@costCentorAction');
Router::get('/api/get_cost_centor_action', 'App\Controller\Api\CostCenterController@GetcostCentorAction');

//permissionGroup
Router::any('/api/permission', 'App\Controller\Api\PermissionController@getPermission');

/**
 * ================================================================
 * CustomerUnit
 * ================================================================
 */
// Customer Settings
Router::get('/api/customer', 'App\Controller\Api\CustomerController@get');
Router::post('/api/customer', 'App\Controller\Api\CustomerController@post');
Router::put('/api/customer', 'App\Controller\Api\CustomerController@post');
Router::delete('/api/customer', 'App\Controller\Api\CustomerController@delete');

Router::get('/api/customer_properties_list', 'App\Controller\Api\CustomerController@create');

// Customer Properties Settings
Router::get('/api/customer_property', 'App\Controller\Api\CustomerPropertyController@get');
Router::post('/api/customer_property', 'App\Controller\Api\CustomerPropertyController@post');
Router::put('/api/customer_property', 'App\Controller\Api\CustomerPropertyController@post');
Router::delete('/api/customer_property', 'App\Controller\Api\CustomerPropertyController@delete');

/**
 * ================================================================
 * RentalUnit
 * ================================================================
 */
// Rental
Router::get('/api/rental', 'App\Controller\Api\RentalController@get');
Router::post('/api/rental', 'App\Controller\Api\RentalController@post');
Router::put('/api/rental', 'App\Controller\Api\RentalController@post');
Router::delete('/api/rental', 'App\Controller\Api\RentalController@delete');
// Get all Rental module property options(Oh my god, the path definition is really bad @Sheng)
Router::get('/api/rental_properties_list', 'App\Controller\Api\RentalController@create');
// Get Rental Property Options
Router::get('/api/rental_properties_selection', 'App\Controller\Api\RentalController@createselection');

// StatusTyp
Router::get('/api/rental_status', 'App\Controller\Api\RentalStatusTypController@get');
Router::post('/api/rental_status', 'App\Controller\Api\RentalStatusTypController@post');
Router::put('/api/rental_status', 'App\Controller\Api\RentalStatusTypController@post');
Router::delete('/api/rental_status', 'App\Controller\Api\RentalStatusTypController@delete');

// Rental Properties
Router::get('/api/rental_properties', 'App\Controller\Api\RentalPropertiesController@get');
Router::post('/api/rental_properties', 'App\Controller\Api\RentalPropertiesController@post');
Router::put('/api/rental_properties', 'App\Controller\Api\RentalPropertiesController@post');
Router::delete('/api/rental_properties', 'App\Controller\Api\RentalPropertiesController@delete');

// Location
Router::get('/api/rental_location', 'App\Controller\Api\RentalLocationController@get');
Router::post('/api/rental_location', 'App\Controller\Api\RentalLocationController@post');
Router::put('/api/rental_location', 'App\Controller\Api\RentalLocationController@post');
Router::delete('/api/rental_location', 'App\Controller\Api\RentalLocationController@delete');

// Names
Router::get('/api/rental_name', 'App\Controller\Api\RentalNameController@get');
Router::post('/api/rental_name', 'App\Controller\Api\RentalNameController@post');
Router::put('/api/rental_name', 'App\Controller\Api\RentalNameController@post');
Router::delete('/api/rental_name', 'App\Controller\Api\RentalNameController@delete');

// Typ (has been abandoned)
// Router::get('/api/rental_typ', 'App\Controller\Api\RentalTypController@get');
// Router::post('/api/rental_typ', 'App\Controller\Api\RentalTypController@post');
// Router::put('/api/rental_typ', 'App\Controller\Api\RentalTypController@post');
// Router::delete('/api/rental_typ', 'App\Controller\Api\RentalTypController@delete');

// Damages
Router::get('/api/rental_damage', 'App\Controller\Api\RentalDamageController@get');
Router::post('/api/rental_damage', 'App\Controller\Api\RentalDamageController@post');
Router::put('/api/rental_damage', 'App\Controller\Api\RentalDamageController@post');
Router::delete('/api/rental_damage', 'App\Controller\Api\RentalDamageController@delete');

// Damages Properties
Router::get('/api/rental_damage_properties', 'App\Controller\Api\RentalDamagePropertiesController@get');
Router::post('/api/rental_damage_properties', 'App\Controller\Api\RentalDamagePropertiesController@post');
Router::put('/api/rental_damage_properties', 'App\Controller\Api\RentalDamagePropertiesController@post');
Router::delete('/api/rental_damage_properties', 'App\Controller\Api\RentalDamagePropertiesController@delete');

/**
 * ================================================================
 * Agreement
 * ================================================================
 */
// Agreement
Router::get('/api/agreement', 'App\Controller\Api\AgreementController@get');
Router::post('/api/agreement', 'App\Controller\Api\AgreementController@post');
Router::put('/api/agreement', 'App\Controller\Api\AgreementController@post');
Router::delete('/api/agreement', 'App\Controller\Api\AgreementController@delete');

// Termination of agreement, return of rental unit 
// 协议终止，归还租赁单元
Router::post('/api/agreement_terminated', 'App\Controller\Api\AgreementController@updateRentalunitStatusTyp');

// Get Agreement Property Options
Router::get('/api/agreement_insurence_list', 'App\Controller\Api\AgreementController@createInsurence');
Router::get('/api/agreement_properties_list', 'App\Controller\Api\AgreementController@createProperty');
Router::get('/api/agreement_extra_list', 'App\Controller\Api\AgreementController@createExtra');

// Agreement Insurence Part
Router::get('/api/agreement_insurence', 'App\Controller\Api\AgreementInsurenceController@get');
Router::post('/api/agreement_insurence', 'App\Controller\Api\AgreementInsurenceController@post');
Router::put('/api/agreement_insurence', 'App\Controller\Api\AgreementInsurenceController@post');
Router::delete('/api/agreement_insurence', 'App\Controller\Api\AgreementInsurenceController@delete');

// Agreement Properties
Router::get('/api/agreement_properties', 'App\Controller\Api\AgreementPropertiesController@get');
Router::post('/api/agreement_properties', 'App\Controller\Api\AgreementPropertiesController@post');
Router::put('/api/agreement_properties', 'App\Controller\Api\AgreementPropertiesController@post');
Router::delete('/api/agreement_properties', 'App\Controller\Api\AgreementPropertiesController@delete');

// Agreement ExtraProperties (has been abandoned)
// Router::get('/api/agreement_extra_properties', 'App\Controller\Api\AgreementExtraPropertiesController@get');
// Router::post('/api/agreement_extra_properties', 'App\Controller\Api\AgreementExtraPropertiesController@post');
// Router::put('/api/agreement_extra_properties', 'App\Controller\Api\AgreementExtraPropertiesController@post');
// Router::delete('/api/agreement_extra_properties', 'App\Controller\Api\AgreementExtraPropertiesController@delete');
