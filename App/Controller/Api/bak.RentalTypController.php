<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalTypResource;

class RentalTypController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalTypResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalTypResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalTypResource)->destroy($data);
    }
}
