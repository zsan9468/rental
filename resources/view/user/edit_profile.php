
<div class="tab-pane fade profile-edit pt-3" id="profile-edit">

    <!-- Profile Edit Form -->
    <form id="edit_user_info" class="needs-validation" novalidate>
        <div class="row mb-3">
            <label for="name" class="col-md-4 col-lg-3 col-form-label">Name</label>
            <div class="col-md-8 col-lg-9">
                <input name="name" type="text" class="form-control eidt_name" id="name" value="" required>
                <input name="userId" type="hidden" class="form-control" id="userId" value="">
                <input name="modify_type" type="hidden" class="form-control" id="userId" value="1">
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Name
                </div>
            </div>
        </div>

        <div class="row mb-3">
            <label for="surname" class="col-md-4 col-lg-3 col-form-label">Surname</label>
            <div class="col-md-8 col-lg-9">
                <input name="surname" type="text" class="form-control eidt_surname" id="surname" value="" required>
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Surname
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
            <div class="col-md-8 col-lg-9">
                <input name="email" type="text" class="form-control eidt_email" id="email" value="" required>
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Email
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="damageImgPath" class="form-label">upload photo as avatar
             </label>
            <input type="file" class="form-control" id="profile_photo_btn" name="damageImg" value="" >
            <input type="hidden" class="form-control" id="profile_photo" name="profile_photo" value="" >
        </div>
        <div class="pt-20">
            <img src=""  class="profile_photo_show" alt="">
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
    </form><!-- End Profile Edit Form -->

</div>
