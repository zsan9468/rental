<div class="tab-pane fade pt-3" id="profile-change-password">
    <!-- Change Password Form -->
    <form class="chanagePassword  needs-validation" novalidate>
        <div class="row mb-3">
            <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
            <div class="col-md-6 col-lg-6">
                <input name="password" type="password" class="form-control" id="currentPassword" required>
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Password（Minimum 6 Characters No Spaces）
                </div>
            </div>
        </div>

        <div class="row mb-3">
            <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
            <div class="col-md-6 col-lg-6">
                <input name="newpassword" type="password" class="form-control" id="newPassword" required>
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a newPassword（Minimum 6 Characters No Spaces）
                </div>
            </div>
            <div  class="col-md-1">
                <i class="bi bi-eye-slash-fill" id="chageTypePass"></i>
            </div>

        </div>

        <div class="row mb-3">
            <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
            <div class="col-md-6 col-lg-6">
                <input name="renewpassword" type="password" class="form-control" id="renewPassword" required>

                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a renewpassword
                </div>
            </div>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-primary">Change Password</button>
        </div>
    </form><!-- End Change Password Form -->
</div>
