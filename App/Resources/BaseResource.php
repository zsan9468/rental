<?php

namespace App\Resources;

class BaseResource
{
    public function baseIndex($model, $params)
    {
        $code = 200;
        if (empty($params)) {
            $start = 0;
            $length = 0;
            $search = '';
            $order = '';
            $result['data'] = $model->getIndex($start, $length, $search, $order);
        } else {

            if (isset($params['id'])) {
                $result['data'] = $model->getOne($params['id']);
            } else {
                $start = $params['start'];
                $length = $params['length'];
                $search = $params['search']['value'];
                $field = $params['columns'][$params['order'][0]['column']]['data'];
                $collation  = $params['order'][0]['dir'];
                $order = "$field $collation";
                $result['data'] = $model->getIndex($start, $length, $search, $order);

                $result['data']['draw'] = $params['draw'];
            }
        }

        $result['code'] = $code;
        return $result;
    }

    public function baseUpdate($model, $data, $id = null)
    {
        $code = 200;
        $result = $model->postItem($data, $id);
        if ($result) {
            return $code;
        } else {
            return 502;
        }
    }

    public function baseDestroy($model, $id)
    {
        if (empty($id)) {
            return 503;
        }
        $result = $model->deleteItem($id);
        if ($result) {
            return 200;
        } else {
            return 502;
        }
    }
}
