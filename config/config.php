<?php

/**
 * --------------------------------------------------------------------------------
 * This configuration mainly stores parameters related to environment configuration
 * --------------------------------------------------------------------------------
 */

// config('frontEndIndex');
return [
    /**
     * The url of the front-end home page
     * 前端首页的地址
     */
    'frontEndIndex' => 'https://rental.asc-vision.de/web/index/index',

    /**
     * The configuration here is very important, 
     * please ensure that the following three values exist in the database table statusyp, 
     * and the values of the fields are the same as the following.
     * 此处配置很重要，请确保数据库表statusyp存在以下三个值，且字段的值和下面一样。
     * 否则代码中会存在逻辑错误
     * 逻辑概述：用下面的值去表statusyp中寻找对应值的id，来定义rentalUnit的状态，
     *         所以值可以改成自己喜欢的，但key对应的意思不可以被改变
     */
    'rentalStatusTyp' => [
        1 => 'free',        // free
        2 => 'rented',      // rented
        3 => 'on repair'    // on repair
    ]
];
