<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">Agreementproperty  <span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary  lang"  key="agreement.create.agreementproperty" id="customer_properties_add">
                                            Create Agreementproperty
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="agreementproperty_properties_table" class="hover table-striped" style="width:100%">-->
                                <table id="agreementproperty_properties_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="agreementproperty_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.customerProperty">Create New
                            CustomerProperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'agreementproperty_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="agreementproperty_update_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.customerProperty">Update New
                            CustomerProperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'agreementproperty_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'propertyName', title: "propertyName", name: "propertyName" },
            { data: 'propertyDescription', title: "propertyDescription", name: "propertyDescription" },
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#agreementproperty_properties_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#agreementproperty_properties_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('#agreementproperty_update_model').modal('show');
                        console.log(row)
                        $("#agreementproperty_form_update input[name=id]").val(row.id);
                        $("#agreementproperty_form_update input[name=propertyName]").val(row.propertyName);
                        $("#agreementproperty_form_update input[name=propertyDescription]").val(row.propertyDescription);
                    })
                    $('#agreementproperty_properties_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#agreementproperty_properties_table').on('click', 'tbody #' + deleteBtn, function () {

                        $('#delcfmOverhaul').modal('show')
                        $('#del_id').text(row.id)
                        $('#del_url').text('/api/agreement_properties')
                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/agreement_properties', 'get', 'agreementproperty_properties_table', columns,);

        $('#customer_properties_add').click(function (e) {
            $('#agreementproperty_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#agreementproperty_form_create').submit(function (e) {
            var data =  $.agreementpropertiesBiaodan('agreementproperty_form_create',e)
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement_properties','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#agreementproperty_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#agreementproperty_form_update').submit(function (e) {
            var id = $("#agreementproperty_form_update input[name=id]").val();
            var data =  $.customerpropertiesBiaodan('agreementproperty_form_update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement_properties','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#agreementproperty_update_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>