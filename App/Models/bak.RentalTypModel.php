<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class RentalTypModel extends BaseModel
{
    protected $table = 'unittyp';

    public function getIndex($start, $length, $search, $order)
    {
        $whereSql = '';
        if (!$search == '') {
            $whereSql = vsprintf("typName REGEXP '%s'", array($search));
        }
        return $this->baseGetIndex($this->table, $start, $length, $search, $order, $whereSql);
    }

    public function postItem($data, $id)
    {
        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
