<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\CustomerModel;

class CustomerResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new CustomerModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data)
    {
        $rules = [
            'firstName' => ['required'],
            'lastName' => ['required'],
            'birthDate' => ['required', 'date:Y-m-d'],
            'street1' => ['required'],
            'postalCode' => ['required', 'numeric'],
            'city' => ['required'],
            'country' => ['required'],
            'email' => ['email'],
            'rateCode' => ['numeric'],
            'salesVolume' => ['numeric'],
            'telephone' => ['numeric'],
            'mobile' => ['numeric'],
        ];
        $messages = [
            'firstName.required' => 'The firstName field is required.',
            'lastName.required' => 'The lastName field is required.',
            'birthDate.required' => 'The birthDate field is required.',
            'birthDate.date' => 'The date format is incorrect, The birthDate field is in Y-m-d format.',
            'street1.required' => 'The street1 field is required.',
            'postalCode.required' => 'The postalCode field is required.',
            'postalCode.numeric' => 'The postalCode field must be a number',
            'city.required' => 'The city field is required.',
            'country.required' => 'The country field is required.',
            'email.email' => 'The email field must be a valid email address.',
            'rateCode.numeric' => 'The rateCode field must be a number.',
            'salesVolume.numeric' => 'The salesVolume field must be a number.',
            'telephone.numeric' => 'The telephone field must be a number.',
            'mobile.numeric' => 'The mobile field must be a number.',
        ];
        $validator = new \App\Lib\Validator($data['customer'], $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        dd(111);

        // properties can selectable(files, number, text, checkbox)
        $newModel = new CustomerModel;
        $resultCode = $this->baseUpdate($newModel, $data);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($id)
    {
        $newModel = new CustomerModel;
        $result = $this->baseDestroy($newModel, $id);
        ApiOutputController::ApiOutput([], $result);
    }

    public function propertiesList()
    {
        $newModel = new CustomerModel;
        $result = $newModel->getPropertiesList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }
}
