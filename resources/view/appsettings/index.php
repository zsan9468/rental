<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>
<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php';?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php';?>

<main id="main" class="main">
    <div class="pagetitle">
        <h1>AppSetInfo</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/web/index/index">Home</a></li>
                <li class="breadcrumb-item">AppSetting</li>
                <li class="breadcrumb-item active">AppSettingInfo</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section profile">
        <div class="row">
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                        <img src="" class="rounded-circle logo_imag">
                        <h2 class="overview_name"></h2>
                        <h3>AppName:<text class="AppName">  </text></h3>
                    </div>
                </div>

            </div>

            <div class="col-xl-12">

                <div class="card">
                    <div class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered">
                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                            </li>

                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit App Info</button>
                            </li>
                        </ul>
                        <div class="tab-content pt-2">
                            <?php include 'overview.php' ?>
                            <?php include 'edit_appsettings.php' ?>
                        </div><!-- End Bordered Tabs -->
                    </div>

                </div>

                <?php include 'add_appsetting.php' ?>
            </div>
        </div>
    </section>

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        $("#selectlogoPath").change(function () {
            $('.logo_imag').attr('src','');
            $.getImageBase(this, 'logo_imag', 'logoPath')
        });

        $("#faviconDataPath").change(function () {
            $('.faviconData').attr('src','');
            $.getImageBase(this, 'faviconData', 'faviconData')
        });


        $.ajaxUnit('/api/appsetting','get', {},
            function (data){
                if(data.payload.code == 200){
                    if(data.payload.data.length  != 0){
                            $(".AppName").text(data.payload.data[0].AppName)
                            $(".OnPremise").text(data.payload.data[0].OnPremise)
                            $(".IP").text(data.payload.data[0].IP)
                            $("#edit_appsettings_info input[name=AppName]").val(data.payload.data[0].AppName)
                            $("#edit_appsettings_info  input[name=OnPremise]").val(data.payload.data[0].OnPremise)
                            $("#edit_appsettings_info  input[name=id]").val(data.payload.data[0].id)
                            $("#edit_appsettings_info input[name=IP]").val(data.payload.data[0].IP)
                            if(data.payload.data[0].OnPremise == 1){
                                $('.OnPremises').prop("checked", true);
                                $('.OnPremise').prop("checked", true);
                            }
                            var logoP = '/resources/assets/img/logo.png';
                            if(data.payload.data[0].logoPath){
                                $("#edit_appsettings_info input[name=IP]").val(data.payload.data[0].IP)
                                logoP = data.payload.data[0].logoPath
                            }
                        $('.logo_imag').attr('src',logoP);
                        $('.appname').text(data.payload.data[0].AppName);
                            return true;
                    }
                    $('#appsettings_form_add').modal('show');
                }else{
                     commonUtil.message(data.payload.data,'danger');
                }
            },
            function (data)
            {
            }
        )

        $('#add_appsettings_info').submit(function (e){
            var AppName = $("#add_appsettings_info input[name=AppName]").val();
            // var OnPremise = $("#add_appsettings_info input[name=OnPremise]").val();
            // var IP = $("#add_appsettings_info input[name=IP]").val();
            var IP = $("#add_appsettings_info input[name=IP]").val();
            var result = $('.OnPremisead').is(':checked');
            if (result == true) {
                OnPremise = 1
            } else {
                OnPremise = 2
            }
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            var data = {
                'AppName': AppName,
                'OnPremise': OnPremise,
                'IP': IP,
            };
            $.ajaxUnit('/api/appsetting','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message("AppSetting successfully！")
                        window.location.reload()
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })

        $('#edit_appsettings_info').submit(function (e){
            var AppName = $("#edit_appsettings_info input[name=AppName]").val();
            var OnPremise = $("#edit_appsettings_info input[name=OnPremise]").val();
            var id = $("#edit_appsettings_info input[name=id]").val();
            var IP = $("#edit_appsettings_info input[name=IP]").val();
            var imageData = $("#edit_appsettings_info input[name=imageData]").val();
            var faviconData = $("#edit_appsettings_info input[name=faviconData]").val();

            var result = $('.OnPremise').is(':checked');
            if (result == true) {
                OnPremise = 1
            } else {
                OnPremise = 2
            }
            console.log(OnPremise)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            var data = {
                'AppName': AppName,
                'OnPremise': OnPremise,
                'id': id,
                'IP': IP,
                'imageData':imageData,
                "faviconData":faviconData
            };
            console.log(data)
            $.ajaxUnit('/api/appsetting','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message("updated successfully！");
                         $.ajaxUpdateAppinfo();
                         window.location.reload()
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })

    })

</script>
</body>

</html>