<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalStatusTypResource;

class RentalStatusTypController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalStatusTypResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalStatusTypResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalStatusTypResource)->destroy($data);
    }
}
