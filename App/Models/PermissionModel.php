<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/3/1
 * Time:  19:09
 */

namespace App\Models;

use App\Lib\DB;

class PermissionModel
{
    protected  $table = 'permission';
    protected $DB;
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }

    public function getAllPerssionList($userId = false)
    {
        if($userId == false)
        {
            return DB::link()->table($this->table)->order('menu_order asc')->get();
        }else{
//            菜单栏
            $sqlMenu = "SELECT	*  FROM	userpermiasionselection AS ups	LEFT JOIN permission AS p ON ups.permission_id = p.id
                    WHERE type = 1 and ups.user_id = $userId order by p.menu_order asc";

//            操作栏

            $sqlAction = "SELECT	*  FROM	userpermiasionselection AS ups	LEFT JOIN permission AS p ON ups.permission_id = p.id
                    WHERE type = 2 and	ups.user_id = $userId  order by p.menu_order asc";
            $dbhandle = $this->DB->query($sqlMenu);

            $result['sqlMenu'] = $dbhandle->fetchAll();
            $dbhandle = $this->DB->query($sqlAction);
            $result['sqlAction'] = $dbhandle->fetchAll();

            return  $result;
        }
    }

    public function getAllPerssionListAdmin()
    {

//            菜单栏
            $sqlMenu = "SELECT	*  FROM	 permission 
                    WHERE type = 1  order by menu_order asc";
//            操作栏
            $sqlAction = "SELECT	*  FROM	 permission
                    WHERE type = 2  order by menu_order asc";
            $dbhandle = $this->DB->query($sqlMenu);
            $result['sqlMenu'] = $dbhandle->fetchAll();
            $dbhandle = $this->DB->query($sqlAction);
            $result['sqlAction'] = $dbhandle->fetchAll();
            return  $result;

    }


    public function getSignalPermission($method,$uri)
    {
        $sqlAction = "SELECT	*  FROM	 permission
                       WHERE method = '$method' and   menu_url = '$uri' ";
        $dbhandle = $this->DB->query($sqlAction);
        $result['sqlAction'] = $dbhandle->fetchAll();
        return $result;
//          return DB::link()->table($this->table)>where("method = '$method' and menu_url = '$uri'")->get();
    }
    public function getSignal($id,$where = '')
    {
        if($where == ''){
            return DB::link()->table($this->table)->where('id='.$id)->get();
        }else{
            return DB::link()->table($this->table)->where($where)->get();
        }
    }
    public function update($id,$data)
    {
        return DB::link()->table($this->table)->where('id='.$id)->update($data);
    }

    /**
     * @param $data Array
     * @return void
     * 数据库添加记录
     */
    public function story($data)
    {
        return DB::link()->table($this->table)->insert($data);
    }

    public function delete($id,$where='')
    {
        if($where == ''){
            return DB::link()->table($this->table)->where('id='.$id)->delete();
        }else{
            return DB::link()->table($this->table)->where($where)->delete();
        }
    }
}