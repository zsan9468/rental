<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\AgreementPropertiesResource;

class AgreementPropertiesController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new AgreementPropertiesResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new AgreementPropertiesResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new AgreementPropertiesResource)->destroy($data);
    }
}
