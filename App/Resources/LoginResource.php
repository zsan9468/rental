<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date: 2023/2/23
 * Time: 20:15
 */
namespace App\Resources;

class LoginResource {


    /**
     * @param $password Password entered by the user
     * @param $salt  Hash 3  salt value
     * @return array Returns passwordSHA3 and saltSHA3 the field
     */
    public function getPassWord($password,$salt = '')
    {
        $returnArray = [];
        if(empty($salt)){
            $salt = $this->shaCode($this->randCode());
        }
        $passWordsalthash = $this->shaCode($salt.$password);
        $returnArray['passwordSHA3'] = $passWordsalthash;
        $returnArray['saltSHA3'] = $salt;
        return $returnArray;
    }

    /**
     * @param $string sha3 encrypted string to encrypt
     * @return string Returns a hash  string
     */
    protected function shaCode($string)
    {
       return hash('sha3-512',$string);
    }


    /**
     * @param $length The length of a random string
     * @return false|string Return random string
     */
    protected function randCode( $length = 16 )  
    { 
        $randCode = substr(sha1(microtime()), 0, $length); 
        return $randCode; 
    }


}