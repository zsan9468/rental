<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
    <!-- ======= Header ======= -->
    <?php include './../../layout/header.php'; ?>
    <!-- ======= Sidebar ======= -->
    <?php include './../../layout/sidebar.php'; ?>
    <main id="main" class="main">
        <section class="section dashboard">
            <div class="row">
                <!-- Left side columns -->
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Recent Sales -->
                        <div class="col-12">
                            <div class="card recent-sales overflow-auto">
                                <div class="card-body">
                                    <h5 class="card-title">COSTCENTER <span> LIST</span></h5>
                                    <div class="card">
                                        <div class="card-body">
                                            <p></p>
                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                data-bs-target="#modalDialogScrollable">
                                                Create New Costcenter
                                            </button>
                                        </div>
                                    </div>
                                    <table id="costcentertbale" class="hover table-striped" style="width:100%">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  Add General Content -->
            <div class="modal fade " id="modalDialogScrollable" tabindex="-1">
                <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Create New Costcenter</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <form class="row g-3 needs-validation" id="Costcenter_form" novalidate>
                                        <div class="col-md-8">
                                            <label for="name" class="form-label ">Branch name   </label>
                                            <input type="text" class="form-control" id="name" name="name" value=""
                                                required>
                                            <div class="valid-feedback">
                                                Costcenter name success
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide a Costcenter name
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="postalCode" class="form-label ">   PostalCode</label>
                                            <input type="number" class="form-control" id="postalCode" name="postalCode"
                                                value="" required>
                                            <div class="valid-feedback">
                                                Entered correctly
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide PostalCode
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="city" class="form-label">City   </label>
                                            <div class="input-group has-validation">
                                                <span class="input-group-text" id="inputGroupPrepend">📍</span>
                                                <input type="text" class="form-control" id="city" name="city"
                                                    aria-describedby="inputGroupPrepend" required>
                                                <div class="valid-feedback">
                                                    Entered correctly
                                                </div>
                                                <div class="invalid-feedback">
                                                    Please provide city.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="street1" class="form-label">Street1   </label>
                                            <input type="text" class="form-control" id="street1" name="street1"
                                                required>
                                            <div class="valid-feedback">
                                                Entered correctly
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide a valid Street1.
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="street2" class="form-label">Street2</label>
                                            <input type="text" class="form-control" id="validationCustom03"
                                                name="street2">
                                        </div>
                                        <div class="col-12">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button class="btn btn-primary" type="submit">Submit form</button>
                                        </div>
                                    </form><!-- End Custom Styled Validation -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Modal Dialog Scrollable-->
            <!--  Edit General Content -->
            <div class="modal fade" id="updateCostcenter" tabindex="-1">
                <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Update New Costcenter</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <form class="row g-3 was-validated" id="update_Costcenter_form" novalidate>
                                        <div class="col-md-8">
                                            <label for="name" class="form-label">Branch name  </label>
                                            <input type="text" class="form-control" id="name" name="name" value=""
                                                required>
                                            <input type="hidden" class="form-control" id="id" name="id" value=""
                                                >
                                            <div class="valid-feedback">
                                                Costcenter name success
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide a Costcenter name
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="postalCode" class="form-label">PostalCode  </label>
                                            <input type="number" class="form-control" id="postalCode" name="postalCode"
                                                value="" required>
                                            <div class="valid-feedback">
                                                Entered correctly
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide PostalCode
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="city" class="form-label">City  </label>
                                            <div class="input-group has-validation">
                                                <span class="input-group-text" id="inputGroupPrepend">📍</span>
                                                <input type="text" class="form-control" id="city" name="city"
                                                    aria-describedby="inputGroupPrepend" required>
                                                <div class="valid-feedback">
                                                    Entered correctly
                                                </div>
                                                <div class="invalid-feedback">
                                                    Please provide city.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="street1" class="form-label">Street1  </label>
                                            <input type="text" class="form-control" id="street1" name="street1"
                                                required>
                                            <div class="valid-feedback">
                                                Entered correctly
                                            </div>
                                            <div class="invalid-feedback">
                                                Please provide a valid Street1.
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="street2" class="form-label">Street2</label>
                                            <input type="text" class="form-control" id="validationCustom03"
                                                name="street2" >

                                        </div>
                                        <div class="col-12">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button class="btn btn-primary" type="submit">Confirm modification？</button>
                                        </div>
                                    </form><!-- End Custom Styled Validation -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Modal Dialog Scrollable-->

        </section>

    </main>
    <!-- ======= Footer ======= -->
    <?php include './../../layout/footer.html'; ?>
    <!-- End Footer -->
    <!-- Vendor JS Files -->
    <?php include './../../layout/js.html' ?>
    <script>
        $(document).ready(function () {
            // 获取列表
            var columns = [
                { data: 'id', title: "ID", name: "id" },
                { data: 'name', title: "Name", name: "greeting" },
                { data: 'city', title: "City", name: "city" },
                { data: 'postalCode', title: "PostalCode", name: "postalCode" },
                { data: 'street1', title: "street1", name: "street1" },
                { data: 'street2', title: "Street2", name: "street2" },
                {data: 'Action', title: "Operation", bSortable: false, render: (data, type, row) => {

                        var upodateBtn = "upodate" + row.id;
                        var deleteBtn = "delete" + row.id;
                        $('#costcentertbale').undelegate('tbody #' + upodateBtn, 'click');

                        $('#costcentertbale').on('click', 'tbody #' + upodateBtn, function (e) {
                            $('#updateCostcenter').modal('show');
                            $("#updateCostcenter input[name='name']").val(row.name)
                            $("#updateCostcenter input[name='city']").val(row.city)
                            $("#updateCostcenter input[name='postalCode']").val(row.postalCode)
                            $("#updateCostcenter input[name='street1']").val(row.street1)
                            $("#updateCostcenter input[name='street2']").val(row.street2)
                            $("#updateCostcenter input[name='id']").val(row.id)
                        })
                        $('#costcentertbale').undelegate('tbody #' + deleteBtn, 'click');
                        $('#costcentertbale').on('click', 'tbody #' + deleteBtn, function () {

                            $('#delcfmOverhaul').modal('show');
                            $('#del_id').text(row.id);
                            $('#del_url').text('/api/cost_centor_action');
                        })

                        return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button>   <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";

                    }
                },
            ];
            tableAjax('/api/get_cost_centor_action', 'get', 'costcentertbale', columns,);
// Create add costcerter
            $('#Costcenter_form').submit(function (e) {
                let value = $('#Costcenter_form').serializeArray();
                $.checkCostCenter(value,e,'create')
                if (e.isDefaultPrevented()) {
                    return;
                }
                e.preventDefault()
                var responses = formAjaxHy('/api/cost_centor_action', value, 'post');
                console.log(responses);
                if (responses.payload.code == 200) {
                     commonUtil.message('Create Success！')
                    table.ajax.reload();
                    $('#modalDialogScrollable').modal('hide');
                } else {
                     commonUtil.message('Add fail！')
                }
                return false;//Block page refresh
            })
// update costcerter record
            $('#update_Costcenter_form').submit(function (e) {
                let updatevalue = $('#update_Costcenter_form').serializeArray();
                $.checkCostCenter(updatevalue,e,'update')
                if (e.isDefaultPrevented()) {
                    return;
                }
                e.preventDefault()
                var responses = formAjaxHy('/api/cost_centor_action', updatevalue, 'put');

                if (responses.payload.code == 200) {
                     commonUtil.message('Update Success！')
                    table.ajax.reload();
                    $('#updateCostcenter').modal('hide');
                } else {
                     commonUtil.message(responses.payload.data)
                }
            })
            // $('#example tbody').on('click', 'tr td:nth-child(3)', function (e) {
            //     var name = $(this).text();
            //      commonUtil.message(name);
            // });

        });
    </script>
</body>

</html>