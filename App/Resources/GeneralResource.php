<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\GeneralModel;

class GeneralResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new GeneralModel;
        $result = $newModel->getIndex();
        ApiOutputController::ApiOutput($result, 200);
    }

    public function update($data, $id)
    {
        $rules = [
            'VATrate' => ['numeric'],
            'date_format' => ['numeric', 'max:2'],

        ];
        $messages = [
            'VATrate.numeric' => 'The VATrate field must be a number',
            'date_format.numeric' => 'The date_format field must be a number',
            'date_format.max' => 'The date_format field can only be up to 2 characters.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $result = $validator->validate();
        if ($result === true) {
            $newModel = new GeneralModel;
            $resultCode = $this->baseUpdate($newModel, $data, $id);
        } else {
            $resultCode = 412;
        }
        ApiOutputController::ApiOutput($result, $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new GeneralModel;
            $result = $this->baseDestroy($newModel, $id);
        } else {
            $result = 503;
        }
        ApiOutputController::ApiOutput([], $result);
    }
}
