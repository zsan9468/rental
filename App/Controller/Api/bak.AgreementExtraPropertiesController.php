<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\AgreementExtraPropertiesResource;

class AgreementExtraPropertiesController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new AgreementExtraPropertiesResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new AgreementExtraPropertiesResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new AgreementExtraPropertiesResource)->destroy($data);
    }
}
