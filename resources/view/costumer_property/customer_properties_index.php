<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">CustomerProperties  <span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary  lang"  key="rentalunits.create.customerProperty" id="customer_properties_add">
                                            Create CustomerProperty
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="customer_properties_table" class="hover table-striped" style="width:100%">-->
                                <table id="customer_properties_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="customer_properties_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.customerProperty">Create New
                            CustomerProperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'customer_properties_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="update_customer_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.customerProperty">Update New
                            CustomerProperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'customer_properties_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'propertyName', title: "propertyName", name: "propertyName" },
            { data: 'propertyDescription', title: "propertyDescription", name: "propertyDescription" },
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#customer_properties_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#customer_properties_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('#update_customer_add_model').modal('show');
                        $(".UpdatePropertySTyle .row").remove()
                        var responses = formAjaxHy('/api/customer_property?id='+row.id, data, 'get');
                        console.log(1)
                        console.log(responses)
                        $("#customer_properties_form_update input[name=id]").val(row.id);
                        $("#customer_properties_form_update input[name=propertyName]").val(row.propertyName);
                        $("#customer_properties_form_update input[name=propertyDescription]").val(row.propertyDescription);
                    })

                    $('#customer_properties_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#customer_properties_table').on('click', 'tbody #' + deleteBtn, function () {

                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/customer_property');

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/customer_property', 'get', 'customer_properties_table', columns,);

        $('#customer_properties_add').click(function (e) {
            $('#customer_properties_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#ccustomer_properties_form_create').submit(function (e) {
            var data =  $.customerpropertiesBiaodan('ccustomer_properties_form_create',e,'create')
            // console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/customer_property','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#customer_properties_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#customer_properties_form_update').submit(function (e) {
            var id = $("#customer_properties_form_update input[name=id]").val();
            var data =  $.customerpropertiesBiaodan('customer_properties_form_update',e,'update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/customer_property','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#update_customer_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>