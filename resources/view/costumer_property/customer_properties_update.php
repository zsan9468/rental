 <form class="row g-3 was-validated" id="customer_properties_form_update" novalidate>

        <div class="col-md-4">
            <label for="firstName" class="form-label">propertyName</label>
            <input type="text" class="form-control" id="propertyName" name="propertyName" value="" required>
            <input type="hidden" class="form-control" id="id" name="id" value="" required>
            <div class="valid-feedback">
                propertyName Data Entry Ok !
            </div>
            <div class="invalid-feedback">
                propertyName provide a firstName
            </div>
        </div>

        <div class="col-md-4">
            <label for="lastName" class="form-label">propertyDescription</label>
            <input type="text" class="form-control" id="propertyDescription" name="propertyDescription" value="" >
        </div>



    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
