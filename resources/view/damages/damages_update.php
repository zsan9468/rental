<form class="row g-3 was-validated" id="Damage_form_update" novalidate>

    <div class="row col-md-10" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label">damageProperties</label>
        <div class="col-sm-8">
            <select id="damageProperties" name="damageProperties" class="form-select">
            </select>
        </div>
    </div>

    <div class="col-md-6">
        <label for="repairDateTime" class="form-label">repairDateTime</label>
        <input type="date" class="form-control" id="repairDateTime" name="repairDateTime" value="" >
        <input type="hidden" class="form-control" id="rentalUnitsId" name="rentalUnitsId" value="" >
        <input type="hidden" class="form-control" id="id" name="id" value="" >

    </div>

    <div class="col-md-6">
        <label for="repairDateTime" class="form-label">damageTime</label>
        <input type="date" class="form-control" id="damageTime" name="damageTime" value="" required>

    </div>
<!--    <div class="col-md-6">-->
<!--        <label for="repairDateTime" class="form-label">damageTime</label>-->
<!--        <input type="date" id="date" class="date form_datetime  form-control">-->
<!---->
<!---->
<!--    </div>-->



    <div class="col-md-6">
        <label for="repairDateTime" class="form-label lang" key="damage.choose.status">damageStatus</label>
        <select id="damageStatus" name="damageStatus" class="form-select" >
            <option value="1" class="lang" key="damage.maintenance">in maintenance</option>
            <option value="2" class="lang" key="damage.already.repaired">already repaired</option>
        </select>
    </div>

    <div class="col-md-6">
        <label for="damageImgPath" class="form-label">damageImg
            <span id="drawing" style="text-decoration: underline;font-size: 0.5rem">Online drawing</span>
            <a href="/public/img/blueprints.jpg" style="text-decoration: underline;font-size: 0.5rem" download="blueprints.jpg">Download Empty Blueprint </a></label>
        <input type="file" class="form-control" id="damageImg" name="damageImg" value="" >
        <input type="hidden" class="form-control" id="damageImgPath" name="damageImgPath" value="" >

    </div>
    <div class="col-md-6 damages_img">
        <img src=''  class='damage_imag'>
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
