/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : test_database

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 15/03/2023 18:06:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for appsettings
-- ----------------------------
DROP TABLE IF EXISTS `appsettings`;
CREATE TABLE `appsettings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OnPremise` tinyint(4) NOT NULL,
  `IP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `AppName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logoPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of appsettings
-- ----------------------------
INSERT INTO `appsettings` VALUES (8, 1, '192.168.1.0', 'Rental', '/public/upload/img/damage/20230315/1678871802.jpeg');

-- ----------------------------
-- Table structure for costcenter
-- ----------------------------
DROP TABLE IF EXISTS `costcenter`;
CREATE TABLE `costcenter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `street1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `street2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postalCode` int(11) NOT NULL,
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of costcenter
-- ----------------------------
INSERT INTO `costcenter` VALUES (24, 'BaoshanSongbin', '2143214', '', 200100, 'Shangdong');
INSERT INTO `costcenter` VALUES (25, 'Pudong', 'PudongJinkeyaun no.151', 'ZhangjiangKejiyuan', 200112, 'ShanghaiShi');

-- ----------------------------
-- Table structure for costcenterselection
-- ----------------------------
DROP TABLE IF EXISTS `costcenterselection`;
CREATE TABLE `costcenterselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `costCenterId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_costCenterSelection_costCenter1_idx`(`costCenterId`) USING BTREE,
  INDEX `fk_costCenterSelection_user1_idx`(`userId`) USING BTREE,
  CONSTRAINT `fk_costCenterSelection_costCenter1` FOREIGN KEY (`costCenterId`) REFERENCES `costcenter` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_costCenterSelection_user1` FOREIGN KEY (`userId`) REFERENCES `user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of costcenterselection
-- ----------------------------
INSERT INTO `costcenterselection` VALUES (34, 24, 40);

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `birthDate` date NOT NULL,
  `companyName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `street1` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `street2` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postalCode` int(11) NOT NULL,
  `city` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `country` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rateCode` int(11) NULL DEFAULT NULL,
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `salesVolume` int(11) NULL DEFAULT NULL,
  `createdDateTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updatedDateTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (23, 'Mengjiao', 'Li', '2023-03-16', 'Diyou', 'Guojia Garden Business Center - Block B', 'Building B, Guojia Garden Business Center, No. 485 Yangnan Road, Pudong New Area, Shanghai', 214214, 'Shanghai', 'china', '2151793666', '21', 'WQBZH@163.com', 23, '', 32, '2023-03-06 15:12:59', '2023-03-12 21:48:52');
INSERT INTO `customer` VALUES (32, 'Haiyang', 'Wang', '1996-02-19', 'SHanghaishenguan', ' Room 1-59, Lane 123, Meimei Road, Pudong New Area, Shanghai', 'Intersection of Shenmei Road and Shenyu Road, Pudong New Area, Shanghai', 200102, 'Shanghai', 'China', '13212169551', '5537158', 'wqzbxh@163.com', 60, 'The first serious user, the program maker', 90, '2023-03-12 21:43:03', '2023-03-12 21:43:03');
INSERT INTO `customer` VALUES (41, 'Lucy', 'Wang', '2023-03-15', 'China', 'Mai Hing Industrial Building 16-18 Hing Yip Street Kwun Tong', 'Mix Technology Ltd.', 200100, 'Hong Kong', 'China', NULL, NULL, 'admin2@163.com', 50, '', 12, '2023-03-15 17:08:55', '2023-03-15 17:08:55');

-- ----------------------------
-- Table structure for customerproperties
-- ----------------------------
DROP TABLE IF EXISTS `customerproperties`;
CREATE TABLE `customerproperties`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propertyName` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `propertyDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 85 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customerproperties
-- ----------------------------
INSERT INTO `customerproperties` VALUES (85, 'driving licence', 'driving licence');
INSERT INTO `customerproperties` VALUES (86, 'teacher\'s license', 'teacher\'s license');

-- ----------------------------
-- Table structure for customerpropertyselection
-- ----------------------------
DROP TABLE IF EXISTS `customerpropertyselection`;
CREATE TABLE `customerpropertyselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `costumerPropertyName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerPropertyValue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerPropertiesId` int(11) NOT NULL,
  `costumerId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_costumerPropertySelection_customerProperties1_idx`(`customerPropertiesId`) USING BTREE,
  INDEX `fk_costumerPropertySelection_costumer1_idx`(`costumerId`) USING BTREE,
  CONSTRAINT `fk_costumerPropertySelection_costumer1` FOREIGN KEY (`costumerId`) REFERENCES `customer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_costumerPropertySelection_customerProperties1` FOREIGN KEY (`customerPropertiesId`) REFERENCES `customerproperties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 89 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customerpropertyselection
-- ----------------------------
INSERT INTO `customerpropertyselection` VALUES (89, 'driving licence', 'YES', 85, 41);
INSERT INTO `customerpropertyselection` VALUES (90, 'teacher', 'YES', 86, 41);

-- ----------------------------
-- Table structure for damageproperties
-- ----------------------------
DROP TABLE IF EXISTS `damageproperties`;
CREATE TABLE `damageproperties`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `damageName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `damageDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of damageproperties
-- ----------------------------
INSERT INTO `damageproperties` VALUES (4, 'engine', 'Vehicle engine');
INSERT INTO `damageproperties` VALUES (5, 'callipers', 'The utility model relates to a pincer measuring tool with two tensioned legs');
INSERT INTO `damageproperties` VALUES (6, 'wiper', 'Windshield wipers for vehicles');
INSERT INTO `damageproperties` VALUES (7, 'steering wheel', '');
INSERT INTO `damageproperties` VALUES (8, 'brake block', '');
INSERT INTO `damageproperties` VALUES (9, 'headlamp', '');

-- ----------------------------
-- Table structure for damages
-- ----------------------------
DROP TABLE IF EXISTS `damages`;
CREATE TABLE `damages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `damagePropertiesId` int(11) NOT NULL,
  `rentalUnitsId` int(11) NOT NULL,
  `repairDateTime` date NULL DEFAULT NULL,
  `damageTime` date NULL DEFAULT NULL,
  `damageStatus` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `damageImgPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_damages_damageProperties1_idx`(`damagePropertiesId`) USING BTREE,
  INDEX `fk_damages_rentalUnits1_idx`(`rentalUnitsId`) USING BTREE,
  CONSTRAINT `fk_damages_damageProperties1` FOREIGN KEY (`damagePropertiesId`) REFERENCES `damageproperties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_damages_rentalUnits1` FOREIGN KEY (`rentalUnitsId`) REFERENCES `rentalunits` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of damages
-- ----------------------------
INSERT INTO `damages` VALUES (30, 8, 15, '2023-03-15', '2023-03-09', '1', '/public/upload/img/damage/20230315/1678874412.png');

-- ----------------------------
-- Table structure for generalsettings
-- ----------------------------
DROP TABLE IF EXISTS `generalsettings`;
CREATE TABLE `generalsettings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `greeting` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VATrate` int(11) NULL DEFAULT NULL,
  `invoiceText1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoiceText2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoiceText3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paymentWarning1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paymentWarning2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paymentWarning3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `offer1` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `offer2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `offer3` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_format` smallint(2) NULL DEFAULT 1,
  `language_forbid` smallint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of generalsettings
-- ----------------------------
INSERT INTO `generalsettings` VALUES (72, 'Name', 1260, '', '', '', '', NULL, '', '', '', '', 2, 2);

-- ----------------------------
-- Table structure for insurence
-- ----------------------------
DROP TABLE IF EXISTS `insurence`;
CREATE TABLE `insurence`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insurenceType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `insurenceCost` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of insurence
-- ----------------------------
INSERT INTO `insurence` VALUES (7, 'life insurance', 1280);
INSERT INTO `insurence` VALUES (11, 'Ping An Insurance', 1200);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'The menu id increases automatically',
  `menu_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT ' Add new ',
  `menu_icon` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Menu icon',
  `menu_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Menu url',
  `method` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'access mode',
  `menu_order` int(3) NULL DEFAULT 0 COMMENT 'nstructional sequence menu',
  `father_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'Parent menu id',
  `type` smallint(2) NULL DEFAULT NULL COMMENT 'Menu Type (1 Left menu)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, 'AppSetting', 'AppSetting', '', NULL, 100, '0', 1);
INSERT INTO `permission` VALUES (2, 'General Settings', 'General-Settings', '', NULL, 99, '0', 1);
INSERT INTO `permission` VALUES (3, 'Customer', 'Customer', NULL, NULL, 2, '0', 1);
INSERT INTO `permission` VALUES (4, 'CostCenter', 'CostCenter-nav', 'costCenter/costCenter_index', NULL, 3, '6', 1);
INSERT INTO `permission` VALUES (5, 'Employee List', NULL, 'user/user_index', NULL, 2, '6', 1);
INSERT INTO `permission` VALUES (6, 'Employee User', 'Emptyee-User', '', NULL, 4, '0', 1);
INSERT INTO `permission` VALUES (7, 'costCenter List', NULL, 'costCenter/costCenter_index', NULL, 0, '4', 1);
INSERT INTO `permission` VALUES (8, 'create_User', '', '/api/user_action', 'POST', 1, '6', 2);
INSERT INTO `permission` VALUES (9, 'delete_User', '', '/api/user_action', 'DELETE', 2, '6', 2);
INSERT INTO `permission` VALUES (10, 'create_costCerter', '', '/api/cost_centor_action', 'POST', 1, '4', 2);
INSERT INTO `permission` VALUES (11, 'update_costCerter', NULL, '/api/cost_centor_action', 'PUT', 2, '4', 2);
INSERT INTO `permission` VALUES (12, 'delete_costCerter', NULL, '/api/cost_centor_action', 'DELETE', 3, '4', 2);
INSERT INTO `permission` VALUES (13, 'General Set', '', 'general_setting/general_index', '', 0, '2', 1);
INSERT INTO `permission` VALUES (14, 'AppSetInfo', NULL, 'appsettings/index', '', 0, '1', 1);
INSERT INTO `permission` VALUES (15, 'add_appsetting', NULL, '/api/appsetting', 'POST', 1, '1', 2);
INSERT INTO `permission` VALUES (16, 'update_appsetting', NULL, '/api/appsetting', 'PUT', 2, '1', 2);
INSERT INTO `permission` VALUES (17, 'Rental Units', 'RentalUnits', '', '', 1, '0', 1);
INSERT INTO `permission` VALUES (18, 'Unit Location List', NULL, 'unitlocation/unitlocation_index', NULL, 4, '17', 1);
INSERT INTO `permission` VALUES (19, 'Permission Set', NULL, 'permission/index', NULL, 0, '1', 1);
INSERT INTO `permission` VALUES (20, 'CustomerProperties List', NULL, 'costumer_property/customer_properties_index', NULL, 0, '3', 1);
INSERT INTO `permission` VALUES (23, 'update_general', '', '/api/general', 'PUT', 2, '2', 2);
INSERT INTO `permission` VALUES (24, 'delete_general', NULL, '/api/general', 'DELETE', 3, '2', 2);
INSERT INTO `permission` VALUES (25, 'create_general', NULL, '/api/general', 'POST', 1, '2', 2);
INSERT INTO `permission` VALUES (28, 'Rental Agreement', 'Rental-Agreement', NULL, NULL, 4, '0', 1);
INSERT INTO `permission` VALUES (31, 'update_User', NULL, '/api/user_action', 'PUT', 3, '6', 2);
INSERT INTO `permission` VALUES (32, 'Customer List', NULL, 'customer/customer_index', '', 0, '3', 1);
INSERT INTO `permission` VALUES (33, 'create_customer', '', '/api/customer', 'POST', 1, '3', 2);
INSERT INTO `permission` VALUES (34, 'update_customer', NULL, '/api/customer', 'PUT', 2, '3', 2);
INSERT INTO `permission` VALUES (35, 'delete_customer', NULL, '/api/customer', 'DELETE', 3, '3', 2);
INSERT INTO `permission` VALUES (36, 'create_customerpropertie', '', '/api/customer_property', 'POST', 1, '3', 2);
INSERT INTO `permission` VALUES (37, 'update_customerpropertie', NULL, '/api/customer_property', 'PUT', 2, '3', 2);
INSERT INTO `permission` VALUES (38, 'delete_customerpropertie', NULL, '/api/customer_property', 'DELETE', 3, '3', 2);
INSERT INTO `permission` VALUES (39, 'Insurence List', NULL, 'insurence/insurence_index', NULL, 0, '28', 1);
INSERT INTO `permission` VALUES (40, 'Agreement List', NULL, 'agreement/agreement_index', NULL, 0, '28', 1);
INSERT INTO `permission` VALUES (44, 'StatusTyp', NULL, 'statustyp/statustyp_index', NULL, 8, '17', 1);
INSERT INTO `permission` VALUES (45, 'Unitnames', '', 'unitnames/unitnames_index', '', 2, '17', 1);
INSERT INTO `permission` VALUES (46, 'Rental Properties', NULL, 'rentalproperties/rentalproperties_index', NULL, 2, '17', 1);
INSERT INTO `permission` VALUES (47, 'Damages List', NULL, 'damages/damages_index', NULL, 3, '17', 1);
INSERT INTO `permission` VALUES (49, 'rentalUnit_add', '', '/api/rental', 'POST', 3, '17', 2);
INSERT INTO `permission` VALUES (50, 'rentalUnit_delete', '', '/api/rental', 'DELETE', 3, '17', 2);
INSERT INTO `permission` VALUES (51, 'rentalUnit_update', '', '/api/rental', 'PUT', 3, '17', 2);
INSERT INTO `permission` VALUES (52, 'Rental List', '', 'rental/rental_index', '', 1, '17', 1);
INSERT INTO `permission` VALUES (53, 'Damages Properties', '', 'damagesproperties/damagesproperties_index', '', 3, '17', 1);
INSERT INTO `permission` VALUES (55, 'AgreementProperty List', '', 'agreementproperty/agreementproperty_index', '', 2, '28', 1);

-- ----------------------------
-- Table structure for rentalagreement
-- ----------------------------
DROP TABLE IF EXISTS `rentalagreement`;
CREATE TABLE `rentalagreement`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdDateTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updatedDateTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `cost` int(11) NULL DEFAULT NULL,
  `rentalBeginDateTime` datetime(0) NULL DEFAULT NULL,
  `rentalEndDateTime` datetime(0) NULL DEFAULT NULL,
  `insurenceId` int(11) NOT NULL,
  `costumerId` int(11) NOT NULL,
  `rentalUnitsId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_rentalAgreement_insurence1_idx`(`insurenceId`) USING BTREE,
  INDEX `fk_rentalAgreement_costumer1_idx`(`costumerId`) USING BTREE,
  INDEX `fk_rentalAgreement_rentalUnits1_idx`(`rentalUnitsId`) USING BTREE,
  CONSTRAINT `fk_rentalAgreement_costumer1` FOREIGN KEY (`costumerId`) REFERENCES `customer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalAgreement_insurence1` FOREIGN KEY (`insurenceId`) REFERENCES `insurence` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalAgreement_rentalUnits1` FOREIGN KEY (`rentalUnitsId`) REFERENCES `rentalunits` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalagreement
-- ----------------------------
INSERT INTO `rentalagreement` VALUES (11, '2023-03-15 17:46:34', '2023-03-15 17:47:46', 980, '2023-03-15 17:46:00', '2023-04-15 17:46:00', 11, 23, 15);

-- ----------------------------
-- Table structure for rentalagreementproperties
-- ----------------------------
DROP TABLE IF EXISTS `rentalagreementproperties`;
CREATE TABLE `rentalagreementproperties`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propertyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `propertyDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalagreementproperties
-- ----------------------------
INSERT INTO `rentalagreementproperties` VALUES (17, 'Fuel filling', 'Fuel filling');
INSERT INTO `rentalagreementproperties` VALUES (18, 'vehicle condition', 'keep a good…');

-- ----------------------------
-- Table structure for rentalagreementpropertyselection
-- ----------------------------
DROP TABLE IF EXISTS `rentalagreementpropertyselection`;
CREATE TABLE `rentalagreementpropertyselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rentalAgreementPropertyName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rentalAgreementPropertyValue` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rentalAgreementPropertiesId` int(11) NOT NULL,
  `rentalAgreementId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_rentalAgreementPropertySelection_rentalAgreementProperti_idx`(`rentalAgreementPropertiesId`) USING BTREE,
  INDEX `fk_rentalAgreementPropertySelection_rentalAgreement1_idx`(`rentalAgreementId`) USING BTREE,
  CONSTRAINT `fk_rentalAgreementPropertySelection_rentalAgreement1` FOREIGN KEY (`rentalAgreementId`) REFERENCES `rentalagreement` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalAgreementPropertySelection_rentalAgreementProperties1` FOREIGN KEY (`rentalAgreementPropertiesId`) REFERENCES `rentalagreementproperties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalagreementpropertyselection
-- ----------------------------
INSERT INTO `rentalagreementpropertyselection` VALUES (3, 'Fuel filling', 'YES', 17, 11);
INSERT INTO `rentalagreementpropertyselection` VALUES (4, 'vehicle condition', 'YES', 18, 11);

-- ----------------------------
-- Table structure for rentalproperties
-- ----------------------------
DROP TABLE IF EXISTS `rentalproperties`;
CREATE TABLE `rentalproperties`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propertieName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `propertyDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalproperties
-- ----------------------------
INSERT INTO `rentalproperties` VALUES (1, 'Body seating', 'Body seating');
INSERT INTO `rentalproperties` VALUES (3, 'gas displacement', 'gas displacement');
INSERT INTO `rentalproperties` VALUES (4, 'speed control system', 'speed control system');

-- ----------------------------
-- Table structure for rentalpropertiesselection
-- ----------------------------
DROP TABLE IF EXISTS `rentalpropertiesselection`;
CREATE TABLE `rentalpropertiesselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rentalPropertiesValue` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rentalPropertiesId` int(11) NOT NULL,
  `rentalUnitsId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_rentalPropertiesSelection_rentalProperties1_idx`(`rentalPropertiesId`) USING BTREE,
  INDEX `fk_rentalPropertiesSelection_rentalUnits1_idx`(`rentalUnitsId`) USING BTREE,
  CONSTRAINT `fk_rentalPropertiesSelection_rentalProperties1` FOREIGN KEY (`rentalPropertiesId`) REFERENCES `rentalproperties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalPropertiesSelection_rentalUnits1` FOREIGN KEY (`rentalUnitsId`) REFERENCES `rentalunits` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalpropertiesselection
-- ----------------------------
INSERT INTO `rentalpropertiesselection` VALUES (24, 'Seats 4 for', 1, 15);
INSERT INTO `rentalpropertiesselection` VALUES (25, '998cc', 3, 15);
INSERT INTO `rentalpropertiesselection` VALUES (26, '8 speed han', 4, 15);
INSERT INTO `rentalpropertiesselection` VALUES (27, 'Four doors ', 1, 16);
INSERT INTO `rentalpropertiesselection` VALUES (28, ' 544hp/76.0', 3, 16);

-- ----------------------------
-- Table structure for rentalunits
-- ----------------------------
DROP TABLE IF EXISTS `rentalunits`;
CREATE TABLE `rentalunits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unitTypId` int(11) NOT NULL,
  `unitNamesId` int(11) NOT NULL,
  `unitLocationPropertiesId` int(11) NOT NULL,
  `statusTypId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_rentalUnits_unitTyp1_idx`(`unitTypId`) USING BTREE,
  INDEX `fk_rentalUnits_unitNames1_idx`(`unitNamesId`) USING BTREE,
  INDEX `fk_rentalUnits_unitLocationProperties1_idx`(`unitLocationPropertiesId`) USING BTREE,
  INDEX `fk_rentalUnits_statusTyp1_idx`(`statusTypId`) USING BTREE,
  CONSTRAINT `fk_rentalUnits_statusTyp1` FOREIGN KEY (`statusTypId`) REFERENCES `statustyp` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalUnits_unitLocationProperties1` FOREIGN KEY (`unitLocationPropertiesId`) REFERENCES `unitlocation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalUnits_unitNames1` FOREIGN KEY (`unitNamesId`) REFERENCES `unitnames` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_rentalUnits_unitTyp1` FOREIGN KEY (`unitTypId`) REFERENCES `unittyp` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rentalunits
-- ----------------------------
INSERT INTO `rentalunits` VALUES (15, 25, 10, 3, 8);
INSERT INTO `rentalunits` VALUES (16, 26, 1, 1, 1);

-- ----------------------------
-- Table structure for statustyp
-- ----------------------------
DROP TABLE IF EXISTS `statustyp`;
CREATE TABLE `statustyp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of statustyp
-- ----------------------------
INSERT INTO `statustyp` VALUES (1, 'free', 'free');
INSERT INTO `statustyp` VALUES (2, 'rented', 'The state of lending');
INSERT INTO `statustyp` VALUES (8, 'on repair', 'on repair');

-- ----------------------------
-- Table structure for unitextrasproperty
-- ----------------------------
DROP TABLE IF EXISTS `unitextrasproperty`;
CREATE TABLE `unitextrasproperty`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extraName` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `extraPrice` float NOT NULL,
  `extraDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unitextrasproperty
-- ----------------------------
INSERT INTO `unitextrasproperty` VALUES (65, 'fuel charge', 20, 'filled up the car with gas');

-- ----------------------------
-- Table structure for unitextraspropertyselection
-- ----------------------------
DROP TABLE IF EXISTS `unitextraspropertyselection`;
CREATE TABLE `unitextraspropertyselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unitExtrasId` int(11) NOT NULL,
  `rentalAgreementId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_unitExtrasSelection_unitExtras1_idx`(`unitExtrasId`) USING BTREE,
  INDEX `fk_unitExtrasSelection_rentalAgreement1_idx`(`rentalAgreementId`) USING BTREE,
  CONSTRAINT `fk_unitExtrasSelection_rentalAgreement1` FOREIGN KEY (`rentalAgreementId`) REFERENCES `rentalagreement` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_unitExtrasSelection_unitExtras1` FOREIGN KEY (`unitExtrasId`) REFERENCES `unitextrasproperty` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unitextraspropertyselection
-- ----------------------------
INSERT INTO `unitextraspropertyselection` VALUES (65, 65, 11);

-- ----------------------------
-- Table structure for unitlocation
-- ----------------------------
DROP TABLE IF EXISTS `unitlocation`;
CREATE TABLE `unitlocation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unitlocation
-- ----------------------------
INSERT INTO `unitlocation` VALUES (1, 'BerlinLausitzer Str. 57', 'Lausitzer Str. 57', '0176 5885529', '');
INSERT INTO `unitlocation` VALUES (3, 'ShangHai', 'Shanghai Pudong JinkeLu 151', '021 82456', 'ShanghaiINgShanghaiINg');

-- ----------------------------
-- Table structure for unitnames
-- ----------------------------
DROP TABLE IF EXISTS `unitnames`;
CREATE TABLE `unitnames`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `manufacture` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unitnames
-- ----------------------------
INSERT INTO `unitnames` VALUES (1, 'BMW 740I', 'BMW');
INSERT INTO `unitnames` VALUES (10, 'Audi A1 Sportback', 'Audi');
INSERT INTO `unitnames` VALUES (11, 'BMW 2-Series', 'BMW');

-- ----------------------------
-- Table structure for unitprice
-- ----------------------------
DROP TABLE IF EXISTS `unitprice`;
CREATE TABLE `unitprice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float NOT NULL,
  `priceGroup` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unitTypId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_unitPrice_unitTyp1_idx`(`unitTypId`) USING BTREE,
  CONSTRAINT `fk_unitPrice_unitTyp1` FOREIGN KEY (`unitTypId`) REFERENCES `unittyp` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unitprice
-- ----------------------------
INSERT INTO `unitprice` VALUES (13, 20300, 'eXtreme2 Power', 25);
INSERT INTO `unitprice` VALUES (14, 6550, 'Luxury', 26);

-- ----------------------------
-- Table structure for unittyp
-- ----------------------------
DROP TABLE IF EXISTS `unittyp`;
CREATE TABLE `unittyp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unittyp
-- ----------------------------
INSERT INTO `unittyp` VALUES (25, 'Car', '');
INSERT INTO `unittyp` VALUES (26, 'Car', '');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `passwordSHA3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `saltSHA3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `userId_2`(`userId`) USING BTREE,
  INDEX `userId_3`(`userId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin_boss', 'a07edbaadfe48bf8c60ab69e421d47e21b14a070b7108b5fbc0af32057a406fdeada94342825c4e9a54c66638d894885e0035b96c2a9fc409b64727bf208bbef', '547c79ff82a90b8b0029c7c21cd07dc2ae3ca7ec9d9a0de50430e1d3d24da6fab904cc81d7b8fb624acb2b950126c2256da508a1ecbb96b64199404d9aa7f990', 'Super ', 'Super Admin', 'Super_Admin@asc.com', 1);
INSERT INTO `user` VALUES (40, 'Markus666', 'df50c02285bcb8c06705d263d0a4102a77915ba6036da883a4dce1347da5f5c7bd0b312e14e67d24c79386efca25ac1e68de6684b5aeb0c362f77f00fb8ee2c0', '576dd4741e36a552b7484ad143e2efc1b3f5f3c4cff82b7c3347490d6e5d62c7fe0b82e76c7bad23f7378d7a6c60dc2b2c2574e8029992a53283b7ee589ef8fe', 'Markus Access', 'love markus', 'wqzbxh@163.com', 3);

-- ----------------------------
-- Table structure for userpermiasionselection
-- ----------------------------
DROP TABLE IF EXISTS `userpermiasionselection`;
CREATE TABLE `userpermiasionselection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1237 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userpermiasionselection
-- ----------------------------
INSERT INTO `userpermiasionselection` VALUES (1237, 3, 40);
INSERT INTO `userpermiasionselection` VALUES (1238, 32, 40);
INSERT INTO `userpermiasionselection` VALUES (1239, 20, 40);
INSERT INTO `userpermiasionselection` VALUES (1240, 36, 40);
INSERT INTO `userpermiasionselection` VALUES (1241, 33, 40);
INSERT INTO `userpermiasionselection` VALUES (1242, 34, 40);
INSERT INTO `userpermiasionselection` VALUES (1243, 37, 40);
INSERT INTO `userpermiasionselection` VALUES (1244, 35, 40);
INSERT INTO `userpermiasionselection` VALUES (1245, 38, 40);
INSERT INTO `userpermiasionselection` VALUES (1246, 28, 40);
INSERT INTO `userpermiasionselection` VALUES (1247, 40, 40);
INSERT INTO `userpermiasionselection` VALUES (1248, 39, 40);
INSERT INTO `userpermiasionselection` VALUES (1249, 55, 40);
INSERT INTO `userpermiasionselection` VALUES (1250, 6, 40);
INSERT INTO `userpermiasionselection` VALUES (1251, 8, 40);
INSERT INTO `userpermiasionselection` VALUES (1252, 31, 40);
INSERT INTO `userpermiasionselection` VALUES (1253, 2, 40);
INSERT INTO `userpermiasionselection` VALUES (1254, 13, 40);
INSERT INTO `userpermiasionselection` VALUES (1255, 25, 40);
INSERT INTO `userpermiasionselection` VALUES (1256, 23, 40);
INSERT INTO `userpermiasionselection` VALUES (1257, 24, 40);

SET FOREIGN_KEY_CHECKS = 1;
