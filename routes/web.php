<?php

use App\Lib\Router;

// homepage
Router::any('/', function () {
    header('Location:' . config('frontEndIndex'));
});

/**
 * --------------------------------------------------------------------------------
 * Please add more custom route files here
 * --------------------------------------------------------------------------------
 */
include_once PROJECT_ROOT_PATH . "/routes/api.php";

Router::dispatch();
