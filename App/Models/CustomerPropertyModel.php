<?php

namespace App\Models;

use App\Lib\DB;

class CustomerPropertyModel extends BaseModel
{
    protected $table = 'customerproperties';

    public function getIndex($start, $length, $search, $order)
    {
        $result = DB::link()->table($this->table)->order($order)->limit($start, $length)->get();
        $toTal = DB::link()->table($this->table)->count();
        $returnArray['recordsFiltered'] = (int)$toTal;
        $returnArray['data'] = $result;
        $returnArray['recordsTotal'] = count($result);
        return $returnArray;
    }

    public function postItem($data, $id)
    {

        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
