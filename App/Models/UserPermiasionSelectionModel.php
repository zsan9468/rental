<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/28
 * Time:  15:08
 */

namespace App\Models;

use App\Lib\DB;
use App\Lib\DBB;

class UserPermiasionSelectionModel
{
    protected $table = 'userpermiasionselection';
    protected $DB;
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }


    public function createAll($params)
    {
        try{
            $sql = "insert into userpermiasionselection (permission_id,user_id) VALUES (?,?)";
            foreach($params as $item){
                $stmt =$this->DB->prepare($sql);
                $res = $stmt->execute(array($item['permission_id'],$item['user_id']));
            }
            if($res){
                return true;
            }else{
                return false;
            }
        }catch (Exception $e){
            return false;
        }
    }

    public function getSignalUserPerssion($id)
    {
        return DB::link()->table($this->table)->where('user_id = '.$id)->get();
    }

    public function delete($id)
    {
        return DB::link()->table($this->table)->where('user_id = '.$id)->delete();
    }

    public function getSignalUserAllPerssion($id)
    {

    }
}