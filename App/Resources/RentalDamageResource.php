<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Lib\UploadFiles;
use App\Models\RentalDamageModel;

class RentalDamageResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new RentalDamageModel;
        $code = 200;

        if (empty($params)) {
            $result['data'] = '';
            $result['code'] = 501;
        } else {
            if (isset($params['id'])) {
                $result['data'] = $newModel->getOne($params['id']);
            } else {
                $start = $params['start'];
                $length = $params['length'];
                $search = $params['search']['value'];


                $field = $params['columns'][$params['order'][0]['column']]['data'];
                switch ($field) {
                    case 'id':
                        $field = "d.id";
                        break;
                    default:
                        $field = "d.id";
                        break;
                }
                $collation  = $params['order'][0]['dir'];
                $order = "$field $collation";

                // FIXME Not finished yet, first deal with the rentalUnit module
                $returnArray = $newModel->getIndex($start, $length, $search, $order);
                $result['data'] = $returnArray;
                $result['data']['draw'] = $params['draw'];
            }
            $result['code'] = $code;
        }
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data)
    {
        $rules = [
            'damagePropertiesId' => ['required'],
            'rentalUnitsId' => ['required'],
            'repairDateTime' => ['required', 'date:Y-m-d'],
            'damageTime' => ['required', 'date:Y-m-d'],
            'damageStatus' => ['required'],
        ];
        $messages = [
            'damagePropertiesId.required' => 'The damagePropertiesId field is required.',
            'rentalUnitsId.required' => 'The rentalUnitsId field is required.',
            'repairDateTime.required' => 'The repairDateTime field is required.',
            'repairDateTime.date' => 'The date format is incorrect, The repairDateTime field is in Y-m-d format.',
            'damageTime.required' => 'The damageTime field is required.',
            'damageTime.date' => 'The date format is incorrect, The damageTime field is in Y-m-d format.',
            'damageStatus.required' => 'The damageStatus field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        if (!empty($data['imageData'])) {
            $uploader = new UploadFiles();
            $imagePath = $uploader->uploadFilesBase64($data['imageData'], 'public/upload/img/damage/');
            $data['damageImgPath'] = '/' . $imagePath;
        }
        unset($data['imageData']);
        $newModel = new RentalDamageModel;
        $resultCode = $this->baseUpdate($newModel, $data);

        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {
        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new RentalDamageModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
