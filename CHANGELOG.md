# Changelog

## 0.2.8 - 2023.02.20

- Added: LoggerInterface

- Added: file_exists /config/database.php

- Moded: Composer is no longer required for autoloading

## 0.2.7 - 2023.02.16

- Added: autoload classes with composer

- Added: namespace

- Moded: Standardized return data format

- Moded: The controller will return different error message content

- Fixed: Throw an exception when updating or inserting no body jsondata

- Fixed: Even if there is no data, it can still be successfully updated or deleted

## 0.2.6 - 2023.02.08

- Added: new router class

- Removed: other router lib

## 0.2.5 - 2023.02.06

- Added: query builder

## 0.2.0 - 2023.02.03

- Added: router

## 0.1.0 - 2023.01.31

- Refactor the project to introduce the REST API standard.

## 0.0.9 - 2023.01.25

- The initial version implements simple CURD operations.
