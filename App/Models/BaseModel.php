<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class BaseModel
{
    public function baseGetIndex($tableName, $start, $length, $search, $order, $whereSql)
    {
        if ($search == '') {
            if ($start == 0 && $length == 0) {
                return  DB::link()->table($tableName)->get();
            }
            $result = DB::link()->table($tableName)->order($order)->limit($start, $length)->get();
            $toTal = DB::link()->table($tableName)->count();
        } else {
            $result = DB::link()->table($tableName)->where($whereSql)->order("'$order[0] $order[1]'")->limit($start, $length)->get();
            $toTal = DB::link()->table($tableName)->where($whereSql)->count();
        }
        $returnArray['recordsFiltered'] = (int)$toTal;
        $returnArray['data'] = $result;
        $returnArray['recordsTotal'] = count($result);
        return $returnArray;
    }

    public function basePostItem($tableName, $data, $id)
    {
        try {
            if (isset($data['id'])) {
                // update
                // Before updating or deleting, you should check whether there is data
                $ReturnBool = false;
                $id = $data['id'];
                $oldData = DB::link()->table($tableName)->where("id = $id")->field('id')->get();
                if (!empty($oldData)) {
                    $ReturnBool = DB::link()->table($tableName)->where("id = $id")->update($data);
                }
            } else {
                // insert
                $ReturnBool = DB::link()->table($tableName)->insert($data);
            }
        } catch (PDOException $e) {
            FileLogger::error('Error Update data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function baseDeleteItem($tableName, $id)
    {
        try {
            $ReturnBool = DB::link()->table($tableName)->where("id = $id")->delete();
        } catch (PDOException $e) {
            FileLogger::error('Error deleting data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function getSingle($tableName, $id, $whereField = 'id')
    {
        try {
            $whereSql = "$whereField = $id";
            $returnArray = DB::link()->table($tableName)->where($whereSql)->get();
        } catch (PDOException $e) {
            // Log the error
            FileLogger::error('Error Get data: ' . $e->getMessage());
        }
        return $returnArray;
    }
}
