<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title "><text class="lang" key="rentalUnits.DamagesProperties_title">DamagesProperties</text>  <span class="lang" key="list.title"> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary lang" id="customer_properties_add"  key="rentalUnits.Create DamagesProperties">
                                            Create DamagesProperties
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="damages_properties_properties_table" class="hover table-striped" style="width:100%">-->
                                <table id="damages_properties_properties_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="damages_properties_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.damagesproperty">Create New
                            Unitnames</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.damagesproperty">Create New
                            Damagesproperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'damagesproperties_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="update_damages_properties" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.damagesproperty">Update
                            Damagesproperty</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'damagesproperties_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'damageName', title: "damageName", name: "damageName" },
            { data: 'damageDescription', title: "damageDescription", name: "damageDescription" },
            { data: 'Action', title: "Action", name: "damageDescription", "orderable" : false,render: (data, type, row) => {

                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    console.log(row)
                    $('#damages_properties_properties_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#damages_properties_properties_table').on('click', 'tbody #' + upodateBtn, function (e) {

                        $('#update_damages_properties').modal('show');
                        $("#damages_properties_form_update input[name=id]").val(row.id);
                        $("#damages_properties_form_update input[name=damageName]").val(row.damageName);
                        $("#damages_properties_form_update input[name=damageDescription]").val(row.damageDescription);
                    })

                    $('#damages_properties_properties_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#damages_properties_properties_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_damage_properties');
                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_damage_properties', 'get', 'damages_properties_properties_table', columns,);

        $('#customer_properties_add').click(function (e) {
            $('#damages_properties_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#damages_properties_form_create').submit(function (e) {
            var data =  $.damages_propertiesBiaodan('damages_properties_form_create',e)
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_damage_properties','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#damages_properties_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#damages_properties_form_update').submit(function (e) {
            var id = $("#damages_properties_form_update input[name=id]").val();
            var data =  $.damages_propertiesBiaodan('damages_properties_form_update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_damage_properties','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#update_damages_properties').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>