<form class="row g-3 needs-validation " id="agreement_create_form" novalidate>
    <div class="col-md-10">
        <label for="firstName" class="form-label">Please select customer</label>
        <input type="text"  autocomplete="off"  onchange="customer_select()" class="form-control"  list="customer_list"  id="customer_name" name="customer_name" value=""   placeholder="Please select customer" required>

        <datalist id="customer_list" name="firstName">
        </datalist>
    </div>
    <div class="col-md-4">
        <input type="hidden" class="form-control" id="costumerId" name="costumerId" value="" required>
        <input type="hidden" class="form-control" id="rentalUnitsId" name="rentalUnitsId" value="" required>
        <label for="lastName" class="form-label">rentalBeginDateTime</label>
        <input type="datetime-local" class="form-control" id="rentalBeginDateTime" name="rentalBeginDateTime" value="" autocomplete
               required>
    </div>
    <div class="col-md-4">
        <label for="lastName" class="form-label">rentalEndDateTime</label>
        <input type="datetime-local" class="form-control" id="rentalEndDateTime" name="rentalEndDateTime" value="" autocomplete
               required>
    </div>

    <div class="row col-md-10" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label">insurenceId</label>
        <div class="col-sm-8">
            <select id="insurenceId" name="insurenceId" class="form-select">

            </select>
        </div>
    </div>

    <div class="col-12">
        <label class="col-md-4 col-form-label"><text class="text-primary">RentalAgreement Properties</text></label>
        <div class="rentalAgreement_properties">
        </div>
    </div>

    <div class="col-12 AddPropertySTyle">
        <label class="col-md-4 col-form-label"><text class="text-primary AddProperty" >unitExtras</text></label>
        <div class="row AddPropertList">

        </div>
    </div>

    <div class="col-md-6">
        <label for="Cost" class="form-label">Cost</label>
        <input type="text" class="form-control" id="cost" name="cost" value="">
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">affirm rent</button>
    </div>
</form><!-- End Custom Styled Validation -->


