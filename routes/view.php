<?php

use App\Lib\Router;

// loginpage
Router::any('/login', function () {
    header('Location: ./resources/view/login/login.php');
});


// homepage
Router::get('/index', function () {
    include './resources/view/index/index.php';
});

Router::get('/user/users_profile', function () {
    include './resources/view/user/users_profile.php';
});
