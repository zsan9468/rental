
<div class="tab-pane fade Edit_appsettings pt-3" id="profile-edit">
    <!-- Profile Edit Form -->
    <form id="edit_appsettings_info" class="needs-validation" novalidate>
        <div class="row mb-3">
            <label for="AppName" class="col-md-1 col-lg-1 col-form-label">AppName</label>
            <div class="col-md-4 col-lg-4">
                <input name="AppName" type="text" class="form-control eidt_AppName" id="AppName" value="" required>
                <input name="id" type="hidden" class="form-control" id="id" value="">
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Name
                </div>
            </div>
        </div>

        <div class="row mb-3">
            <label for="surname" class="form-check-label col-md-2 col-lg-2 col-form-label">OnPremiseEnable</label>
            <div class="col-md-1 col-lg-1" style="text-align: left">
                 <input  class='form-check-input  OnPremise' type='checkbox' name='OnPremise'  > <i class="bi bi-check"></i>
            </div>
        </div>


        <div class="row mb-3">
            <label for="email" class="col-md-1 col-lg-1 col-form-label">IP</label>
            <div class="col-md-2 col-lg-2">
                <input name="IP" type="text" class="form-control eidt_IP" id="IP" value="" required>
                <div class="valid-feedback">
                    Correct input format
                </div>
                <div class="invalid-feedback">
                    Please provide a Email
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <label for="email" class="col-md-1 col-lg-1 col-form-label">upload Logo</label>
            <div class="col-md-2 col-lg-2 logoPathShow" >
                <input name="" type="file" class="form-control  " id="selectlogoPath">
                <input name="imageData" type="hidden"  id="logoPath" class="form-control logoPath "  >
                <img src="/resources/assets/img/logo.png"  class="logo_imag pt-2" alt="" style="width: 200px;height: 200px;">
            </div>
        </div>


        <div class="row mb-3">
            <label for="email" class="col-md-1 col-lg-1 col-form-label">favicon</label>
            <div class="col-md-2 col-lg-2 logoPathShow" >
                <input name="" type="file" class="form-control  " id="faviconDataPath">
                <input name="faviconData" type="hidden"  id="faviconData" class="form-control logoPath " >
                <img src=""  class="faviconData pt-2" alt="" style="">
            </div>
        </div>


        <div class="text-center">
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
    </form><!-- End Profile Edit Form -->

</div>
