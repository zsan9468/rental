<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/28
 * Time:  16:58
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Lib\DB;
use App\Models\CostcenterSelectionModel;
use App\Models\UserPermiasionSelectionModel;

class CostcenterSelectionResource
{

    protected $table = 'costcenterselection';
    /**
     * Store a newly created resource in storage.
     *
     */
    public function store($costcerterId, $userId)
    {
        if (!empty($costcerterId)) {
            $costcenterSelectMode = new CostcenterSelectionModel();
            $data['costCenterId'] = $costcerterId;
            $data['userId'] = $userId;

            $is_value = $costcenterSelectMode->checkCostcenterSelection($data);
//          The hook checks to see if a binding record exists
            if ($is_value) {
                ApiOutputController::ApiOutput([], 502, '');
            }
            $result = $costcenterSelectMode->cerate($data);

            if ($result) {
                return true;
            } else {
                return false;
            }

        }
    }

    public function update($cost_center,$userId)
    {
        $costcenterSelectMode = new CostcenterSelectionModel();
        $costcenterSelectMode->update($cost_center,$userId);
    }

    public function delete($id)
    {
            DB::link()->table($this->table)->where('userId = '.$id)->delete();
    }
}