<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title"> <text class="lang"     key="rentalUnits.Rental.title">Rental</text> <span class="lang"     key="list.title"> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary lang" id="rentalunit_add_view"
                                                key="rentalUnits.create.rentalunits">
                                            Create RentalUnits Info
                                        </button>
                                    </div>
                                </div>
                                <table id="rentalUnit_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="rental_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalUnits.create.rentalunits">Create New
                        RentalUnits</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'rental_add.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="update_rental_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalUnits.update.rentalunits">Create New
                            RentalUnits</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">

                                <?php include 'rental_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="damages_add_model" tabindex="-2" style="z-index: 9998;">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg" style="max-width: 1200px">
                <div class="modal-content" style="width: 1200px; height: auto">

                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalUnits.create.rentalunits.damage">Create New
                            Damage </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'damages.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="damages_drawing_model" tabindex="-1" style="z-index: 9999;">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"  style="max-width: 1600px">
                <div class="modal-content"  style="max-width: 1600px">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalUnits.create.rentalunits.damage">在线画图</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'drawing.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="more_action_model" tabindex="-1"  style="z-index: 9997;">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"  style="max-width: 1000px">
                <div class="modal-content"  style="max-width: 1000px">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.more.rentalunits">More Information</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body pt-3">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'renta_more.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="agreement_add_model" tabindex="-1"  style="z-index: 9997;">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"  style="max-width: 1000px">
                <div class="modal-content"  style="max-width: 1000px">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.agreement.create">Create new Agreement</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body pt-3">
                        <div class="card">
                            <div class="card-body">
                                <?php include '../agreement/agreement_add.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>

    function customer_select(){
        var ar =$("#agreement_create_form #customer_list option[value = "+$('#agreement_create_form #customer_name').val()+"]");
        $("#agreement_create_form #costumerId").val($('#agreement_create_form #customer_name').val());
        $('#agreement_create_form #customer_name').val(ar[0].outerText);
    }
    $(document).ready(function () {
        $.agreementInitialize("agreement_create_form",'customer_list')
        $("#damageImg").change(function () {
            $('.damage_imag').attr('src','');
            $.getImageBase(this, 'damage_imag', 'damageImgPath')
        });
        $("#rental").click(function () {
            $('#agreement_add_model').modal('show');
            var rentalUnitid =  $('.unitsid').val();
            $('#agreement_create_form #rentalUnitsId').val(rentalUnitid);

        });



    $('#agreement_create_form').submit(function (e) {

           var data = $.rentalAgreementBiaodan('agreement_create_form',e,'create')
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement', 'post', data,
                function (data) {
                    if (data.payload.code == 200) {
                        $('#agreement_add_model').modal('hide');
                        commonUtil.message('create success！')
                        table.ajax.reload();
                    } else {
                        commonUtil.message(data.payload.data);
                    }
                }, function (data) {
                    console.log(data)
                })


        })

        /**
         * 定义出租单元显示列表 Define the rental unit display list
         * @type {[{data: string, name: string, title: string},{data: string, name: string, title: string},{data: string, name: string, title: string},{data: string, name: string, title: string},{data: string, name: string, title: string},null,null,null]}
         */
        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'name', title: "name", name: "name",className:"lang",key:'units_name' },
            { data: 'location', title: "location", name: "location" },
            { data: 'price', title: "price 💰 ", name: "price" },
            { data: 'priceGroup', title: "priceGroup", name: "priceGroup" },
            { data: 'typName', title: "typName", name: "typName" },
            { data: 'type', title: "type", name: "type" },
            {
                data: 'Action', title: "Action", name: "surname", "orderable": false, render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    var damagesBtn = "damages" + row.id;
                    $('#rentalUnit_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#rentalUnit_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#rentalUnit_table').undelegate('tbody #' + damagesBtn, 'click');
                    $('#rentalUnit_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $.RentalUnitRelevantInformatio('rental_form_update');
                        $('#update_rental_add_model').modal('show');
                        $('.damage_imag').attr('src','');
                        data = { 'id': row.id }
                        var signalRentunit = formAjaxHy('/api/rental?id=' + row.id, '', 'get');
                        //反显示数据
                        $("#rental_form_update input[name=price]").val(row.price)
                        $("#rental_form_update input[name=id]").val(row.id)
                        $("#rental_form_update input[name=unittyp_id]").val(row.unittyp_id)
                        $("#rental_form_update input[name=priceGroup]").val(row.priceGroup)
                        $("#rental_form_update input[name=typDescription]").val(row.typDescription)
                        $("#rental_form_update input[name=typName]").val(row.typName)
                        $("#rental_form_update #unitLocationPropertiesId").val(row.unitlocation_id)
                        $("#rental_form_update #unitNamesId").val(row.unitnames_id)
                        $("#rental_form_update #statusTypId").val(row.statustyp_id)
                        // 循环
                        var propertiesUnit = signalRentunit.payload.data.properties;
                        //判断出租单元属性平并循环输出
                        console.log(propertiesUnit)
                        if (propertiesUnit.length > 0) {
                            $('#rental_form_update  .Rental_properties').html("");
                            for (i = 0; propertiesUnit.length > i; i++) {
                                $('#rental_form_update  .Rental_properties').append("<div class='row propertyNameClss'>" +
                                    "<div class='col-md-3'>" + propertiesUnit[i].rentalPropertieName + "</div>" +
                                    " <div class='col-md-4'>" +
                                    "<input type='hidden' maxlength='11' class='form-control' id='' name='selectionId' value='" + propertiesUnit[i].selectionId + "'>" +
                                    "<input type='hidden' maxlength='11' class='form-control' id='' name='rentalPropertiesId' value='" + propertiesUnit[i].rentalPropertiesId + "'>" +
                                    "<input type='text' maxlength='11' class='form-control' id='rentalPropertiesValue' name='rentalPropertiesValue' value='" + propertiesUnit[i].rentalPropertiesValue + "'>" +
                                    "</div>" +
                                    " </div>")
                            }
                        }

                    })
                    $('#rentalUnit_table').on('click', 'tbody #' + damagesBtn, function (e) {
                        $("#damages_add_model").modal('show');
                        $('.damage_imag').attr('src','');
                        $("#Damage_form_create input[name=rentalUnitsId]").val(row.id)
                        $.GetDamage('Damage_form_create');
                    })

                    $('#rentalUnit_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental');

                    })

                   // "<button type=\"button\" class=\"btn btn-sm  btn-outline-secondary  \"" + " id=" + damagesBtn + " >damages</button> " +
                    return     "<button type=\"button\" onclick='moreAction("+row.id+")' class=\"btn btn-sm  btn-outline-secondary lang \" key=\"rental.moreaction\"  >moreAction</button> " +
                        "<button type=\"button\" class=\"btn btn-sm btn-outline-secondary  \"" + " id=" + upodateBtn + " lang \" key=\"rental.moreaction\" >Update</button> " +
                        "  <button type=\"button\" class=\" btn btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";

                }
            },
        ];
        /**
         * 获取表单数据源 Get form data source
         */
        tableAjax('/api/rental', 'get', 'rentalUnit_table', columns,);
        $('#rentalunit_add_view').click(function (e) {
            $(".customer_properties").html('');
            $('#rental_add_model').modal('show');
            $.RentalUnitRelevantInformatio('rental_form_create')
        })
        /**
         * 创建新的出租单元  Create new rental units
         */
        $('#rental_form_create').submit(function (e) {
            var data = $.rentalUnitInfoBiaodan('rental_form_create',e,'create')
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental', 'post', data,
                function (data) {
                    if (data.payload.code == 200) {
                        $('#rental_add_model').modal('hide');
                         commonUtil.message('create success！')
                        table.ajax.reload();
                    } else {
                         commonUtil.message(data.payload.data);
                    }
                }, function (data) {
                    console.log(data)
                })


        })
        /**
         * 修改出租单元表单提交 Modify the rental unit form submission
         *rentalUnitInfoBiaodan 表单字段 Form Field
         */
        $('#rental_form_update').submit(function (e) {
            var id = $("#rental_form_update input[name=id]").val();
            var data = $.rentalUnitInfoBiaodan('rental_form_update',e,'update')
            data.rental.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental', 'put', data,
                function (data) {
                    if (data.payload.code == 200) {
                        $('#update_rental_add_model').modal('hide');
                         commonUtil.message('Update success！')
                        table.ajax.reload();
                    } else {
                         commonUtil.message(data.payload.data);
                    }
                }, function (data) {
                    console.log(data)
                })


        })

        $('#Damage_form_create').submit(function (e) {
            var data = $.rental_damage_Biaodan('Damage_form_create')
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_damage', 'post', data,
                function (data) {
                    if (data.payload.code == 200) {
                        $('#damages_add_model').modal('hide');
                        commonUtil.message('SUCCESSS!')
                        table.ajax.reload();
                    } else {
                         commonUtil.message(data.payload.data);
                    }
                }, function (data) {
                    console.log(data)
                })
        })
       $('#damageCreate').click(function (){
          var rentalUnitid =  $('.unitsid').val();
          if(rentalUnitid){
              $("#damages_add_model").modal('show');
              $('.damage_imag').attr('src','');
              $("#Damage_form_create input[name=rentalUnitsId]").val(rentalUnitid)
              $.GetDamage('Damage_form_create');
          }else{
              commonUtil.message('not find rentalUnitId!','danger')
          }
       })

    });
    /**
     * 更多操作
     */
    function moreAction(e)
    {
        $('.rental_property').html("");
        $('#more_action_model').modal('show')
        $('.unitsid').val(e);
        var responses = formAjaxHy('/api/rental?id='+e,'', 'get');
        var rentalData =responses.payload.data.data[0];
        $('.unitLocation').text(rentalData.location);
        $('.location_description').text(rentalData.location_description);
        $('.manufacture').text(rentalData.manufacture);
        $('.names').text(rentalData.names);
        $('.priceGroup').text(rentalData.priceGroup);
        $('.UnitNames').text(rentalData.name);
        $('.typName').text(rentalData.typName);
        $('.typDescription').text(rentalData.status_description);
        $('.statusTyp').text(rentalData.type);
        $('.price').text(rentalData.price);
        if(responses.payload.data.data.properties){
            var propertiesUnits = responses.payload.data.data.properties;
            var propertiesUnitsDom = [];
            for (var i = 0 ;propertiesUnits.length > i ;i++ )
            {
                var properItem = propertiesUnits[i]
                if(properItem.rentalPropertiesValue != ''){
                    propertiesUnitsDom.push(" <div class='row'> <div class='col-lg-4 rentalPropertieName' >"+properItem.rentalPropertieName+"</div> " +
                    "<div class='col-lg-4 rentalPropertiesValue'>"+properItem.rentalPropertiesValue+"</div>" +
                    " <div class='col-lg-4 propertyDescription'>"+properItem.propertyDescription+"</div> </div>");
                }

            }
            $('.rental_property').html(propertiesUnitsDom.join(' '))
        }
        $('.unitsid').val(e);
    }

</script>
</body>

</html>