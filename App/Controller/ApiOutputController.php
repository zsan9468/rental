<?php

namespace App\Controller;

use App\Lib\FileLogger;

class ApiOutputController
{
    /**
     * @Description Data is returned only when the return status code is 200,
     *              otherwise the corresponding string will be displayed
     * @DateTime 2023-02-28
     */
    public static $statusTexts = [
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        204 => 'Deleted',

        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        408 => 'Request Timeout',
        410 => 'Gone',
        411 => 'Database Connection Error',
        412 => 'Data validation failed',

        500 => 'The result of the query is empty',
        501 => 'Missing Required Parameter',
        502 => 'Operation Failed!',

        503 => 'id must exist! ',
        504 => 'The query result is empty of permission!',
        505 => 'The query result is empty of costCenter!',
        506 => 'costCenter is currently in use and cannot be deleted!',

        601 => 'User already exists! ',
        602 => 'User Not exists! ',
        603 => 'User password is incorrect! ',

        604 => 'Change password need to provide old password, new password and confirm password, your parameter is incorrect!',
        605 => 'The new password is different from the confirmed password! ',
        606 => 'The password of the current login user is incorrect!! ',
        607 => 'Login information not found, please log in again!',
        608 => 'The current user has no permission! ',
        609 => 'The same level cannot be operated!',
        701 => 'Failed to add permissions, please check the program! ',
        702 => 'Do not allow the same operation authority! ',
        703 => 'There are other permissions under this permission level, so they cannot be deleted! ',
        704 => 'Failed to delete permissions, please check the program! ',
        801 => 'Current rental unit used in other information, failed to delete !',

        // Model Related
        10400 => 'Model error, missing table name',
        10401 => 'Model error, When updating or deleting, where is required',
        10402 => 'Please pass in the second parameter, the data to be inserted cannot be empty',

        // Agreement Related
        11400 => 'The current rentalunit cannot be rented',
        11401 => 'The agreement that has been generated does not support the replacement of the rental unit at the moment, please create a new agreement.',
        11403 => 'Illegal date, end date must be later than start date'

    ];

    public static function ApiOutput($data, $code, $header = '')
    {
        if ($code < 300) {
            $success = true;
        } else {
            FileLogger::warning('Failed to return data');
            $success = false;
        }

        // Define the returned API data header
        header_remove('Set-Cookie');
        $httpHeaders = array('Content-Type: application/json', $header);
        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }

        if ($code != 200) {
            if (empty($data)) {
                $data = self::$statusTexts[$code];
            }
        }
        // Define the returned API data format
        $returnData = json_encode(array(
            'success' => $success,
            'payload' => [
                'code' => $code,
                'data' => $data
            ]
        ));

        echo $returnData;
        exit;
    }
}
