<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date: 2023/2/23
 * Time: 21:15
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Lib\UploadFiles;
use App\Models\UserModel;
use App\Models\UserPermiasionSelectionModel;

class UserResource extends ApiOutputController
{
    /**
     * @param $userInfo    User input information, including account number and password
     * @return void
     */
    public function checkUserInfo($userInfo)
    {

        $rules = [
            'username' => ['required'],
            'password' => ['required'],
        ];
        $messages = [
            'username.required' => 'The username field is required.',
            'password.required' => 'The password field is required.',
        ];
        $validator = new \App\Lib\Validator($userInfo, $rules, $messages);
        $resultValidata = $validator->validate();
        if ($resultValidata !== true) {
            ApiOutputController::ApiOutput($resultValidata, 412);
        }

        $currentDbUserSalt = '';
        $userModel = new UserModel();
        // Obtain the info of the corresponding user in the database;
        $userDbInfo = $userModel->getUserInfo($userInfo['username']);
        if ($userDbInfo == false) {
            ApiOutputController::ApiOutput([], 602, '');
        }
        //Get the salt value from the database
        if (count($userDbInfo) > 0) {
            $currentDbUserSalt = $userDbInfo['saltSHA3'];
        }
        //Gets the salt password made up of user input
        $loginResource = new LoginResource();
        $loginUserHashInfo = $loginResource->getPassWord($userInfo['password'], $currentDbUserSalt);
        // making comparisons
        if ($userDbInfo['passwordSHA3'] == $loginUserHashInfo['passwordSHA3']) {
            // Store user information in SESSION
            $_SESSION['userInfo'] = $userDbInfo;
            $PermissionResource  = new PermissionResource();
            $permiasionsArr = $PermissionResource->permissionList($userDbInfo['userId']);
            //获取所有权限
            if ($userDbInfo['username']  == 'admin_boss') {
                $permiasionsArr = $PermissionResource->permissionListClassify();
            }
            $permissions['sqlMenu'] = $PermissionResource->arrayPidProcess($permiasionsArr['sqlMenu']);
            $permissions['sqlMenu'] = $PermissionResource->objArrayToArray($permissions['sqlMenu']);

            $currentUserPermissionArr = array();
            foreach ($permiasionsArr['sqlAction'] as $item) {
                array_push($currentUserPermissionArr, $item['id']);
            }
            $permissions['sqlAction'] = $currentUserPermissionArr;

            $_SESSION['permission'] = $permissions;
            unset($userDbInfo['passwordSHA3'],$userDbInfo['saltSHA3']);
//            session_id('fdbccvvkfmvs1spd4s5i7');print_r($_SESSION);exit;
            $this->ApiOutput(json_encode($userDbInfo), 200, '');
        }
        $this->ApiOutput([], 603, '');
    }

    /**
     * Add a user This parameter specifies the configuration information required for user modification
     * @return array
     */
    public function getUserCorrelation()
    {

        $resultArr = [];
        // Obtain the permission list
        $permissionResource = new PermissionResource();
        $permissions = $permissionResource->permissionList();
        // GETcostcerter Info
        $costCenterRe = new CostCenterResource();

        $resultArrCenterRe = $costCenterRe->getAllCostCenter();
        // 处理权限集合:Sets of processing permissions
        if ($permissions) {
            $permissions = $permissionResource->arrayPidProcess($permissions);
            $permissions = $permissionResource->objArrayToArray($permissions);
            $resultArr['permissions'] = $permissions;
        } else {
            ApiOutputController::ApiOutput('', 504, '');
        }
        // 处理costCerter集合: Process the costCerter collection
        if ($resultArrCenterRe) {
            $resultArr['costCerter'] = $resultArrCenterRe;
        } else {
            ApiOutputController::ApiOutput('', 504, '');
        }
        return $resultArr;
    }

    /**
     * 处理菜单
     * Processing menu
     */
    public function arrayPidProcess($data, $res = array(), $pid = '0')
    {
        // var_dump($data);
        foreach ($data as $k => $v) {
            if ($v['father_id'] == $pid) {
                $res[$v['id']]['info'] = $v;
                $res[$v['id']]['child'] = $this->arrayPidProcess($data, array(), $v['id']);
            }
        }
        return $res;
    }

    /**
     * @param $res
     * @return array
     * 处理[] 数组和array数组之间的转换 :Handle conversions between [] arrays and array arrays
     */
    public function objArrayToArray($res)
    {
        $arr = array();
        foreach ($res as $item) {
            $item['son'] = array();
            if (!empty($item['child'])) {
                foreach ($item['child'] as $childItem) {
                    array_push($item['son'], $childItem);
                }
            }
            array_push($arr, $item);
        }
        return $arr;
    }

    /**
     * @param $dataPar
     * 用户列表数据处理  List Data Processing
     *
     */
    public function index($params)
    {
        if (empty($params)) {
            ApiOutputController::ApiOutput([], 501, '');
        }

        $start = $params['start'];
        $length = $params['length'];

        //Datatables the value that the table plug-in needs to pass
        $draw = $params['draw'];
        if ($draw > 0) {
            $search = $params['search']['value'];
        }
        $userModel = new UserModel();
        // Process collation rules
        $field = $params['columns'][$params['order'][0]['column']]['data'];
        $collation  = $params['order'][0]['dir'];
        $order = "$field $collation";

        $result = $userModel->getUserList($start, $length, $search, $order);
        $result['draw'] = $draw;
        if ($result) {
            return $result;
        }
    }

    /**
     * Access to personal information all
     * @param $data [id:1]
     * @return void
     */
    public function show($data)
    {
        $userModel = new UserModel();
        // Get detailed user information
        $details = false;
        if (!empty($data['details']) && $data['details'] == true) {
            if (!empty($_SESSION['userInfo'])) {
                $data['id'] = $_SESSION['userInfo']['userId'];
                $details = true;
            } else {
                ApiOutputController::ApiOutput([], 607, '');
            }
        }
        if (empty($data['id'])) {
            ApiOutputController::ApiOutput([], 501, '');
        }
        //       userinfo list
        $userInfo = $userModel->getUserAccordingId($data['id'], $details);
        if (!$userInfo) {
            ApiOutputController::ApiOutput([], 602, '');
        }
        // permission list user
        $permiasionModel = new UserPermiasionSelectionModel();
        $permiasionArr = $permiasionModel->getSignalUserPerssion($data['id']);
        $permiasions = array();
        foreach ($permiasionArr as $item) {
            array_push($permiasions, $item['permission_id']);
        }
        $userInfo['permissions'] = $permiasions;
        $userInfo['userInfo'] = $userInfo[0];
        unset($userInfo[0]);
        ApiOutputController::ApiOutput($userInfo, 200, '');
    }

    /**
     * Store a newly created resource in storage.
     * Save user information
     */
    public function store($requestData)
    {
        $rules = [
            'username' => ['required'],
            'password' => ['required', 'min:6'],
            'email' => ['email'],
        ];
        $messages = [
            'username.required' => 'The username field is required.',
            'password.required' => 'The password field is required.',
            'password.min' => 'The password must be at least 6 characters.',
            'email.email' => 'The email field must be a valid email address.',
        ];
        $validator = new \App\Lib\Validator($requestData, $rules, $messages);
        $resultValidata = $validator->validate();
        if ($resultValidata !== true) {
            ApiOutputController::ApiOutput($resultValidata, 412);
        }

        $resultCode = 601;
        $UserModel = new UserModel();
        // 处理密码
        $loginResouce = new LoginResource();
        $requestData['password'] = $loginResouce->getPassWord($requestData['password']);

        // 添加的用户信息
        $tablesData['passwordSHA3'] = $requestData['password']['passwordSHA3'];
        $tablesData['saltSHA3'] = $requestData['password']['saltSHA3'];
        $tablesData['username'] = $requestData['username'];
        $tablesData['name'] = $requestData['name'];
        $tablesData['surname'] = $requestData['surname'];
        $tablesData['email'] = $requestData['email'];
        $tablesData['user_type'] = $requestData['user_type'];
        $upload = new UploadFiles();
        $profilePhotp = $upload->createRandProfilePhoto('public/Img/user', $tablesData['username']);
        $tablesData['profile_photo']  = $profilePhotp;
        $result = $UserModel->create($tablesData);
        if ($result) {
            // 处理权限
            $resoucePermisson = new UserPermiasionSelectionResource();
            $resoucePermisson->store($requestData['permission'], $result);
            // 处理costCenter
            $costcenterSelection = new CostcenterSelectionResource();
            $costcenterSelection->store($requestData['cost_center'], $result);
            $resultCode = 200;
        }
        ApiOutputController::ApiOutput($result, $resultCode);
    }

    /**
     * Update the specified resource in storage.
     * @param  int  $id
     */
    public function update($requestData)
    {
        if (empty($requestData['userId']) == true) {
            ApiOutputController::ApiOutput([], 503, '');
        }

        $UserModel = new UserModel();
        // 处理密码
        if (!empty($requestData['password'])) {
            $loginResouce = new LoginResource();
            $requestData['password'] = $loginResouce->getPassWord($requestData['password']);
            $tablesData['passwordSHA3'] =   $requestData['password']['passwordSHA3'];
            $tablesData['saltSHA3'] =   $requestData['password']['saltSHA3'];
        }
        // 添加的用户信息
        if (isset($requestData['username'])) {
            $tablesData['username'] =   $requestData['username'];;
        }
        if (isset($requestData['surname'])) {
            $tablesData['surname'] =   $requestData['surname'];
        }
        if (isset($requestData['user_type'])) {
            $tablesData['user_type'] =   $requestData['user_type'];
        }
        if (isset($requestData['name'])) {
            $tablesData['name'] =   $requestData['name'];
        }
        if (isset($requestData['email'])) {
            $tablesData['email'] =   $requestData['email'];
        }

        if (isset($requestData['profile_photo']) && !empty($requestData['profile_photo']) ) {
            $tablesData['profile_photo'] =   $requestData['profile_photo'];
            $uploadRe = new UploadFiles();
            $tablesData['profile_photo'] = $uploadRe->uploadFilesBase64($tablesData['profile_photo'],'public/Img/user/');
        }

        // Administrators cannot modify administrators
//        $current_user_type = $_SESSION['userInfo']['user_type'];
//        $updateUser = $UserModel->getUserAccordingId($requestData['userId']);
//        $updateUser_user_type = $updateUser[0]['user_type'];
//        // var_dump($updateUser_user_type,$current_user_type);exit;
//        if ($current_user_type == $updateUser_user_type) {
//            if ($current_user_type != 1) {
//                ApiOutputController::ApiOutput('', 609, '');
//            }
//        }

        $result = $UserModel->update($tablesData, $requestData['userId']);

        if (isset($requestData['email'])) {
            $tablesData['email'] =   $requestData['email'];
        }

        // change permission
        $resoucePermisson = new UserPermiasionSelectionResource();
        if (isset($requestData['permission'])) {
            if (!empty($requestData['permission'])) {
                $resoucePermisson->update($requestData['permission'], $requestData['userId']);
            } else {
                $resoucePermisson->destroy($requestData['userId']);
            }
        }

        // change costCenter
        if (!empty($requestData['cost_center'])) {
            $costcenterSelection = new CostcenterSelectionResource();
            $costcenterSelection->update($requestData['cost_center'], $requestData['userId']);
        }

        if ($result) {
//            更新缓存数据
            $data =  $UserModel->getUserAccordingId($requestData['userId']);
            unset($data[0]['passwordSHA3'],$data[0]['saltSHA3']);
            ApiOutputController::ApiOutput(json_encode($data[0]), 200, '');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id

     */
    public function destroy($data)
    {
        if (empty($data['id']) == true) {
            ApiOutputController::ApiOutput([], 503, '');
        }
        $userModel = new UserModel();
        $result = $userModel->delete($data['id']);
        if ($result) {
            ApiOutputController::ApiOutput([], 200, '');
        }
    }

    /**
     * change password
     */
    public function chanagePassword($data)
    {
        if (!empty($data['password'])  && !empty($data['newpassword'])  && !empty($data['renewpassword'])) {
            if ($data['newpassword'] !== $data['renewpassword']) {
                ApiOutputController::ApiOutput([], 605, '');
            }

            $userModel = new  UserModel();
            $userResult = $userModel->getUserAccordingId($_SESSION['userInfo']['userId']);
            $loginResource = new LoginResource();
            $passWord = $loginResource->getPassWord($data['password'], $userResult[0]['saltSHA3']);
            if ($userResult[0]['passwordSHA3'] !== $passWord['passwordSHA3']) {
                ApiOutputController::ApiOutput([], 606, '');
            };
            $newPassWord = $loginResource->getPassWord($data['newpassword']);
            $result = $userModel->update($newPassWord, $_SESSION['userInfo']['userId']);
            if ($result) {
                ApiOutputController::ApiOutput([], 200, '');
            } else {
                ApiOutputController::ApiOutput([], 502, '');
            }
        }
        ApiOutputController::ApiOutput([], 604, '');
    }
}
