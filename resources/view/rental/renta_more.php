

<div class="tab-content  pt-2">
<div class="tab-pane fade show active profile-overview Units-overview" id="profile-overview">
    <h5 class="card-title">Units Details</h5>
    <div class="row">
        <div class="col-lg-2 col-md-4 label ">UnitNames</div>
        <div class="col-lg-9 col-md-8"><text class="UnitNames">UnitNames</text></div>
        <input type="hidden" name="id" class="unitsid" value="">
    </div>

    <div class="row" >
        <div class="col-lg-2 col-md-4 label">typName</div>
        <div class="col-lg-9 col-md-8"><text class="typName"></text></div>
    </div>
    <div class="row" >
        <div class="col-lg-2 col-md-4 label">statusTyp</div>
        <div class="col-lg-9 col-md-8"><text class="statusTyp"></text></div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 label">typDescription</div>
        <div class="col-lg-9 col-md-8"><text class="typDescription"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-4 label">priceGroup</div>
        <div class="col-lg-9 col-md-8"><text class="priceGroup"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-4 label">price</div>
        <div class="col-lg-9 col-md-8"><text class="price"></text></div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-4 label">unitLocation</div>
        <div class="col-lg-9 col-md-8"><text class="unitLocation"></text></div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 label">location_description</div>
        <div class="col-lg-9 col-md-8"><text class="location_description"></text></div>
    </div>

    <div class="row pt-3 rental_property_title">
        <div class="col-lg-2 col-md-2 label">properties</div>
        <div class="col-lg-9 col-md-8 rental_property">
        </div>
    </div>
</div>
    <div class="col-12 pt-2">
        <button type="button" class="btn btn-info" id="damageCreate" >Add Damages Record</button>
        <button class="btn btn-primary"   id="rental"  type="">Rent</button>
        <button type="button"  disabled class="btn btn-secondary">more Action</button>
    </div>
</div>