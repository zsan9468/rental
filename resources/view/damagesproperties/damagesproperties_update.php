 <form class="row g-3 was-validated" id="damages_properties_form_update" novalidate>

        <div class="col-md-4">
            <label for="firstName" class="form-label">damageName</label>
            <input type="text" class="form-control" id="damageName" name="damageName" value="" required>
            <input type="hidden" class="form-control" id="id" name="id" value="" required>
            <div class="valid-feedback">
                propertyName Data Entry Ok !
            </div>
            <div class="invalid-feedback">
                propertyName provide a firstName
            </div>
        </div>

        <div class="col-md-4">
            <label for="damageDescription" class="form-label">damageDescription</label>
            <input type="text" class="form-control" id="damageDescription" name="damageDescription" value="" >
        </div>



    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
