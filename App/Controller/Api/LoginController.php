<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date: 2023/2/23
 * Time: 21:12
 */

namespace App\Controller\Api;

use App\Resources\UserResource;

class LoginController
{

    /**
     * @return void login action
     */
    public function login()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $userResource = new UserResource();
        $userResource->checkUserInfo($data);
    }
}
