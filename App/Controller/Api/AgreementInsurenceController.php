<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\AgreementInsurenceResource;

class AgreementInsurenceController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new AgreementInsurenceResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new AgreementInsurenceResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new AgreementInsurenceResource)->destroy($data);
    }
}
