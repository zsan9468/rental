<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalLocationResource;

class RentalLocationController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalLocationResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalLocationResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalLocationResource)->destroy($data);
    }
}
