<header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
      <a href="/web/index/index" class="logo d-flex align-items-center">
        <img src="" class="logo_imag" alt="">
        <span class="d-none d-lg-block lang appname" >Rental APP</span>


      </a>
        <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->
    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">
          <li>
              <select id="chanageLag" class="secontpassWarning" ">
                  <option value="en-US"  class="lang" key="language.en">English</option>
                  <option value="de-DE"  class="lang" key="language.de">German</option>
                  <option value="cn-CN"  class="lang" key="language.cn">simplified Chinese</option>
                  <option value="it-IT"  class="lang" key="language.it">it</option>
              </select>
          </li>
        <li class="nav-item dropdown pe-3">
          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <img src="" alt="Profile" class="rounded-circle profile_photo">
            <span class="d-none d-md-block dropdown-toggle ps-2 namesurname"></span>
          </a><!-- End Profile Iamge Icon --><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
             <h6 class="userType"></h6>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>


            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="/web/user/users_profile">
                <i class="bi bi-gear"></i>
                <span class="lang" key="header.account.setting">Account  Settings</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            <li>
              <a class="dropdown-item d-flex align-items-center" href="/web/login/login">
                <i class="bi bi-box-arrow-right"></i>
                <span  class="lang" key="header.sign.out">Sign Out</span>
              </a>
            </li>
          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->
      </ul>
    </nav><!-- End Icons Navigation -->
  </header><!-- End Header -->