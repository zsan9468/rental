<?php

namespace App\Controller\Api;

use App\Controller\ApiOutputController;
use App\Controller\BaseController;
use App\Lib\Base64ImageSaver;
use App\Lib\UploadFiles;
use App\Models\AppSettingModel;

class AppSettingController extends BaseController
{
    public function get()
    {
        $result = (new AppSettingModel)->getOne();
        ApiOutputController::ApiOutput($result, 200);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        $id = '';

        $rules = [
            'AppName' => ['required'],
            'OnPremise' => ['required', 'numeric', 'max:4'],
            'IP' => ['required', 'ip'],
        ];
        $messages = [
            'AppName.required' => 'The AppName field is required.',
            'OnPremise.required' => 'The OnPremise field is required.',
            'OnPremise.numeric' => 'The OnPremise field must be a number.',
            'OnPremise.max' => 'The OnPremise field can only be up to 4 characters.',
            'IP.required' => 'The ip field is required.',
            'IP.ip' => 'The ip field format is incorrect.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $result = $validator->validate();
        if ($result === true) {
            if (!empty($data['imageData'])) {
                $uploader = new UploadFiles();
                $imagePath = $uploader->uploadFilesBase64($data['imageData'], 'public/upload/img/damage/');
                $data['LogoPath'] = '/' . $imagePath;
            }
            if (!empty($data['faviconData'])) {
                $favIconuploader = new UploadFiles();
                $favIconPath = $favIconuploader->uploadFilesBase64($data['faviconData'], 'public/upload/img/web/');
                $data['faviconPath'] = '/' . $favIconPath;
            }
            unset($data['imageData'], $data['faviconData']);
            $resultBool = (new AppSettingModel)->postItem($data, $id);
            $resultCode = 200;
            if (!$resultBool) {
                $resultCode = 502;
            }
            $result = [];
        } else {
            $resultCode = 412;
        }
        ApiOutputController::ApiOutput($result, $resultCode);
    }

    public function delete()
    {
        $code = 200;
        $data = $this->getQueryJson();
        $id = '';

        if (isset($data['id'])) {
            $id = $data['id'];
            $result = (new AppSettingModel)->postItem($data, $id);
            if (!$result) {
                $code = 502;
            }
        } else {
            $code = 501;
        }
        ApiOutputController::ApiOutput([], $code);
    }
}
