<?php

namespace App\Models;

use App\Lib\DB;

class AppSettingModel extends BaseModel
{
    protected $table = 'appsettings';

    public function getOne()
    {
        $returnArray = DB::link()->table($this->table)->limit(1)->get();
        return $returnArray;
    }

    public function postItem($data, $id)
    {
        return $this->basePostItem($this->table, $data, $id);
    }

    public function deleteItem($id)
    {
        return $this->baseDeleteItem($this->table, $id);
    }
}
