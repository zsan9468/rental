<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\RentalStatusTypModel;

class RentalStatusTypResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new RentalStatusTypModel;
        $result = $this->baseIndex($newModel, $params);
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'type' => ['required'],
        ];
        $messages = [
            'type.required' => 'The type field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new RentalStatusTypModel;
        $resultCode = $newModel->postItem($data);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new RentalStatusTypModel;
            $resultCode = $newModel->deleteItem($id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
