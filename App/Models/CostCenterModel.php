<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/24
 * Time:  13:56
 */

namespace App\Models;

use App\Lib\DB;

class CostCenterModel
{
    /**
     * @var string Define table name
     */
    protected $table = 'costcenter';
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }
    /**
     * @param $data receiving parameter Array
     * @return bool
     */
    public function create($data)
    {
        return DB::link()->table($this->table)->insert($data);
    }

    public function getCostCenterList($start = 0, $length = 0, $search = '', $order = '')
    {
        $returnArray = [];
        //Return all information
        if ($length == 0) {
            //            $result = DB::link()->table($this->table)->get();
            $sql = "select * from costcenter";
            $stm = $this->DB->query($sql);
            $result1 = $stm->fetchAll();
            //            var_dump($result1,$result);exit;
            return  $result1;
        }

        if ($search == '') {
            $result = DB::link()->table($this->table)->order("'$order[0] $order[1]'")->limit($start, $length)->get();
            $toTal = DB::link()->table($this->table)->order("'$order[0] $order[1]'")->count();
            $returnArray['recordsFiltered'] = (int)$toTal;
            $returnArray['data'] = $result;
            $returnArray['recordsTotal'] = count($result);
        } else {
            $whereSql = vsprintf("name REGEXP '%s' or city REGEXP '%s' or postalCode REGEXP '%s'", array($search, $search, $search));
            $result = DB::link()->table($this->table)->where($whereSql)->order("'$order[0] $order[1]'")->limit($start, $length)->get();
            $toTal = DB::link()->table($this->table)->where($whereSql)->order("'$order[0] $order[1]'")->count();
            $returnArray['recordsFiltered'] = (int)$toTal;
            $returnArray['data'] = $result;
            $returnArray['recordsTotal'] = count($result);
        }
        return  $returnArray;
    }

    public function update($data)
    {
        return DB::link()->table($this->table)->where("id = " . $data['id'])->update($data);
    }

    public function delete($id)
    {
        return DB::link()->table($this->table)->where("id = " . $id)->delete();
    }
}
