<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\CustomerPropertyModel;

class CustomerPropertyResource extends BaseResource
{
    public function index($params)
    {
        if (empty($params)) {
            $result['data'] = '';
            $result['code'] = 501;
        } else {
            $start = $params['start'];
            $length = $params['length'];
            $search = $params['search']['value'];

            $field = $params['columns'][$params['order'][0]['column']]['data'];
            $collation  = $params['order'][0]['dir'];
            $order = "$field $collation";

            $whereSql = '';

            $newModel = new CustomerPropertyModel;
            $result['data'] = $newModel->getIndex($start, $length, $search, $order, $whereSql);
            $result['data']['draw'] = $params['draw'];
            $result['code'] = 200;
        }
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data, $id = null)
    {
        $rules = [
            'propertyName' => ['required'],
        ];
        $messages = [
            'propertyName.required' => 'The propertyName field is required.',
        ];
        $validator = new \App\Lib\Validator($data, $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $newModel = new CustomerPropertyModel;
        $resultCode = $this->baseUpdate($newModel, $data, $id);
        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($data)
    {

        if (isset($data['id'])) {
            $id = $data['id'];
            $newModel = new CustomerPropertyModel;
            $resultCode = $this->baseDestroy($newModel, $id);
        } else {
            $resultCode = 503;
        }
        ApiOutputController::ApiOutput([], $resultCode);
    }
}
