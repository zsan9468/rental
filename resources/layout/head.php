<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Rental APP</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <!-- Favicons -->
    <link href="/resources/assets/img/favicon.png" class="icon" rel="icon">
<!--    <link href="/resources/assets/img/apple-touch-icon.png"  class="icon" rel="apple-touch-icon">-->

    <!-- Vendor CSS Files -->
    <link href="/resources/assets/vendor/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/resources/assets/vendor/jquery/jquery.dataTables.css"  rel="stylesheet">
    <link href="/resources/assets/vendor/jquery/jquery.ui.css"  rel="stylesheet">
    <link href="/resources/assets/vendor/bootstrap-icons/fonts/bootstrap-icons.woff" rel="stylesheet">
    <link href="/resources/assets/vendor/bootstrap-icons/fonts/bootstrap-icons.woff2" rel="stylesheet">
    <link href="/resources/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/assets/vendor/bootstrap/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/resources/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="/resources/assets/css/style.css" rel="stylesheet">

  </head>