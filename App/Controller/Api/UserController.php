<?php

/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date: 2023/2/23
 * Time: 6:15
 */

namespace App\Controller\Api;


use App\Controller\ApiOutputController;
use App\Controller\BaseController;
use App\Resources\CostCenterResource;
use App\Resources\UserResource;

class UserController extends BaseController
{
    /**
     * @return void
     * 添加修改用户时需要获取的其他的一些属性,如权限集合,costcerter
     * Add other attributes that need to be obtained when modifying the user, such as permission set,costcerter
     */
    public function getAddUserCorrelation()
    {
        $userResoure = new UserResource();
        $result = $userResoure->getUserCorrelation();
        ApiOutputController::ApiOutput($result, 200, '');
    }

    /**
     * @return void
     *  对用户的操作集合:GET-获取,POST-添加,PUT-修改,DELETE-删除
     *  Collection of user operations: GET-get, POST-add, PUT-modify, DELETE-delete
     */
    public function userInfoAction()
    {
        $data = $this->getQueryJson();
        $userReRe = new UserResource();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $data = $this->getQueryStringParams();
            $userReRe->show($data);
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userReRe->store($data);
        }

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $userReRe->update($data);
        }
        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $userReRe->destroy($data);
        }
    }

    /**
     * @return void
     * 获得用户列表
     * Get user list
     */
    public function getUserAction()
    {
        $data = $this->getQueryStringParams();
        $userRe = new UserResource();
        $resultArr = $userRe->index($data);
        ApiOutputController::ApiOutput($resultArr, 200, '');
    }


    /**
     * @return void
     * 修改密码
     * change password
     */
    public function chanagePassword()
    {
        $data = $this->getQueryJson();
        $userRe = new UserResource();
        $resultArr = $userRe->chanagePassword($data);
    }
}
