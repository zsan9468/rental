<!--  Add General Content -->
<div class="modal fade" id="gereral_form_add" tabindex="-1">
    <div class="modal-dialog modal-dialog-scrollable  modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title lang"  key="rentalunits.create.general">Create New
                    General Content</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                            <form class="row g-3 was-validated" id="gereral_form_create" novalidate >
                                <div class="col-md-5">
                                    <label for="Greeting" class="form-label">Greeting</label>
                                    <input type="text" class="form-control"  name="greeting" required>
                                </div>
                                <div class="col-md-5">
                                    <label for="VATrate" class="form-label">VATrate</label>
                                    <input type="number" class="form-control" name="VATrate" >
                                </div>

                                <div class="col-md-8">
                                    <label for="invoiceText1" class="form-label">invoiceText1</label>
                                    <input type="text" class="form-control" name="invoiceText1">
                                </div>
                                <div class="col-md-8">
                                    <label for="invoiceText2" class="form-label">invoiceText2</label>
                                    <input type="text" class="form-control" name="invoiceText2"
                                           placeholder="Please Enter invoiceText2" >
                                </div>

                                <div class="col-md-8">
                                    <label for="invoiceText3" class="form-label">invoiceText3</label>
                                    <input type="text" class="form-control" name="invoiceText3"
                                    >
                                </div>

                                <div class="col-md-4">
                                    <label for="paymentWarning1" class="form-label">paymentWarning1</label>
                                    <input type="text" class="form-control" name="paymentWarning1"
                                           placeholder="Please Enter paymentWarning1" >
                                </div>
                                <div class="col-md-4">
                                    <label for="paymentWarning2" class="form-label">paymentWarning2</label>
                                    <input type="text" class="form-control" name="VATrate" placeholder="" >
                                </div>

                                <div class="col-md-4">
                                    <label for="paymentWarning3" class="form-label">paymentWarning3</label>
                                    <input type="text" class="form-control" name="paymentWarning3"
                                           placeholder="" >
                                </div>

                                <div class="col-md-4">
                                    <label for="offer1" class="form-label">offer1</label>
                                    <input type="text" class="form-control" name="offer1" placeholder="P" >
                                </div>

                                <div class="col-md-4">
                                    <label for="offer2" class="form-label">offer2</label>
                                    <input type="text" class="form-control" name="offer2" placeholder="" >
                                </div>

                                <div class="col-md-4">
                                    <label for="offer3" class="form-label">offer3</label>
                                    <input type="text" class="form-control" name="offer3" placeholder="">
                                </div>

                                <div class="row col-md-8" style="margin-top: 1rem">
                                    <label class="col-md-6 col-form-label lang" key="general.time.format">Time display format</label>
                                    <div class="col-sm-6">
                                        <select id="date_format" name="date_format" class="form-select">
                                            <option value="1">YYYY-MM-DD</option>
                                            <option value="2">MM/DD/YYYY</option>
                                            <option value="3">DD-MM-YYYY </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary  lang" key="close" data-bs-dismiss="modal">Close</button>
                                <button type="reset" class="btn btn-secondary lang" key="reset">Reset</button>
                                <button type="submit" class="btn btn-primary lang" key="submit">Submit</button>
                            </div>
                        </form><!-- End No Labels Form -->

                    </div>
                </div>

            </div>

        </div>
    </div>
</div><!-- End Modal Dialog Scrollable-->
