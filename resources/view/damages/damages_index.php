<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title lang" key="rentalunits.damages.title">Damages Damages   <span class="lang" key="list.title"> LIST</span></h5>
                                <div class="card">
                                </div>
                                <table id="damages_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Edit General Content -->
        <div class="modal fade" id="damages_update_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.damages">Create New
                            Unitnames</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'damages_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="damages_drawing_model" tabindex="-1" style="z-index: 9999;">
            <div class="modal-dialog modal-dialog-scrollable modal-lg"  style="max-width: 1600px">
                <div class="modal-content"  style="max-width: 1600px">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalUnits.create.rentalunits.damage">在线画图</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include '../rental/drawing.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {
        $("#logoPath").change(function () {
            $('.damage_imag').attr('src','');
            $.getImageBase(this, 'damage_imag', 'damageImgPath')
        });
        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'rentalUnitsId', title: "rentalUnitsId", name: "rentalUnitsId" },
            { data: 'unit_name', title: "unit_name", name: "unit_name" },
            { data: 'damageName', title: "damageName", name: "damageName" },
            { data: 'typName', title: "typName", name: "typName" },
            { data: 'damageTime', title: "damageTime", name: "damageTime" ,render:(data,type,row)=>{
                    var dateInfo  =  dateFormaChanget(data)
                    return dateInfo;
                }
            },
            { data: 'repairDateTime', title: "repairDateTime", className :'dateChange' , name: "repairDateTime",render:(data,type,row)=>{
                   var dateInfo  =  dateFormaChanget(data)
                    return dateInfo;
              }
            },
            { data: 'damageStatus', title: "damageStatus", name: "damageStatus" ,"orderable" : false  ,render: (data, type, row) => {

                    if(row.damageStatus == 1 ){
                        return " <text class='text-info'>in maintenance</text>";
                    }
                    if(row.damageStatus == 2 ){
                        return " <text class='text-success'>already repaired</text>";
                    }
                }
            },
            { data: 'location', title: "location", name: "location" },
            { data: 'number', title: "number", name: "number" },
            { data: 'Action', title: "Action", name: "propertieName", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#damages_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#damages_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('#damages_update_model').modal('show');
                        $.GetDamage('Damage_form_update');
                        var responses = formAjaxHy('/api/rental_damage?id='+row.id, data, 'get');
                        var damage_propertieCurl = formAjaxHy('/api/rental_damage_properties', data, 'get');
                        console.log(row.id,row,row.repairDateTime)
                        var d2=new Date(row.repairDateTime)
                        $("#Damage_form_update input[name=id]").val(row.id);
                        $("#Damage_form_update #damageProperties").val(row.damagepropertiesId);
                        $("#Damage_form_update input[name=rentalUnitsId]").val(row.rentalUnitsId);
                        $("#Damage_form_update input[name=damageTime]").val(row.damageTime);
                        $("#Damage_form_update input[name=repairDateTime]").val(row.repairDateTime);
                        $("#Damage_form_update #damageStatus").val(row.damageStatus);
                        $(".damage_imag").attr('src',row.damageImgPath);
                    })

                    $('#damages_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#damages_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_damage');
                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_damage', 'get', 'damages_table', columns,);

        $('#customer_properties_add').click(function (e) {
            $('#rentalproperties_add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#Damage_form_update').submit(function (e) {
            var data = $.rental_damage_Biaodan('Damage_form_update')
            var id = $("#Damage_form_update input[name=id]").val();
            data.id = id ;
            if (e.isDefaultPrevented()) {
                return false;
            }
            e.preventDefault()
            console.log(11111)
            $.ajaxUnit('/api/rental_damage', 'put', data,
                function (data) {
                    if (data.payload.code == 200) {
                        $('#damages_update_model').modal('hide');
                        commonUtil.message('SUCCESSS!')
                        table.ajax.reload();
                    } else {
                        commonUtil.message(data.payload.data);
                    }
                }, function (data) {
                    console.log(data)
                })
        })

    });

</script>
</body>

</html>