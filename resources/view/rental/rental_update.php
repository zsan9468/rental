<form class="row g-3 was-validated" id="rental_form_update" novalidate>
    <input type="hidden" class="form-control" id="id" name="id" value="" required >
    <input type="hidden" class="form-control" id="unittyp_id" name="unittyp_id" value="" required >


    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label">UnitNames</label>
        <div class="col-sm-8">
            <select id="unitNamesId" name="unitNamesId" class="form-select">
            </select>
        </div>
    </div>
    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label">statusTyp</label>
        <div class="col-sm-8">
            <select id="statusTypId" name="statusTypId" class="form-select">
            </select>
        </div>
    </div>
    <div class="row col-md-8" style="margin-top: 1rem">
        <label class="col-md-4 col-form-label">unitLocation</label>
        <div class="col-sm-8">
            <select id="unitLocationPropertiesId" name="unitLocationPropertiesId" class="form-select">
            </select>
        </div>
    </div>


    <div class="col-md-6">
        <label for="typName" class="form-label">Type Name</label>
        <input type="text" class="form-control" id="typName" name="typName" value="" required >
        <div class="valid-feedback">
            Type Name Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Type Nam provide a lastName
        </div>
    </div>

    <div class="col-md-12">
        <label for="typDescription" class="form-label">typDescription</label>
        <textarea type="text" class="form-control" name ="typDescription" id="typDescription"  value="" ></textarea>
    </div>

    <div class="col-md-12">
        <label for="" class="form-label"> <text class="text-primary" > RentalProperties</text></label>
        <div class="Rental_properties">

        </div>

    </div>


    <div class="col-md-5">
        <label for="priceGroup" class="form-label">priceGroup</label>
        <input type="text" class="form-control" id="priceGroup" name="priceGroup" value=""   required>
        <div class="valid-feedback">
            priceGroup Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a priceGroup
        </div>
    </div>

    <div class="col-md-4">
        <label for="typName" class="form-label">Price</label>
        <input type="text" class="form-control" id="price" name="price" value=""   required>
        <div class="valid-feedback">
            Price Data Entry Ok !
        </div>
        <div class="invalid-feedback">
            Please provide a Price
        </div>
    </div>


    <div class="col-12">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </div>
</form><!-- End Custom Styled Validation -->
