<?php

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Models\RentalModel;

class RentalResource extends BaseResource
{
    public function index($params)
    {
        $newModel = new RentalModel;
        // Reduce the degree of coupling and return the status code uniformly
        $code = 200;

        if (empty($params)) {
            $result['data'] = '';
            $result['code'] = 501;
        } else {
            if (isset($params['id'])) {
                // GetOne
                $result['data'] = $newModel->getOne($params['id']);
            } else {
                // GetAll
                $start = $params['start'];
                $length = $params['length'];
                $search = $params['search']['value'];

                $field = $params['columns'][$params['order'][0]['column']]['data'];
                switch ($field) {
                    case 'id':
                        $field = "r.`id`";
                        break;
                    default:
                        $field = "r.`id`";
                        break;
                }

                $collation  = $params['order'][0]['dir'];
                $order = "$field $collation";

                $result['data'] = $newModel->getIndex($start, $length, $search, $order);
                $result['data']['draw'] = (int)$params['draw'];
            }
            $result['code'] = $code;
        }

        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function update($data)
    {
        $rules = [
            'unitNamesId' => ['required'],
            'unitLocationPropertiesId' => ['required'],
            'typName' => ['required'],
            'statusTypId' => ['required'],
            'price' => ['required', 'numeric'],
        ];
        $messages = [
            'unitNamesId.required' => 'The unitNamesId field is required.',
            'unitLocationPropertiesId.required' => 'The unitLocationPropertiesId field is required.',
            'typName.required' => 'The typName field is required.',
            'statusTypId.required' => 'The statusTypId field is required.',
            'price.required' => 'The price field is required.',
            'price.numeric' => 'The price field must be a number.',
        ];
        if (isset($data['rental']['id'])) {
            $rules['unitTypId'] = ['required'];
            $messages['unitTypId.required'] = ['The unitTypId field is required.'];
        }
        $validator = new \App\Lib\Validator($data['rental'], $rules, $messages);
        $resultValidated = $validator->validate();
        if ($resultValidated !== true) {
            ApiOutputController::ApiOutput($resultValidated, 412);
        }

        $data['typ']['typName'] = $data['rental']['typName'];
        $data['typ']['description'] = $data['rental']['typDescription'];
        $data['price']['price'] = $data['rental']['price'];
        $data['price']['priceGroup'] = $data['rental']['priceGroup'];
        unset($data['rental']['typName'], $data['rental']['typDescription'], $data['rental']['price'], $data['rental']['priceGroup']);
        $newModel = new RentalModel;
        $resultCode = $this->baseUpdate($newModel, $data);

        ApiOutputController::ApiOutput([], $resultCode);
    }

    public function destroy($id)
    {
        $newModel = new RentalModel;
        $result = $this->baseDestroy($newModel, $id);
        ApiOutputController::ApiOutput([], $result);
    }

    public function propertiesList()
    {
        $newModel = new RentalModel;
        $result = $newModel->getPropertiesList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }

    public function propertiesSelectionList()
    {
        $newModel = new RentalModel;
        $result = $newModel->getPropertiesSelectionList();
        ApiOutputController::ApiOutput($result['data'], $result['code']);
    }
}
