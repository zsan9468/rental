<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title "><text class="lang" key="rentalUnits.Statustyp_title">Statustyp</text>  <span class="lang" key="list.title"> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary lang" id="statustyp_add"  key="rentalUnits.Create Statustyp">
                                            Create Statustyp
                                        </button>
                                    </div>
                                </div>
<!--                                <table id="statustyp_table" class="hover table-striped" style="width:100%">-->
                                <table id="statustyp_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="statustyp__add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.create.statustyp">Create New
                            Statustyp</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'statustyp_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="statustyp_update" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalunits.update.statustyp">Update New
                            Statustyp</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'statustyp_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'type', title: "type", name: "type" },
            { data: 'description', title: "description", name: "description" },
            { data: 'Action', title: "Action", name: "description", "orderable" : false,render: (data, type, row) => {

                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    console.log(row)
                    $('#statustyp_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#statustyp_table').on('click', 'tbody #' + upodateBtn, function (e) {

                        $('#statustyp_update').modal('show');
                        $("#statustyp_form_update input[name=id]").val(row.id);
                        $("#statustyp_form_update input[name=description]").val(row.description);
                        $("#statustyp_form_update input[name=type]").val(row.type);
                    })

                    $('#statustyp_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#statustyp_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/rental_status');

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/rental_status', 'get', 'statustyp_table', columns,);

        $('#statustyp_add').click(function (e) {
            $('#statustyp__add_model').modal('show');
        })

// Obtain information about adding customer information
        $('#statustyp_form_create').submit(function (e) {
            var data =  $.statustypBiaodan('statustyp_form_create',e,'create')
            console.log(data)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_status','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#statustyp__add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#statustyp_form_update').submit(function (e) {
            var id = $("#statustyp_form_update input[name=id]").val();
            var data =  $.statustypBiaodan('statustyp_form_update',e,'update')
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/rental_status','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#statustyp_update').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>