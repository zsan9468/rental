<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Lib\UploadFiles;
use App\Resources\RentalDamageResource;

class RentalDamageController extends BaseController
{
    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalDamageResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalDamageResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        (new RentalDamageResource)->destroy($data);
    }
}
