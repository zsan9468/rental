<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Resources\RentalResource;

class RentalController extends BaseController
{
    // Get all Rental module property options
    public function create()
    {
        (new RentalResource)->propertiesList();
    }

    // Get Rental Property Options
    public function createselection()
    {
        (new RentalResource)->propertiesSelectionList();
    }

    public function get()
    {
        $data = $this->getQueryStringParams();
        (new RentalResource)->index($data);
    }

    public function post()
    {
        $data = $this->getQueryJson();
        (new RentalResource)->update($data);
    }

    public function delete()
    {
        $data = $this->getQueryJson();
        $id = $data['id'];
        (new RentalResource)->destroy($id);
    }
}
