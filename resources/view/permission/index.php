<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <?php include 'permission_update.php' ?>
                <?php include 'permission_add.php' ?>
                <div class="card-body" >
                    <h5 class="card-title lang"  key="permission.management">Permission Management</h5>
                    <div class="card " >
                            <div class="card-body">
                                <p></p>
                                <button type="button" class="btn btn-primary lang"  key="permission_create" id="permission_create_btn">
                                    Create Permission
                                </button>
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Permission list</h5>
                            <!-- Small tables -->
                            <table class="table table-sm ">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">menu_name</th>
                                    <th scope="col">menu_Id</th>
                                    <th scope="col">menu_url</th>
                                    <th scope="col">method</th>
                                    <th scope="col">menu_order</th>
                                    <th scope="col">Update</th>
                                </tr>
                                </thead>
                                <tbody class="permission">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    var permissionGlobal ;
    $(document).ready(function () {
        $.ajaxUnit('/api/permission','get', {},
            function (data){
                if(data.payload.code == 200){
                    var permissionDatas = [];
                    var perdata = data.payload.data;
                    permissionGlobal = perdata;
                    console.log(perdata)
                    for (var j = 0, perlen = perdata.length; j < perlen; j++) {
                        var peritemdata = perdata[j];
                        permissionDatas.push("<tr class='father_menu'>"+
                            "<th scope='row'>"+peritemdata.info.id+"</th>"+
                        "<th >"+peritemdata.info.menu_name+"</th>"+
                        "<th>"+peritemdata.info.menu_icon+"</th>"+
                        "<th>"+peritemdata.info.menu_url+"</th>"+
                        " <th>"+peritemdata.info.method+"</th>"+
                        " <th>"+peritemdata.info.menu_order+"</th>"+
                        " <th onclick='updateper("+peritemdata.info.id+","+ j +")'><button class='btn  btn-sm btn-outline-secondary  '>Update</button></th>"+
                        " <th onclick='deletepermission("+peritemdata.info.id+")'><button class='btn  btn-sm btn-outline-danger  '>Delete</button></th>"+
                        "</tr>")
                        if (peritemdata.son != '') {
                            var sonArr = [];
                            var sonDatas = peritemdata.son;
                            permissionDatas.push(" <div class='row md-12'>")
                            for (var z = 0, sonlenth = sonDatas.length; z < sonlenth; z++) {
                                var sonPerData = sonDatas[z];
                                if(sonPerData.info.type == 1 ){
                                    permissionDatas.push(" <tr class='cmenu'> <td>&nbsp;&nbsp;&nbsp;"+sonPerData.info.id+"</td>")
                                }else{
                                    permissionDatas.push("<tr class='actionper '><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+sonPerData.info.id+"</td>")
                                }
                                permissionDatas.push("<td >"+sonPerData.info.menu_name+"</th>"+
                                "<td>"+sonPerData.info.menu_icon+"</td>"+
                                "<td>"+sonPerData.info.menu_url+"</td>"+
                                " <td>"+sonPerData.info.method+"</td>"+
                                " <td>"+sonPerData.info.menu_order+"</td>"+
                                " <td onclick='updateper("+sonPerData.info.id+")'><button class='btn  btn-sm btn-outline-secondary  '>Update</button></td>"+
                                " <td onclick='deletepermission("+sonPerData.info.id+")'><button class='btn  btn-sm btn-outline-danger  '>Delete</button></td>"+
                                "</tr>")
                            }
                        }
                        permissionDatas.push("<span class='badge bg-primary rounded-pill'>14</span></li>")
                    }
                    $('.permission').append(permissionDatas.join(' '))
                }else{
                     commonUtil.message(data.payload.data);
                }
            },
            function (data){}
        )


        /**
         *权限选择时动态调整可输入框- The input box is dynamically adjusted when permission is selected
         */

        $(".father_id").change(function(e) {
            console.log($(this).val())
            if($(this).val() == 0){
                $(".method").val('0');
                $(".method").attr('disabled',"disabled");
                $(".menu_url").val('');
                $(".menu_url").attr('disabled',"disabled");
                $(".type").val(1);
                $(".type").attr('disabled',"disabled");
                $(".menu_icon").removeAttr('disabled');
            }else{
                $(".menu_icon").val("");
                $(".menu_icon").attr('disabled',"disabled");
                $(".menu_url").removeAttr('disabled');

                $(".type").removeAttr("disabled");
                if( $(".type").val() == 1)
                {
                    $(".method").val('0');
                    $(".method").attr('disabled',"disabled");
                }else{
                    $(".method").removeAttr("disabled");
                }
            }
        });

        $(".type").change(function(e) {
            if(($(this).val() == 1)){
                $(".method").val('0');
                $(".method").attr('disabled',"disabled");
            }else{
                $(".method").val('POST');
                $(".method").removeAttr("disabled");
            }
        })



        /**
         * 点击创建权限初始化一些权限选择信息-Click Create Permission to initialize some permission selection information
         * 获取权限顶级信息-Permission_top_list(PARA_N1 ，PARA_N2)
         */
        $('#permission_create_btn').click(function () {
             $("#permission_create .father_id").html("")
            $.Permission_top_list(permissionGlobal,"#permission_create #father_id")
            //初始化操作-Initialize the operation
            if( $("#permission_create .father_id").val()==0){
                $("#permission_create .method").val('0');
                $("#permission_create .method").attr('disabled',"disabled");
                $("#permission_create .menu_url").val('');
                $("#permission_create .menu_url").attr('disabled',"disabled");
                $("#permission_create .type").val(1);
                $("#permission_create .type").attr('disabled',"disabled");
                $("#permission_create .menu_icon").removeAttr('disabled');
            }
            $("#create_permission_model").modal('show')
        })
        /**
         * 创建一个权限 Create a permission
         * permission_create 表单ID
         * Permission_data_biaodan() 处理权限表单信息 Process permission form information
         * isDefaultPrevented 阻止表单提交 Blocking form submission
         */
        $('#permission_create').submit(function (e) {
            var data =  $.Permission_data_biaodan('permission_create',e)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/permission','post',data,
                function(data){
                    if(data.payload.code == 200){
                        commonUtil.message('Update success!')
                        $('#update_permission_model').modal('hide');
                        window.location.reload();
                    }else{
                        commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        /**
         * 创建一个权限 Update a permission
         * update_permission 表单ID
         * Permission_data_biaodan() 处理权限表单信息 Process permission form information
         * isDefaultPrevented 阻止表单提交 Blocking form submission
         */
        $('#update_permission').submit(function (e) {
            var data =  $.Permission_data_biaodan('update_permission')
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/permission','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success!')
                        $('#update_permission_model').modal('hide');
                        window.location.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })
        })
    });
    function updateper(e,a){
        $("#father_id").html('');
        $('#update_permission_model').modal('show')
        $.ajaxUnit('/api/permission','get', {id:e},function (data){
            if(data.payload.code == 200)
            {
                var perdata = data.payload.data[0]
                console.log(perdata)
                $("#update_permission input[name=menu_name]").val(perdata.menu_name);
                $("#update_permission input[name=menu_icon]").val(perdata.menu_icon);
                $("#update_permission input[name=menu_url]").val(perdata.menu_url);
                $("#update_permission input[name=menu_order]").val(perdata.menu_order);
                $("#update_permission input[name=id]").val(perdata.id);
                $("#method").val(perdata.method);
                $("#father_id").val(perdata.father_id);
                $("#type").val(perdata.type);
                if( $("#update_permission .father_id").val()==0){
                    $("#update_permission .method").val('0');
                    $("#update_permission .method").attr('disabled',"disabled");
                    $("#update_permission .menu_url").val('');
                    $("#update_permission .menu_url").attr('disabled',"disabled");
                    $("#update_permission .type").val(1);
                    $("#update_permission .type").attr('disabled',"disabled");
                    $("#update_permission .menu_icon").removeAttr('disabled');
                }else{
                    if( $("#update_permission .type").val()==1){
                        $("#update_permission .menu_icon").attr('disabled',"disabled");
                        $("#update_permission .method").attr('disabled',"disabled");
                    }
                }
            }
        },function (){})
        $.Permission_top_list(permissionGlobal,"#update_permission #father_id")
    }
    function deletepermission(e){
        var datadelete =  {id:e}
            var responses = formAjaxHy('/api/permission',datadelete, 'DELETE');
            console.log(responses)
            if(responses.payload.code == 200)
            {
                commonUtil.message('Delete success!')
                window.location.reload();
            }else{
                commonUtil.message(responses.payload.data)
            }
    }



</script>
</body>

</html>