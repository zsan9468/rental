<?php

namespace App\Models;

use App\Lib\DB;
use App\Lib\FileLogger;
use PDOException;

class CustomerModel extends BaseModel
{
    protected $table = 'customer';
    protected $tablePropertySelection = 'customerpropertyselection';
    protected $tableProperty = 'customerproperties';

    public function getIndex($start, $length, $search, $order)
    {
        $whereSql = '';
        if (!$search == '') {
            $whereSql = vsprintf("firstName REGEXP '%s' or lastName REGEXP '%s'", array($search, $search));
        }
        return $this->baseGetIndex($this->table, $start, $length, $search, $order, $whereSql);
    }

    public function getOne($id)
    {
        $returnArray['customer'] = $this->getSingle($this->table, $id);
        $result['selection'] = $this->getSingle($this->tablePropertySelection, $id, 'costumerId');
        $result['properties'] = DB::link()->table($this->tableProperty)->get();
        // First, return all the Properties for the front-end Create;
        // If the object has a corresponding property, fill in the value.
        $returnArray['customerproperties'] = [];
        $sortData = [];
        foreach ($result['properties'] as $v) {
            $sortData['customerPropertiesId'] = $v['id'];
            $sortData['costumerPropertyName'] = $v['propertyName'];
            $sortData['propertyDescription'] = $v['propertyDescription'];
            $sortData['selectionId'] = '';
            $sortData['customerPropertyValue'] = '';
            foreach ($result['selection'] as $vv) {
                if ($vv['customerPropertiesId'] == $v['id']) {
                    $sortData['selectionId'] = $vv['id'];
                    $sortData['customerPropertyValue'] = $vv['customerPropertyValue'];
                }
            }
            $returnArray['customerproperties'][] = $sortData;
        }
        return $returnArray;
    }

    public function postItem($data)
    {
        try {
            // Begin the transaction
            DB::link()->db->beginTransaction();

            if (isset($data['customer']['id'])) {
                // Update
                $id = $data['customer']['id'];
                unset($data['customer']['id']);
                // update the main data
                DB::link()->table($this->table)->where("id = $id")->update($data['customer']);

                // Insert the additional information data
                if (!empty($data['customerproperties'])) {
                    foreach ($data['customerproperties'] as $v) {
                        if (!empty($v['customerPropertyValue'])) {
                            // If there is a value
                            // it is determined whether to add or update according to the selectionId
                            if (isset($v['selectionId'])) {
                                // update
                                $propertiesId = $v['selectionId'];
                                unset($v['selectionId']);
                                DB::link()->table($this->tablePropertySelection)->where("id = $propertiesId")->update($v);
                            } else {
                                // add
                                $v['costumerId'] = $id;
                                DB::link()->table($this->tablePropertySelection)->insert($v);
                            }
                        } else {
                            // Value is empty, delete or do nothing
                            if (isset($v['selectionId'])) {
                                // delete
                                $propertiesId = $v['selectionId'];
                                DB::link()->table($this->tablePropertySelection)->where("id = $propertiesId")->delete();
                            }
                        }
                    }
                }
            } else {
                // Insert
                // Insert the main data
                DB::link()->table($this->table)->insert($data['customer']);
                // Get the ID of the newly inserted main record
                $costumerId = DB::link()->db->lastInsertId();

                // Insert the additional information data
                if (!empty($data['customerproperties'])) {
                    foreach ($data['customerproperties'] as $v) {
                        if (!empty($v['customerPropertyValue'])) {
                            $v['costumerId'] = $costumerId;
                            DB::link()->table($this->tablePropertySelection)->insert($v);
                        }
                    }
                }
            }

            // Commit the transaction
            DB::link()->db->commit();

            $ReturnBool = true;
        } catch (PDOException $e) {
            // Roll back the transaction on error
            DB::link()->db->rollBack();

            // Log the error
            FileLogger::error('Error inserting data: ' . $e->getMessage());

            // Return failure
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function deleteItem($id)
    {
        try {
            DB::link()->db->beginTransaction();
            DB::link()->table($this->tablePropertySelection)->where("costumerId = $id")->delete();
            DB::link()->table($this->table)->where("id = $id")->delete();
            DB::link()->db->commit();
            $ReturnBool = true;
        } catch (PDOException $e) {
            DB::link()->db->rollBack();
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            $ReturnBool = false;
        }
        return $ReturnBool;
    }

    public function getPropertiesList()
    {
        try {
            $returnArray['data'] = DB::link()->table($this->tableProperty)->get();
            $returnArray['code'] = 200;
        } catch (PDOException $e) {
            FileLogger::error('Error inserting data: ' . $e->getMessage());
            $returnArray['data'] = '';
            $returnArray['code'] = 502;
        }
        return $returnArray;
    }
}
