<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">EMPLOYEE USER <span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary" id="emplyee_user_view">
                                            Create Employee User
                                        </button>
                                    </div>
                                </div>
                                <table id="emplyee_user_tbale" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="emplyee_user_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 1200px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang" key="user.Create New Employee">Create New Employee</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <form class="row g-3 was-validated" id="emplyee_user_form" novalidate>
                                    <div class="col-md-8">
                                        <label for="name" class="form-label lang" key="user.username">UserName</label>
                                        <input type="text" class="form-control" id="username" name="username" value="" required>
                                        <div class="valid-feedback">
                                            UserName Data Entry Ok !
                                        </div>
                                        <div class="invalid-feedback">
                                            Please provide a UserName
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <label for="name" class="form-label">Password     </label>
                                        <i class="bi bi-eye-slash-fill" id="chageTypePass">      </i>
                                        <input type="password" class="form-control" id="password" name="password" value="" autocomplete
                                               required>
                                        <div class="valid-feedback">
                                            Password Data Entry Ok !
                                        </div>
                                        <div class="invalid-feedback">
                                            Please provide a Password（Minimum 6 Characters No Spaces）
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <label for="affirm_password" class="form-label"> Please enter the password a second time    </label>
                                       <input type="password" class="form-control" id="affirm_password" name="affirm_password" value=""  required>
                                        <p class="text-warning show lang  secontpassWarning" id="showpassWarning" >Password inconsistency</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="" >
                                    </div>
                                    <div class="col-md-6">
                                        <label for="name" class="form-label">Surname</label>
                                        <input type="text" class="form-control" id="surname" name="surname" value="">
                                    </div>
                                    <div class="col-md-8">
                                        <label for="name" class="form-label">Email</label>
                                        <i>@</i><input type="email" class="form-control" id="email" name="email" value="">
                                    </div>

                                    <div class="row col-md-8" style="margin-top: 1rem">
                                        <label class="col-md-4 col-form-label">User Type  </label>
                                        <div class="col-sm-8">
                                            <select id="user_type" name="user_type" class="form-select">
                                                <option value='3'>Employee</option>
                                                <option value='2'>Admin</option>
                                                <option value='1' disabled>IT</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12 hy_create_permission">
                                        <label for="name" class="form-label"><b>Permission Choose</b></label>
                                        <!--                                        menu-->
                                        <div class="row md-12 menu ">

                                        </div>
                                    </div>

                                    <div class="row col-md-8" style="margin-top: 1rem">
                                        <label class="col-md-3 col-form-label">CostCenter</label>
                                        <div class="col-sm-9">
                                            <select id="cost_center" name="cost_center" class="form-select">

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button class="btn btn-primary" type="submit">Submit form</button>
                                    </div>
                                </form><!-- End Custom Styled Validation -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->

        <?php include 'update.php' ?>
    </section>

</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>

    $(document).ready(function () {
        var pwd = $("#password");
        var eye = $("#chageTypePass");
        $("#chageTypePass").click(function (){
            if (pwd.attr("type") == "text") {
                pwd.attr("type", "password");
                eye.attr("class", "bi-eye-slash-fill");
            } else {
                pwd.attr("type", "text");
                eye.attr("class", "bi-eye-slash");
            }
        })
//       Get user list
        var columns = [
            { data: 'userId', title: "userId", name: "userId" },
            { data: 'username', title: "username", name: "greeting" },
            { data: 'user_type', title: "user_type", defaultContent : " ", name: "user_type",'searchable': false ,render: (data, type, row) => {

                    if(row.user_type == 2 ){
                        return " <text class='text-info'>Admin</text>";
                    }
                    if(row.user_type == 3 ){
                        return " <text class='text-success'>Employee</text>";
                    }
                }
            },
            { data: 'city', title: "city", name: "city", "orderable" : false },
            { data: 'email', title: "email", name: "email" },
            { data: 'surname', title: "surname", name: "surname" },
            { data: 'costName', title: "costName", name: "costName" , "orderable" : false},
            { data: 'Action', title: "Action", name: "surname", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.userId;
                    var deleteBtn = "delete" + row.userId;
                    $('#emplyee_user_tbale').undelegate('tbody #' + upodateBtn, 'click');
                    $('#emplyee_user_tbale').on('click', 'tbody #' + upodateBtn, function (e) {
                        var responses = formAjaxHy('/api/get_user_info', value = '', 'get');
                        console.log(responses.payload.data.permissions)
                        get_user_correlation(responses,'update_menu','update_update_cost_center')
                        var userInfo = formAjaxHy('/api/user_action?id='+row.userId,'', 'get');
                        $('#update_emplyee_user_model').modal('show');
                        $("#update_emplyee_user_model input[name='username']").val(row.username)
                        $("#update_emplyee_user_model input[name='email']").val(row.email)
                        $("#update_emplyee_user_model input[name='surname']").val(row.surname)
                        $("#update_user_type").val(row.user_type)
                        $("#update_emplyee_user_model input[name='userId']").val(row.userId)
                        $("#update_emplyee_user_model input[name='name']").val(row.name)
                        $("#update_update_cost_center").val(row.costcerter_id)
                        $.each($('.update_menu').find("input[type='checkbox']"), function (index, domEle) {
                            for (var pi = 0; pi < userInfo.payload.data.permissions.length;pi++){
                                if(userInfo.payload.data.permissions[pi] == $(this).val() )
                                {
                                    $(this).prop("checked", true);
                                }
                            }
                        });
                    })

                    $('#emplyee_user_tbale').undelegate('tbody #' + deleteBtn, 'click');
                    $('#emplyee_user_tbale').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.userId);
                        $('#del_url').text('/api/user_action');

                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/get_users', 'get', 'emplyee_user_tbale', columns,);

// Add user information
        $('#emplyee_user_form').submit(function (e) {
            let selectedAr = $('.menu').find("input[type='checkbox']")
            var arrpermission = [];
            a = 0;
            $(selectedAr).each(function(i){
               if($(this).is(':checked')){
                   arrpermission[a] = $(this).val();
                    a++;
               };
            });
            var arrpermissions = arrpermission.join(",");

            var affirm_password = $('#emplyee_user_form input[name=affirm_password]').val()
            var set_password = $('#emplyee_user_form input[name=password]').val()
            if(set_password != affirm_password) {
                $("#showpassWarning").addClass('showpassWarning')
                return  false;
            }else{
                $("#showpassWarning").removeClass('showpassWarning')
            }
            console.log(111)

            let value = $('#emplyee_user_form').serializeArray();
            console.log(value)
            $.checkUSerFild(value,e,'create');
            console.log('TetsPAss')
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            var permission = {};
            value.push({'name':'permission','value':arrpermissions})
            var responses = formAjaxHy('/api/user_action', value, 'post');
            if (responses.payload.code == 200) {
                 commonUtil.message('Create Success！')
                $('#emplyee_user_model').modal('hide');
                table.ajax.reload();
            } else {
                 commonUtil.message(responses.payload.data)
            }
            return false;//Block page refresh
        })
// Obtain information about adding user information
        $('#emplyee_user_view').click(function () {
            var responses = formAjaxHy('/api/get_user_info', value = '', 'get');
            $('#emplyee_user_model').modal('show');
            if (responses.payload.data) {
                var data = responses.payload.data.costCerter;
                var items = [];
                for (var i = 0, len = data.length; i < len; i++) {
                    var itemdata = data[i];
                    items.push('<option value="' + itemdata.id + '">' + itemdata.name + '</option>')
                }
                $('#cost_center').html(items.join(' '))
                var permissionDatas = [];
                var perdata = responses.payload.data.permissions;
                for (var j = 0, perlen = perdata.length; j < perlen; j++) {
                    var peritemdata = perdata[j];
                    permissionDatas.push(" <div class='form-check col-md-12'>" +
                        "<input class='form-check-input  father' type='checkbox' name='permission[]' value='"+peritemdata.info.id+"'>" +
                        "<label class='form-check-label text-primary' for='gridCheck1'>" +peritemdata.info.menu_name+
                        "</label>")
                    if (peritemdata.son != '') {
                        var sonArr = [];
                        var sonDatas = peritemdata.son;
                        permissionDatas.push(" <div class='row md-12'>")
                        for (var z = 0, sonlenth = sonDatas.length; z < sonlenth; z++) {
                            var sonPerData = sonDatas[z];
                            permissionDatas.push("<div class='form-check col-md-3' >" +
                                "<input class='form-check-input son' type='checkbox' name='permission[]'  value='"+sonPerData.info.id+"'>" +
                                "<label class='form-check-label' for='gridCheck1'>" + sonPerData.info.menu_name +
                                "</label>" +
                                "</div>")
                        }
                        permissionDatas.push("</div>")
                    }
                    permissionDatas.push("</div>")
                }
                $('.menu').html(permissionDatas.join(' '))
            }
        })
        $('#update_emplyee_user_form').submit(function (e) {
            let selectedAr = $('.update_menu').find("input[type='checkbox']")
            var arrpermission = [];
            a = 0;
            $(selectedAr).each(function(i){
                if($(this).is(':checked')){
                    arrpermission[a] = $(this).val();
                    a++;
                };
            });
            var arrpermissions = arrpermission.join(",");
            let value = $('#update_emplyee_user_form').serializeArray();

            $.checkUSerFild(value,e,'update');
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            var permission = {};
            value.push({'name':'permission','value':arrpermissions})
            var responses = formAjaxHy('/api/user_action', value, 'put');
            if (responses.payload.code == 200) {
                 commonUtil.message('Update Success！')
                table.ajax.reload();
                $('#update_emplyee_user_model').modal('hide');
            } else {
                 commonUtil.message(responses.payload.data)
            }
        })
// Add click events to dynamically added DOM
        $(document).on('click', '.son', function(){
            var result = $(this).is(':checked');
            if (result == true) {
                $(this).parent().parent().parent().children('input').prop("checked", true);
                var fatherR = true;
            } else {
                var fatherR = false;
            }
            $.each($(this).parent().siblings('div').find("input[type='checkbox']"), function (index, domEle) {
                if (fatherR == true) { return false; }
                if ($(this).is(':checked') == true) {
                    fatherR = true;
                    return false;
                } else {
                    fatherR = false;
                }
            });
            if (fatherR == false) {
                $(this).parent().parent().parent().children('input').prop("checked", false);
            }
        });
        $(document).on('click', '.father', function(){
            var result = $(this).is(':checked');
            if (result == true) {
                $(this).parent().find("input[type='checkbox']").prop("checked", true);
            } else {
                $(this).parent().find("input[type='checkbox']").prop("checked", false);
            }
        })
    });

</script>
</body>

</html>