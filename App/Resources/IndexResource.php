<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/3/4
 * Time:  16:43
 */

namespace App\Resources;

use App\Controller\ApiOutputController;
use App\Lib\DB;
use App\Models\AgreementModel;
use App\Models\CustomerModel;
use App\Models\RentalModel;

class IndexResource
{
    public function __construct()
    {
        $this->DB = DB::link()->db;
    }

    public function getCollectInfo()
    {
        //获取所有的顾客
        $returnArray =array();
        $customerModel = new CustomerModel();
        $customerInfo = $customerModel->getIndex(0,0,'','');
        $customer = $this->ProcessingCustomer($customerInfo);
        $returnArray['customer'] = $customer;
        $rentalModle = new RentalModel();
        $returnArray['rentalUnitCount'] = DB::link()->table('rentalunits')->count();


        $agreementMode = new AgreementModel();
        $returnArray['agreement'] = $agreementMode->Statistics();

        ApiOutputController::ApiOutput($returnArray,200,'');

    }

    /**
     * @return void
     * Processing customer information
     */

    public function ProcessingCustomer($customerInfo)
    {
        $coustomerArr = array();
        $coustomerArr['totalCount'] = count($customerInfo);
        $today = date('Y-m-d');
        $yesterday = date("Y-m-d", strtotime("-1 day"));
        $todayCustomer = array();
        $yesterdayCustomer = array();
        foreach ($customerInfo as $item){
            $itemDay = date("Y-m-d", strtotime($item['createdDateTime'] ));
            if(strtotime($itemDay)  == strtotime($today)){
                array_push($todayCustomer,$item);
            }
            if(strtotime($itemDay) == strtotime($yesterday)){
                array_push($yesterdayCustomer ,$item);
            }
        }
        $coustomerArr['today'] = $todayCustomer;
        $coustomerArr['yesterday'] = $yesterdayCustomer;
        return $coustomerArr;

    }
}