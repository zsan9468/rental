<aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
        <li class="nav-item">
            <a class="nav-link-home nav-link-home-color" id="home" href="../index/index">
                <i class="bi bi-grid" style="margin-right: 10px;"></i>
                <span   class="lang"  key="dashboard.widget1.title">Dashboard</span>
            </a>
        </li>

        <?php
            foreach ($_SESSION['permission']['sqlMenu'] as $item)
            {
                ?>
                <li class="nav-item">
                    <a class="nav-link collapsed " data-bs-target="#<?php echo $item['info']['menu_icon'] ?>" data-bs-toggle="collapse" href="#">
                        <i class="bi bi-journal-text"></i><span  class="lang"  key="<?php echo "siderbar.".$item['info']['menu_name'] ?>"><?php echo $item['info']['menu_name'] ?></span><i class="bi bi-chevron-down ms-auto"></i>
                    </a>
                    <ul id="<?php echo $item['info']['menu_icon'] ?>" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                        <?php
                        if (count($item['child']) > 0){
                            foreach ($item['child'] as $childItem){
                                ?>
                                <li>
                                    <a href="../<?php echo $childItem['info']['menu_url'] ?>">
                                        <i class="bi bi-circle"></i><span  class="lang"  key="<?php echo "siderbar.".$childItem['info']['menu_name'] ?>"><?php echo $childItem['info']['menu_name'] ?></span>
                                    </a>
                                </li>

                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
        ?>

    </ul>
        <ul class="sidebar-nav secontpassWarning" id="sidebar-navs" style="text-align: right">
        <?php
        foreach ($_SESSION['permission']['sqlMenu'] as $item)
        {
            ?>
            <li class="nav-item">
                <a class="collapsed " id="hypa" data-bs-target="#<?php echo $item['info']['menu_icon'] ?>" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-journal-text hypa"></i>222
                </a>
                <ul id="<?php echo $item['info']['menu_icon'] ?>" class="hy_show" data-bs-parent="#sidebar-nav">
                    <?php
                    if (count($item['child']) > 0){
                        foreach ($item['child'] as $childItem){
                            ?>
                            <li>
                                <a href="../<?php echo $childItem['info']['menu_url'] ?>">
                                    <span  class="lang"  key="<?php echo "siderbar.".$childItem['info']['menu_name'] ?>">
                                      <i class="bi bi-journal-text"></i>22
                                    </span>
                                </a>
                            </li>

                            <?php
                        }
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
        ?>
    </ul>
    <text style="font-size:7px;padding-top: 10px;
          position: fixed;
          bottom: 20px;
          left: 20px;">&nbsp;V.0.1.0&nbsp;</text>
</aside><!-- End Sidebar-->

