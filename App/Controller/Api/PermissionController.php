<?php
/**
 * Created by : VsCode
 * User: Dumb Lake Monster (Wang Haiyang)
 * Date:  2023/2/27
 * Time:  15:20
 */

namespace App\Controller\Api;

use App\Controller\ApiOutputController;
use App\Resources\CostCenterResource;
use App\Resources\PermissionResource;
use App\Resources\UserResource;

class PermissionController extends \App\Controller\BaseController
{

    public function getPermission()
    {
//        var_dump($_REQUEST);
        $data = $this->getQueryJson();
        $PermissionRe= new PermissionResource();
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $data = $this->getQueryStringParams();
            if(empty($data['id'])){
                $result = $PermissionRe->permissionList();
                $data  = $PermissionRe->arrayPidProcess($result);
                $data  = $PermissionRe->objArrayToArray($data);
                ApiOutputController::ApiOutput($data,200,'');
            }
            $PermissionRe->getSignal($data['id']);
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $PermissionRe->create($data);
        }

        if($_SERVER['REQUEST_METHOD'] == 'PUT')
        {
            $PermissionRe->update($data);

        }
        if($_SERVER['REQUEST_METHOD'] == 'DELETE')
        {
            $PermissionRe->delete($data);
        }
    }
}