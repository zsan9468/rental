<!DOCTYPE html>
<html lang="en">
<?php include './../../layout/common.php'; ?>
<?php include './../../layout/head.php'; ?>

<body>
<!-- ======= Header ======= -->
<?php include './../../layout/header.php'; ?>
<!-- ======= Sidebar ======= -->
<?php include './../../layout/sidebar.php'; ?>
<main id="main" class="main">
    <section class="section dashboard">
        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-12">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <h5 class="card-title">Insurence  <span> LIST</span></h5>
                                <div class="card">
                                    <div class="card-body">
                                        <p></p>
                                        <button type="button" class="btn btn-primary" id="insurence_add">
                                            Create Insurence
                                        </button>
                                    </div>
                                </div>
                                <table id="insurence_table" class="hover table-striped" style="width:100%">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  Add General Content -->
        <div class="modal fade" id="insurence_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalagreement.create.insurence">Create New
                            Insurence</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                <?php include 'insurence_add.php'?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Dialog Scrollable-->
        <!--  Edit General Content -->
        <div class="modal fade" id="update_customer_add_model" tabindex="-1">
            <div class="modal-dialog modal-dialog-scrollable  modal-lg">
                <div class="modal-content" style="width: 800px; height: auto">
                    <div class="modal-header">
                        <h4 class="modal-title lang"  key="rentalagreement.update.insurence">Update New
                            Insurence</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class=" modal-body">
                        <div class="card">
                            <div class="card-body">
                                 <?php include 'insurence_update.php'?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- ======= Footer ======= -->
<?php include './../../layout/footer.html'; ?>
<!-- End Footer -->
<!-- Vendor JS Files -->
<?php include './../../layout/js.html' ?>
<script>
    $(document).ready(function () {

        var columns = [
            { data: 'id', title: "ID", name: "id" },
            { data: 'insurenceType', title: "insurenceType", name: "insurenceType" },
            { data: 'insurenceCost', title: "insurenceCost", name: "insurenceCost" },
            { data: 'Action', title: "Action", name: "Action", "orderable" : false,render: (data, type, row) => {
                    var upodateBtn = "upodate" + row.id;
                    var deleteBtn = "delete" + row.id;
                    $('#insurence_table').undelegate('tbody #' + upodateBtn, 'click');
                    $('#insurence_table').on('click', 'tbody #' + upodateBtn, function (e) {
                        $('#update_customer_add_model').modal('show');
                        $(".UpdatePropertySTyle .row").remove()
                        $("#insurence_form_update input[name=id]").val(row.id);
                        $("#insurence_form_update input[name=insurenceCost]").val(row.insurenceCost);
                        $("#insurence_form_update input[name=insurenceType]").val(row.insurenceType);
                    })
                    $('#insurence_table').undelegate('tbody #' + deleteBtn, 'click');
                    $('#insurence_table').on('click', 'tbody #' + deleteBtn, function () {
                        $('#delcfmOverhaul').modal('show');
                        $('#del_id').text(row.id);
                        $('#del_url').text('/api/agreement_insurence');
                    })
                    return "<button type=\"button\" class=\"btn  btn-sm   btn-outline-secondary  \"" + " id=" + upodateBtn + " >Update</button> " +
                        "  <button type=\"button\" class=\"btn  btn-sm  btn-outline-danger\"" + " id=" + deleteBtn + " >Delete</button>";
                }
            },
        ];
        tableAjax('/api/agreement_insurence', 'get', 'insurence_table', columns,);

        $('#insurence_add').click(function (e) {
            $('#insurence_add_model').modal('show');
        })
        /**
         * 添加保单类型
         */
        $('#insurence_form_create').submit(function (e) {
            var data =  $.agreement_insurenceBiaodan('insurence_form_create',e)
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement_insurence','post',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Create success!')
                        $('#insurence_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })
        $('#insurence_form_update').submit(function (e) {
            var id = $("#insurence_form_update input[name=id]").val();
            var data =  $.agreement_insurenceBiaodan('insurence_form_update',e)
            data.id = id
            if (e.isDefaultPrevented()) {
                return;
            }
            e.preventDefault()
            $.ajaxUnit('/api/agreement_insurence','put',data,
                function(data){
                    if(data.payload.code == 200){
                         commonUtil.message('Update success！')
                        $('#update_customer_add_model').modal('hide');
                        table.ajax.reload();
                    }else{
                         commonUtil.message(data.payload.data);
                    }
                },function(data){
                    console.log(data)
                })


        })

    });

</script>
</body>

</html>